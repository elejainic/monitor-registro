package guineaecuatorial.gov.monitorregistros.model.subarea;

import com.google.gson.annotations.Expose;

/**
 * Created by MarcosM on 23/06/2015.
 */
public class Row {

	@Expose
	private Integer id;
	@Expose
	private String nome;
	@Expose
	private String tipo;
	@Expose
	private String serv;
	@Expose
	private String periodo;
@Expose
private String progress;
@Expose
private String total;
@Expose
private String icon;
@Expose
private String cor;

public Row(Integer id, String nome, String tipo, String serv, String periodo, String progress, String total, String icon, String cor) {
	this.id = id;
	this.nome = nome;
	this.tipo = tipo;
	this.serv = serv;
	this.periodo = periodo;
	this.progress = progress;
	this.total = total;
	this.icon = icon;
	this.cor = cor;
}

	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @return The id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id The id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return The nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome The nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return The tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo The tipo
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return The serv
	 */
	public String getServ() {
		return serv;
	}

	/**
	 * @param serv The serv
	 */
	public void setServ(String serv) {
		this.serv = serv;
	}

	/**
	 * @return The periodo
	 */
	public String getPeriodo() {
		return periodo;
	}

	/**
	 * @param periodo The periodo
	 */
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

public String getCor() {
	return cor;
}

public void setCor(String cor) {
	this.cor = cor;
}
}