package guineaecuatorial.gov.monitorregistros.model.subarea;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;

import java.lang.reflect.Type;

/**
 * Created by MarcosM on 23/06/2015.
 */
public class Rows {

	@Expose
	private Row row;

	/**
	 *
	 * @return
	 * The row
	 */
	public Row getRow() {
		return row;
	}

	/**
	 *
	 * @param row
	 * The row
	 */
	public void setRow(Row row) {
		this.row = row;
	}




	public static class GrafItemDeserializer implements JsonDeserializer<Row[]> {

		@Override
		public Row[] deserialize(JsonElement json, Type typeOfT,
		                             JsonDeserializationContext context) throws JsonParseException {

			Row child = null;
			try {
				if (json instanceof JsonArray) {

					return new Gson().fromJson(json, Row[].class);

				}

				child = context.deserialize(json, Row.class);
			} catch (JsonParseException e) {
				e.printStackTrace();
			}

			return new Row[] { child };
		}
	}
}