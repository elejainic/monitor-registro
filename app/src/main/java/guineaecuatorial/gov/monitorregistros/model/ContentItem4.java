package guineaecuatorial.gov.monitorregistros.model;


public class ContentItem4 {
	public String name;
	public String tota;
	public String dia;
	public String orga;
	public String etapa;
	public String tempo;

	public ContentItem4(String n, String t, String di, String org, String et, String tp) {
		name = n;
		tota = t;
		dia = di;
		orga = org;
		etapa = et;
		tempo = tp;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getTempo() {
		return tempo;
	}

	public void setTempo(String tempo) {
		this.tempo = tempo;
	}

	public String getOrga() {
		return orga;
	}

	public void setOrga(String orga) {
		this.orga = orga;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTota() {
		return tota;
	}

	public void setTota(String tota) {
		this.tota = tota;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

}