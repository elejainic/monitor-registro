package guineaecuatorial.gov.monitorregistros.model.taskMy;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

/**
 * Created by MarcosM on 02/11/2015.
 */
public class REQ5003951_Model extends GenericArgs.Rows.Row {
private java.util.ArrayList<lista_taskModel> lista_task;


public REQ5003951_Model(java.util.ArrayList<lista_taskModel> lista_task) {
	super();
	this.lista_task = lista_task;

}

public java.util.ArrayList<lista_taskModel> getLista_task() {
	return this.lista_task;
}

public void setLista_task(java.util.ArrayList<lista_taskModel> lista_task) {
	this.lista_task = lista_task;
}

public static class lista_taskModel {
	private int id;

	public lista_taskModel() {
	}

	public lista_taskModel(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

}

}