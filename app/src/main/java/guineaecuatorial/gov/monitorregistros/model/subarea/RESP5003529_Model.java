package guineaecuatorial.gov.monitorregistros.model.subarea;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;


/**
 * Created by MarcosM on 20/06/2015.
 */
public  class  RESP5003529_Model  extends  GenericArgs.Rows.Row{
	@Expose
	private List<subareasModel> subareas = new ArrayList<>();
	private  String status;
	private  String status_text;



	public  void  setSubareas(List<subareasModel>  subareas){  this.subareas= subareas;  }
	public  void  setStatus(String  status){  this.status=status;  }
	public  void  setStatus_text(String  status_text){  this.status_text=status_text;  }


	public  List<subareasModel> getSubareas(){  return subareas; }
	public  String  getStatus(){  return  this.status; }
	public  String  getStatus_text(){  return  this.status_text; }

	public   class  subareasModel{
		private  String nome;
		private  int id;
		private   Grafs grafs;


		public  subareasModel(){}
		public  subareasModel(String nome, int id, Grafs grafs){

			this.nome=nome;
			this.id = id;
			this.grafs=grafs;
		}

		public void setGrafs(Grafs grafs) {
			this.grafs = grafs;
		}


		public  String  getNome(){  return  this.nome; }
		public Grafs  getGrafs(){  return  grafs; }

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
	}

	public static class SubAreaItemDeserializer implements JsonDeserializer<subareasModel[]> {

		@Override
		public subareasModel[] deserialize(JsonElement json, Type typeOfT,
		                         JsonDeserializationContext context) throws JsonParseException {

			subareasModel child = null;
			try {
				if (json instanceof JsonArray) {

					return new Gson().fromJson(json, subareasModel[].class);

				}

				child = context.deserialize(json, subareasModel.class);
			} catch (JsonParseException e) {
				e.printStackTrace();
			}

			return new subareasModel[] { child };
		}
	}






}
