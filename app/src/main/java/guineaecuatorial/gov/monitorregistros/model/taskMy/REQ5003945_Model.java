package guineaecuatorial.gov.monitorregistros.model.taskMy;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public  class  REQ5003945_Model  extends  GenericArgs.Rows.Row{
private int p_env_fk;
private String p_session_id;
private int p_prof_type_fk;
private int p_org_fk;
private int p_search;
private  String p_status_running;
private  String numero_processo;
private  String tipo_processo;
private  String organica;
private  String prioridade;
private  String data_inicio;
private  String data_fim;
private  String search;


public  REQ5003945_Model(int  p_env_fk, String p_session_id, int  p_prof_type_fk, int  p_org_fk, int  p_search, String  p_status_running, String  numero_processo, String  tipo_processo, String  organica, String  prioridade, String  data_inicio, String  data_fim, String  search){
	super();
	this.p_env_fk=p_env_fk;
	this.p_session_id=p_session_id;
	this.p_prof_type_fk=p_prof_type_fk;
	this.p_org_fk=p_org_fk;
	this.p_search=p_search;
	this.p_status_running=p_status_running;
	this.numero_processo=numero_processo;
	this.tipo_processo=tipo_processo;
	this.organica=organica;
	this.prioridade=prioridade;
	this.data_inicio=data_inicio;
	this.data_fim=data_fim;
	this.search=search;

}

public  void  setP_env_fk(int p_env_fk){  this.p_env_fk=p_env_fk;  }
public  void  setP_session_id(String p_session_id){  this.p_session_id=p_session_id;  }
public  void  setP_prof_type_fk(int p_prof_type_fk){  this.p_prof_type_fk=p_prof_type_fk;  }
public  void  setP_org_fk(int p_org_fk){  this.p_org_fk=p_org_fk;  }
public  void  setP_search(int p_search){  this.p_search=p_search;  }
public  void  setP_status_running(String  p_status_running){  this.p_status_running=p_status_running;  }
public  void  setNumero_processo(String  numero_processo){  this.numero_processo=numero_processo;  }
public  void  setTipo_processo(String  tipo_processo){  this.tipo_processo=tipo_processo;  }
public  void  setOrganica(String  organica){  this.organica=organica;  }
public  void  setPrioridade(String  prioridade){  this.prioridade=prioridade;  }
public  void  setData_inicio(String  data_inicio){  this.data_inicio=data_inicio;  }
public  void  setData_fim(String  data_fim){  this.data_fim=data_fim;  }
public  void  setSearch(String  search){  this.search=search;  }


public int getP_env_fk(){  return  this.p_env_fk; }
public String getP_session_id(){  return  this.p_session_id; }
public int getP_prof_type_fk(){  return  this.p_prof_type_fk; }
public int getP_org_fk(){  return  this.p_org_fk; }
public int getP_search(){  return  this.p_search; }
public  String  getP_status_running(){  return  this.p_status_running; }
public  String  getNumero_processo(){  return  this.numero_processo; }
public  String  getTipo_processo(){  return  this.tipo_processo; }
public  String  getOrganica(){  return  this.organica; }
public  String  getPrioridade(){  return  this.prioridade; }
public  String  getData_inicio(){  return  this.data_inicio; }
public  String  getData_fim(){  return  this.data_fim; }
public  String  getSearch(){  return  this.search; }

}