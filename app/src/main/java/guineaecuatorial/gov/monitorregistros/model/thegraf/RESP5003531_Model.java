package guineaecuatorial.gov.monitorregistros.model.thegraf;

import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;


/**
 * Created by MarcosM on 23/06/2015.
 */
public  class  RESP5003531_Model  extends  GenericArgs.Rows.Row{
	private  ArrayList<dadosModel> dados;
	private String dt_ultima_act;
	private  String status;
	private  String status_text;

	public  RESP5003531_Model(){super();}
	public  RESP5003531_Model(ArrayList<dadosModel> dados, String dt_ultima_act, String status, String status_text){
		super();
		this.dados=dados;
		this.dt_ultima_act = dt_ultima_act;
		this.status=status;
		this.status_text=status_text;
	}

	public  void  setDados(ArrayList<dadosModel>  dados){  this.dados=dados;  }
	public  void  setStatus(String  status){  this.status=status;  }
	public  void  setStatus_text(String  status_text){  this.status_text=status_text;  }


	public  ArrayList<dadosModel>  getDados(){  return  this.dados; }
	public  String  getStatus(){  return  this.status; }
	public  String  getStatus_text(){  return  this.status_text; }

	public String getDt_ultima_act() {
		return dt_ultima_act;
	}

	public void setDt_ultima_act(String dt_ultima_act) {
		this.dt_ultima_act = dt_ultima_act;
	}

	public  static    class  dadosModel{
		private  String xvals;
		private  String total;
		private  String nomelinha;
		private  String data_evol;
		private  String id;
		private  String titulo;
		private  String subtit;
		private  String datatext;

		public  dadosModel(){}
		public  dadosModel(String  xvals, String  total, String  nomelinha, String  data_evol, String  id, String  titulo, String  subtit, String  datatext){

			this.xvals=xvals;
			this.total=total;
			this.nomelinha=nomelinha;
			this.data_evol=data_evol;
			this.id=id;
			this.titulo=titulo;
			this.subtit=subtit;
			this.datatext=datatext;
		}

		public  void  setXvals(String  xvals){  this.xvals=xvals;  }
		public  void  setTotal(String  total){  this.total=total;  }
		public  void  setNomelinha(String  nomelinha){  this.nomelinha=nomelinha;  }
		public  void  setData_evol(String  data_evol){  this.data_evol=data_evol;  }
		public  void  setId(String  id){  this.id=id;  }
		public  void  setTitulo(String  titulo){  this.titulo=titulo;  }
		public  void  setSubtit(String  subtit){  this.subtit=subtit;  }
		public  void  setDatatext(String  datatext){  this.datatext=datatext;  }


		public  String  getXvals(){  return  this.xvals; }
		public  String  getTotal(){  return  this.total; }
		public  String  getNomelinha(){  return  this.nomelinha; }
		public  String  getData_evol(){  return  this.data_evol; }
		public  String  getId(){  return  this.id; }
		public  String  getTitulo(){  return  this.titulo; }
		public  String  getSubtit(){  return  this.subtit; }
		public  String  getDatatext(){  return  this.datatext; }

	}


}
