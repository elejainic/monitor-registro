package guineaecuatorial.gov.monitorregistros.model.taskAvail;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public class RESP5003952_Model extends GenericArgs.Rows.Row {
private String v_mensagem;
private String status;
private String status_text;

public RESP5003952_Model() {
	super();
}

public RESP5003952_Model(String v_mensagem, String status, String status_text) {
	super();
	this.v_mensagem = v_mensagem;
	this.status = status;
	this.status_text = status_text;
}

public String getV_mensagem() {
	return this.v_mensagem;
}

public void setV_mensagem(String v_mensagem) {
	this.v_mensagem = v_mensagem;
}

public String getStatus() {
	return this.status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getStatus_text() {
	return this.status_text;
}

public void setStatus_text(String status_text) {
	this.status_text = status_text;
}

}