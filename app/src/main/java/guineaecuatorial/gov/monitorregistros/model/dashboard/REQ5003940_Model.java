package guineaecuatorial.gov.monitorregistros.model.dashboard;


import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public class REQ5003940_Model extends GenericArgs.Rows.Row {
    private int dashbord_id;
    private String session;
    private String lingua;


    public REQ5003940_Model(int dashbord_id, String session, String lingua) {
        super();
        this.dashbord_id = dashbord_id;
        this.session = session;
        this.lingua = lingua;

    }

    public int getDashbord_id() {
        return this.dashbord_id;
    }

    public void setDashbord_id(int dashbord_id) {
        this.dashbord_id = dashbord_id;
    }

    public String getSession() {
        return this.session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getLingua() {
        return this.lingua;
    }

    public void setLingua(String lingua) {
        this.lingua = lingua;
    }


}