package guineaecuatorial.gov.monitorregistros.model.taskMy;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public  class  RESP5003945_Model  extends  GenericArgs.Rows.Row{
private  java.util.ArrayList<minhas_tarefasModel> minhas_tarefas;
private  String status;
private  String status_text;


public  RESP5003945_Model(java.util.ArrayList<minhas_tarefasModel>  minhas_tarefas, String  status, String  status_text){
	super();
	this.minhas_tarefas=minhas_tarefas;
	this.status=status;
	this.status_text=status_text;
}

public java.util.ArrayList<minhas_tarefasModel> getMinhas_tarefas() {
	return this.minhas_tarefas;
}

public  void  setMinhas_tarefas(java.util.ArrayList<minhas_tarefasModel>  minhas_tarefas){  this.minhas_tarefas=minhas_tarefas;  }

public  String  getStatus(){  return  this.status; }

public void setStatus(String status) {
	this.status = status;
}

public  String  getStatus_text(){  return  this.status_text; }

public void setStatus_text(String status_text) {
	this.status_text = status_text;
}

public  static    class  minhas_tarefasModel{
	private  String priority;
	private  String tipo;
	private  String descricao;
	private  String atribuido_por;
	private  String data_entrada;
	private  String dias_em_espera;
	private int id;
	private  String proc_fk;
	private  String proc_num;
	private  String search;
	private  String apache_dad;
	private  String env_fk;
	private int org_fk;
	private  String flg_adhoc;
	private String executar;
	private String detalhe;


	public minhas_tarefasModel(String priority, String tipo, String descricao, String atribuido_por, String data_entrada, String dias_em_espera, int id, String proc_fk, String proc_num, String search, String apache_dad, String env_fk, int org_fk, String flg_adhoc, String executar, String detalhe) {

		this.priority=priority;
		this.tipo=tipo;
		this.descricao=descricao;
		this.atribuido_por=atribuido_por;
		this.data_entrada=data_entrada;
		this.dias_em_espera=dias_em_espera;
		this.id=id;
		this.proc_fk=proc_fk;
		this.proc_num=proc_num;
		this.search=search;
		this.apache_dad=apache_dad;
		this.env_fk=env_fk;
		this.org_fk = org_fk;
		this.flg_adhoc=flg_adhoc;
		this.executar = executar;
		this.detalhe = detalhe;
	}

	public String getPriority() {
		return this.priority;
	}

	public  void  setPriority(String  priority){  this.priority=priority;  }

	public String getTipo() {
		return this.tipo;
	}

	public  void  setTipo(String  tipo){  this.tipo=tipo;  }

	public String getDescricao() {
		return this.descricao;
	}

	public  void  setDescricao(String  descricao){  this.descricao=descricao;  }

	public String getAtribuido_por() {
		return this.atribuido_por;
	}

	public  void  setAtribuido_por(String  atribuido_por){  this.atribuido_por=atribuido_por;  }

	public String getData_entrada() {
		return this.data_entrada;
	}

	public  void  setData_entrada(String  data_entrada){  this.data_entrada=data_entrada;  }

	public String getDias_em_espera() {
		return this.dias_em_espera;
	}

	public  void  setDias_em_espera(String  dias_em_espera){  this.dias_em_espera=dias_em_espera;  }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public  String  getProc_fk(){  return  this.proc_fk; }

	public void setProc_fk(String proc_fk) {
		this.proc_fk = proc_fk;
	}

	public  String  getProc_num(){  return  this.proc_num; }

	public void setProc_num(String proc_num) {
		this.proc_num = proc_num;
	}

	public  String  getSearch(){  return  this.search; }

	public void setSearch(String search) {
		this.search = search;
	}

	public  String  getApache_dad(){  return  this.apache_dad; }

	public void setApache_dad(String apache_dad) {
		this.apache_dad = apache_dad;
	}

	public  String  getEnv_fk(){  return  this.env_fk; }

	public void setEnv_fk(String env_fk) {
		this.env_fk = env_fk;
	}

	public  String  getFlg_adhoc(){  return  this.flg_adhoc; }

	public void setFlg_adhoc(String flg_adhoc) {
		this.flg_adhoc = flg_adhoc;
	}

	public String getExecutar() {
		return executar;
	}

	public void setExecutar(String executar) {
		this.executar = executar;
	}

	public String getDetalhe() {
		return detalhe;
	}

	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

	public int getOrg_fk() {
		return org_fk;
	}

	public void setOrg_fk(int org_fk) {
		this.org_fk = org_fk;
	}
}


}