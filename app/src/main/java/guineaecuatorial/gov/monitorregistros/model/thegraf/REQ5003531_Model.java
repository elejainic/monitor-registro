package guineaecuatorial.gov.monitorregistros.model.thegraf;


import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

/**
 * Created by MarcosM on 23/06/2015.
 */
public  class  REQ5003531_Model  extends  GenericArgs.Rows.Row{
	private  String session;
	private  String idgraf;
	private  String periodo;
	private  String data_de;
	private  String data_ate;
	private int id_subarea;


	public  REQ5003531_Model(){super();}
	public  REQ5003531_Model(String session, String idgraf, String periodo, String data_de, String data_ate, int id_subarea){
		super();
		this.session=session;
		this.idgraf=idgraf;
		this.periodo=periodo;
		this.data_de=data_de;
		this.data_ate=data_ate;

		this.id_subarea = id_subarea;
	}

	public  void  setSession(String  session){  this.session=session;  }
	public  void  setIdgraf(String  idgraf){  this.idgraf=idgraf;  }
	public  void  setPeriodo(String  periodo){  this.periodo=periodo;  }
	public  void  setData_de(String  data_de){  this.data_de=data_de;  }
	public  void  setData_ate(String  data_ate){  this.data_ate=data_ate;  }


	public  String  getSession(){  return  this.session; }
	public  String  getIdgraf(){  return  this.idgraf; }
	public  String  getPeriodo(){  return  this.periodo; }
	public  String  getData_de(){  return  this.data_de; }
	public  String  getData_ate(){  return  this.data_ate; }

	public int getId_subarea() {
		return id_subarea;
	}

	public void setId_subarea(int id_subarea) {
		this.id_subarea = id_subarea;
	}
}