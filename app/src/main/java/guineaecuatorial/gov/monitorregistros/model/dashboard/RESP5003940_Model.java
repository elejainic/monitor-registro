package guineaecuatorial.gov.monitorregistros.model.dashboard;


import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public class RESP5003940_Model extends GenericArgs.Rows.Row {
    private java.util.ArrayList<grafsModel> grafs;
    private String status;
    private String status_text;

    public RESP5003940_Model() {
        super();
    }

    public RESP5003940_Model(java.util.ArrayList<grafsModel> grafs, String status, String status_text) {
        super();
        this.grafs = grafs;
        this.status = status;
        this.status_text = status_text;
    }

    public java.util.ArrayList<grafsModel> getGrafs() {
        return this.grafs;
    }

    public void setGrafs(java.util.ArrayList<grafsModel> grafs) {
        this.grafs = grafs;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_text() {
        return this.status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }

    public static class grafsModel {
        private int id;
        private String tipo;
        private String periodo;
        private String nome_dashboard;
        private String tp_dashboard;
        private String dash_periodo;
        private Dados dados;
//		private RESP5003531_Model dados;

        public grafsModel() {
        }

        public grafsModel(int id, String tipo, String periodo, String nome_dashboard, String tp_dashboard, String dash_periodo, Dados dados) {

            this.id = id;
            this.tipo = tipo;
            this.periodo = periodo;
            this.nome_dashboard = nome_dashboard;
            this.tp_dashboard = tp_dashboard;
            this.dash_periodo = dash_periodo;
            this.dados = dados;
        }

        public int getId() {
            return this.id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTipo() {
            return this.tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public String getPeriodo() {
            return this.periodo;
        }

        public void setPeriodo(String periodo) {
            this.periodo = periodo;
        }

        public String getNome_dashboard() {
            return this.nome_dashboard;
        }
//		public  void  setDados(JSONObject dados){  this.dados= RESP5003531_Helper.parseModel(dados);  }

        public void setNome_dashboard(String nome_dashboard) {
            this.nome_dashboard = nome_dashboard;
        }

        public String getTp_dashboard() {
            return this.tp_dashboard;
        }

        public void setTp_dashboard(String tp_dashboard) {
            this.tp_dashboard = tp_dashboard;
        }

        public String getDash_periodo() {
            return this.dash_periodo;
        }

        public void setDash_periodo(String dash_periodo) {
            this.dash_periodo = dash_periodo;
        }

        public Dados getDados() {
            return this.dados;
        }

        public void setDados(Dados dados) {
            this.dados = dados;
        }

    }


}