package guineaecuatorial.gov.monitorregistros.model.taskMy;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

/**
 * Created by MarcosM on 02/11/2015.
 */
public class RESP5003951_Model extends GenericArgs.Rows.Row {
private String v_mensagem;
private String v_mensagem_sucess;
private String status;
private String status_text;

public RESP5003951_Model() {
	super();
}

public RESP5003951_Model(String v_mensagem, String v_mensagem_sucess, String status, String status_text) {
	super();
	this.v_mensagem = v_mensagem;
	this.v_mensagem_sucess = v_mensagem_sucess;
	this.status = status;
	this.status_text = status_text;
}

public String getV_mensagem() {
	return this.v_mensagem;
}

public void setV_mensagem(String v_mensagem) {
	this.v_mensagem = v_mensagem;
}

public String getV_mensagem_sucess() {
	return this.v_mensagem_sucess;
}

public void setV_mensagem_sucess(String v_mensagem_sucess) {
	this.v_mensagem_sucess = v_mensagem_sucess;
}

public String getStatus() {
	return this.status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getStatus_text() {
	return this.status_text;
}

public void setStatus_text(String status_text) {
	this.status_text = status_text;
}

}