package guineaecuatorial.gov.monitorregistros.model.login;


import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public class REQ5001186_Model extends GenericArgs.Rows.Row {
	private String username;
	private String password;
	private String google_id;
	private String forma_login;


	public REQ5001186_Model() {
		super();
	}

	public REQ5001186_Model(String username, String password, String google_id, String forma_login) {
		super();
		this.username = username;
		this.password = password;
		this.google_id = google_id;
		this.forma_login = forma_login;

	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGoogle_id() {
		return this.google_id;
	}

	public void setGoogle_id(String google_id) {
		this.google_id = google_id;
	}

	public String getForma_login() {
		return this.forma_login;
	}

	public void setForma_login(String forma_login) {
		this.forma_login = forma_login;
	}


}