package guineaecuatorial.gov.monitorregistros.model.taskByColaboradores;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public class RESP5003950_Model extends GenericArgs.Rows.Row {
private java.util.ArrayList<colaboradoresModel> colaboradores;
private String status;
private String status_text;

public RESP5003950_Model() {
	super();
}

public RESP5003950_Model(java.util.ArrayList<colaboradoresModel> colaboradores, String status, String status_text) {
	super();
	this.colaboradores = colaboradores;
	this.status = status;
	this.status_text = status_text;
}

public java.util.ArrayList<colaboradoresModel> getColaboradores() {
	return this.colaboradores;
}

public void setColaboradores(java.util.ArrayList<colaboradoresModel> colaboradores) {
	this.colaboradores = colaboradores;
}

public String getStatus() {
	return this.status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getStatus_text() {
	return this.status_text;
}

public void setStatus_text(String status_text) {
	this.status_text = status_text;
}

public static class colaboradoresModel {
	private String nome;
	private String contacto;
	private int n_tarefas;
	private int n_atendimento;
	private int media_tempo;
	private int ranking;
	private int percent;

	public colaboradoresModel() {
	}

	public colaboradoresModel(String nome, String contacto, int n_tarefas, int n_atendimento, int media_tempo, int ranking, int percent) {
		this.nome = nome;
		this.contacto = contacto;
		this.n_tarefas = n_tarefas;
		this.n_atendimento = n_atendimento;
		this.media_tempo = media_tempo;
		this.ranking = ranking;
		this.percent = percent;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getContacto() {
		return this.contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public int getNtarefas() {
		return this.n_tarefas;
	}

	public void setNtarefas(int n_tarefas) {
		this.n_tarefas = n_tarefas;
	}

	public int getNatendimento() {
		return this.n_atendimento;
	}

	public void setNatendimento(int n_atendimento) {
		this.n_atendimento = n_atendimento;
	}

	public int getMediatempo() {
		return this.media_tempo;
	}

	public void setMediatempo(int media_tempo) {
		this.media_tempo = media_tempo;
	}

	public int getRanking() {
		return this.ranking;
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public int getPercent() {
		return this.percent;
	}

	public void setPercent(int percent) {
		this.percent = percent;
	}

}

}
