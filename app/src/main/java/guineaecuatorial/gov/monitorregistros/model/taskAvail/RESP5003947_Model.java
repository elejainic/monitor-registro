package guineaecuatorial.gov.monitorregistros.model.taskAvail;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public  class  RESP5003947_Model  extends  GenericArgs.Rows.Row{
private  java.util.ArrayList<tarefas_disponiveisModel> tarefas_disponiveis;
private  String status;
private  String status_text;

public  RESP5003947_Model(){super();}
public  RESP5003947_Model(java.util.ArrayList<tarefas_disponiveisModel>  tarefas_disponiveis, String  status, String  status_text){
	super();
	this.tarefas_disponiveis=tarefas_disponiveis;
	this.status=status;
	this.status_text=status_text;
}

public java.util.ArrayList<tarefas_disponiveisModel> getTarefas_disponiveis() {
	return this.tarefas_disponiveis;
}

public  void  setTarefas_disponiveis(java.util.ArrayList<tarefas_disponiveisModel>  tarefas_disponiveis){  this.tarefas_disponiveis=tarefas_disponiveis;  }

public  String  getStatus(){  return  this.status; }

public void setStatus(String status) {
	this.status = status;
}

public  String  getStatus_text(){  return  this.status_text; }

public void setStatus_text(String status_text) {
	this.status_text = status_text;
}

public  static    class  tarefas_disponiveisModel{
	private  String priority;
	private  String categoria_processo;
	private  String tarefa;
	private  String dt__entrada;
	private int id;
	private  String proc_fk;
	private  String proc_num;
	private  String search;
	private  String env_fk;
	private  int prof_tp_fk;
	private  int org_fk;
	private  int user_assign_fk;

	public  tarefas_disponiveisModel(){}

	public tarefas_disponiveisModel(String priority, String categoria_processo, String tarefa, String dt__entrada, int id, String proc_fk, String proc_num, String search, String env_fk, int prof_tp_fk, int org_fk, int user_assign_fk) {

		this.priority=priority;
		this.categoria_processo=categoria_processo;
		this.tarefa=tarefa;
		this.dt__entrada=dt__entrada;
		this.id=id;
		this.proc_fk=proc_fk;
		this.proc_num=proc_num;
		this.search=search;
		this.env_fk=env_fk;
		this.prof_tp_fk=prof_tp_fk;
		this.org_fk=org_fk;
		this.user_assign_fk=user_assign_fk;
	}

	public String getPriority() {
		return this.priority;
	}

	public  void  setPriority(String  priority){  this.priority=priority;  }

	public String getCategoria_processo() {
		return this.categoria_processo;
	}

	public  void  setCategoria_processo(String  categoria_processo){  this.categoria_processo=categoria_processo;  }

	public String getTarefa() {
		return this.tarefa;
	}

	public  void  setTarefa(String  tarefa){  this.tarefa=tarefa;  }

	public String getDt__entrada() {
		return this.dt__entrada;
	}

	public  void  setDt__entrada(String  dt__entrada){  this.dt__entrada=dt__entrada;  }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public  String  getProc_fk(){  return  this.proc_fk; }

	public void setProc_fk(String proc_fk) {
		this.proc_fk = proc_fk;
	}

	public  String  getProc_num(){  return  this.proc_num; }

	public void setProc_num(String proc_num) {
		this.proc_num = proc_num;
	}

	public  String  getSearch(){  return  this.search; }

	public void setSearch(String search) {
		this.search = search;
	}

	public  String  getEnv_fk(){  return  this.env_fk; }

	public void setEnv_fk(String env_fk) {
		this.env_fk = env_fk;
	}

	public  int  getProf_tp_fk(){  return  this.prof_tp_fk; }

	public void setProf_tp_fk(int prof_tp_fk) {
		this.prof_tp_fk = prof_tp_fk;
	}

	public  int  getOrg_fk(){  return  this.org_fk; }

	public void setOrg_fk(int org_fk) {
		this.org_fk = org_fk;
	}

	public  int  getUser_assign_fk(){  return  this.user_assign_fk; }

	public void setUser_assign_fk(int user_assign_fk) {
		this.user_assign_fk = user_assign_fk;
	}

}


}