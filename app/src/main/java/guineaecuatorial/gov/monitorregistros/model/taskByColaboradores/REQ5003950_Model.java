package guineaecuatorial.gov.monitorregistros.model.taskByColaboradores;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

/**
 * Created by MarcosM on 03/11/2015.
 */
public class REQ5003950_Model extends GenericArgs.Rows.Row {
private String p_session;
private String data_inicio;
private String data_fim;
private String numero_processo;
private String prioridade;
private String tipo_processo;
private String tipo_etapa;
private String organica;


public REQ5003950_Model(String p_session, String data_inicio, String data_fim, String numero_processo, String prioridade, String tipo_processo, String tipo_etapa, String organica) {
	super();
	this.p_session = p_session;
	this.data_inicio = data_inicio;
	this.data_fim = data_fim;
	this.numero_processo = numero_processo;
	this.prioridade = prioridade;
	this.tipo_processo = tipo_processo;
	this.tipo_etapa = tipo_etapa;
	this.organica = organica;

}

public String getP_session() {
	return this.p_session;
}

public void setP_session(String p_session) {
	this.p_session = p_session;
}

public String getData_inicio() {
	return this.data_inicio;
}

public void setData_inicio(String data_inicio) {
	this.data_inicio = data_inicio;
}

public String getData_fim() {
	return this.data_fim;
}

public void setData_fim(String data_fim) {
	this.data_fim = data_fim;
}

public String getNumero_processo() {
	return this.numero_processo;
}

public void setNumero_processo(String numero_processo) {
	this.numero_processo = numero_processo;
}

public String getPrioridade() {
	return this.prioridade;
}

public void setPrioridade(String prioridade) {
	this.prioridade = prioridade;
}

public String getTipo_processo() {
	return this.tipo_processo;
}

public void setTipo_processo(String tipo_processo) {
	this.tipo_processo = tipo_processo;
}

public String getTipo_etapa() {
	return this.tipo_etapa;
}

public void setTipo_etapa(String tipo_etapa) {
	this.tipo_etapa = tipo_etapa;
}

public String getOrganica() {
	return this.organica;
}

public void setOrganica(String organica) {
	this.organica = organica;
}

}