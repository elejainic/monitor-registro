package guineaecuatorial.gov.monitorregistros.model.dashboard;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.model.subarea.Rows;
import guineaecuatorial.gov.monitorregistros.model.thegraf.RESP5003531_Model;


/**
 * Created by MarcosM on 24/06/2015.
 */
public class Dados {

    @Expose
    private List<RESP5003531_Model> rows = new ArrayList<>();

    /**
     * @return The rows
     */
    public List<RESP5003531_Model> getRows() {
        return rows;
    }

    /**
     * @param rows The rows
     */
    public void setRows(List<RESP5003531_Model> rows) {
        this.rows = rows;
    }

    public static class GrafItemDeserializer implements JsonDeserializer<Rows[]> {

        @Override
        public Rows[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            try {
                if (json instanceof JsonArray) {

                    return new Gson().fromJson(json, Rows[].class);

                }

                Rows child = context.deserialize(json, Rows.class);

                return new Rows[]{child};
            } catch (JsonParseException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}