package guineaecuatorial.gov.monitorregistros.model.subarea;


import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

/**
 * Created by MarcosM on 20/06/2015.
 */
public  class  REQ5003529_Model  extends  GenericArgs.Rows.Row{
	private  String session;
	private  int idsubareas;


	public  REQ5003529_Model(){super();}
	public  REQ5003529_Model(String  session, int  idsubareas){
		super();
		this.session=session;
		this.idsubareas=idsubareas;

	}

	public  void  setSession(String  session){  this.session=session;  }
	public  void  setIdsubareas(int  idsubareas){  this.idsubareas=idsubareas;  }


	public  String  getSession(){  return  this.session; }
	public  int  getIdsubareas(){  return  this.idsubareas; }


}
