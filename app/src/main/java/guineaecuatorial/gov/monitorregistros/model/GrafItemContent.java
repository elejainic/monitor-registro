package guineaecuatorial.gov.monitorregistros.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.model.subarea.Row;


/**
 * Helper class for providing sample nome for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class GrafItemContent {

	/**
	 * An array of sample (dummy) items.
	 */
	public List<Row> ITEMS = new ArrayList<>();

	/**
	 * A map of sample (dummy) items, by ID.
	 */

//	static {
//		// Add 3 sample items.
//		addItem(new GrafItem("GRAF1", "Licença 1","pie","SEMANA"));
//		addItem(new GrafItem("GRAF2", "Licença 2","pie","SEMANA"));
//		addItem(new GrafItem("GRAF3", "Licença 3","pie","SEMANA"));
//		addItem(new GrafItem("GRAF5", "Licença Evolução 5","pie","SEMANA"));
//		addItem(new GrafItem("GRAF5", "Licença Evolução 5","pie","SEMANA"));
//		addItem(new GrafItem("GRAF5", "Licença Evolução 5","pie","SEMANA"));
//		addItem(new GrafItem("GRAF5", "Licença Evolução 5","pie","SEMANA"));
//		addItem(new GrafItem("GRAF5", "Licença Evolução 5","pie","SEMANA"));
//	}
	public void addItem(Row item) {
		ITEMS.add(item);
//		ITEM_MAP.put(item.serv, item);
	}



	/**
	 * A dummy item representing a piece of nome.
	 */
	public static class GrafItem implements Parcelable {
		public static final Creator<GrafItem> CREATOR = new Creator<GrafItem>() {
			@Override
			public GrafItem createFromParcel(Parcel in) {
				return new GrafItem(in);
			}

			@Override
			public GrafItem[] newArray(int size) {
				return new GrafItem[size];
			}
		};
		public int id;
		public String serv;
		public String nome;
		public String tipo;
		public String periodo;

		public GrafItem(int id, String nome, String tipo, String period) {
			this.id = id;
			this.nome = nome;
			this.tipo = tipo;
			this.periodo = period;
		}

		protected GrafItem(Parcel in) {
			id = in.readInt();
			serv = in.readString();
			nome = in.readString();
			tipo = in.readString();
			periodo = in.readString();
		}

		public int getId() {
			return id;
		}

		public String getNome() {
			return nome;
		}

		public String getTipo() {
			return tipo;
		}

		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		public String getPeriod() {
			return periodo;
		}

		public void setPeriod(String period) {
			this.periodo = period;
		}

		@Override
		public String toString() {
			return nome;
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeInt(id);
			dest.writeString(serv);
			dest.writeString(nome);
			dest.writeString(tipo);
			dest.writeString(periodo);
		}
	}


}
