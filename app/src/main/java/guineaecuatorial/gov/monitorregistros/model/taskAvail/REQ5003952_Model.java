package guineaecuatorial.gov.monitorregistros.model.taskAvail;

import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public class REQ5003952_Model extends GenericArgs.Rows.Row {
private String session_id;
private java.util.ArrayList<lista_taskModel> lista_task;


public REQ5003952_Model() {
	super();
}

public REQ5003952_Model(String session_id, java.util.ArrayList<lista_taskModel> lista_task) {
	super();
	this.session_id = session_id;
	this.lista_task = lista_task;

}

public String getSession_id() {
	return this.session_id;
}

public void setSession_id(String session_id) {
	this.session_id = session_id;
}

public java.util.ArrayList<lista_taskModel> getLista_task() {
	return this.lista_task;
}

public void setLista_task(java.util.ArrayList<lista_taskModel> lista_task) {
	this.lista_task = lista_task;
}

public static class lista_taskModel {
	private int id;

	public lista_taskModel() {
	}

	public lista_taskModel(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

}

}