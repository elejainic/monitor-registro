package guineaecuatorial.gov.monitorregistros.model.login;


import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public class RESP5001186_Model extends GenericArgs.Rows.Row {
	private String sessid;
	private String mensagem;
	private String status;
	private String status_text;

	public RESP5001186_Model() {
		super();
	}

	public RESP5001186_Model(String sessid, String mensagem, String status, String status_text) {
		super();
		this.sessid = sessid;
		this.mensagem = mensagem;
		this.status = status;
		this.status_text = status_text;
	}

	public String getSessid() {
		return this.sessid;
	}

	public void setSessid(String sessid) {
		this.sessid = sessid;
	}

	public String getMensagem() {
		return this.mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus_text() {
		return this.status_text;
	}

	public void setStatus_text(String status_text) {
		this.status_text = status_text;
	}

}