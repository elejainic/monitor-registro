package guineaecuatorial.gov.monitorregistros.util;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.fragments.BarChartFrag;

//import cv.gov.nosi.monitor.R;
//import cv.gov.nosi.monitor.fragments.BarChartFrag;

public class MyMarkerViewBar extends MarkerView {


	private TextView tvContent;
	private TextView tvDatasetName;

	public MyMarkerViewBar(Context context, int layoutResource) {
		super(context, layoutResource);

		tvContent = (TextView) findViewById(R.id.tvContent);
		tvDatasetName = (TextView) findViewById(R.id.tv_marker_dataset_name);
//		mChart = (LineChart) context.findViewById(R.id.lineChart1);
	}


	// callbacks everytime the MarkerView is redrawn, can be used to update the
	// nome
	@Override
	public void refreshContent(Entry e, Highlight h) {

//		if (e instanceof CandleEntry) {
//
//			CandleEntry ce = (CandleEntry) e;
//
//			tvContent.setText(" " + Utils.formatNumber(ce.getHigh(), 0, true).trim());
//		} else {


			String totalStac;
			NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###.##");

			String stackName="";


			if (h.getStackIndex() != -1) {
				totalStac = nf.format((double) ((BarEntry) e).getVals()[h.getStackIndex()]);
				stackName = BarChartFrag.mChart.getLegend().getLabel(h.getStackIndex()) + ": " + totalStac + " / ";
			}


				try {

					tvDatasetName.setText(BarChartFrag.mChart.getXValue(e.getXIndex()));
					int backgroundColor = ContextCompat.getColor(getContext(), ColorTemplate.PASTEL_COLORS[e.getXIndex()]);
					tvDatasetName.setBackgroundColor(backgroundColor);
					tvContent.setBackgroundColor(backgroundColor);
					if(((BarEntry)e).isStacked()) {
						View viewById = findViewById(R.id.viewStackedMarker);
						viewById.setVisibility(VISIBLE);
						viewById.setBackgroundColor(BarChartFrag.mChart.getLegend().getColors()[h.getStackIndex()]);

					}
					tvContent.setText(stackName + "Total: " + nf.format((double) e.getVal()));
					if(e.getData()!=null){
						BigDecimal total = (BigDecimal) e.getData();
						tvContent.setText(stackName + "Total: " + nf.format(total));

					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}




//		}
	}

	@Override
	public int getXOffset() {
		return -(getWidth() / 2);
	}

	@Override
	public int getYOffset() {
		return -getHeight()-5;
	}


	@Override
	public void draw(Canvas canvas, float posx, float posy) {


		// take offsets into consideration
		if(posx>(BarChartFrag.mChart.getWidth()-(getWidth() / 2)))
			posx += (BarChartFrag.mChart.getWidth()-(getWidth() / 2))-posx+getXOffset();
		else if(posx<(getWidth() / 2))
			posx +=(getWidth() / 2)-posx+getXOffset();
		else
			posx += getXOffset();
		posy += getYOffset();

		// translate to the correct position and draw
		canvas.translate(posx, posy);
		draw(canvas);
		canvas.translate(-posx, -posy);

	}
}
