package guineaecuatorial.gov.monitorregistros.util;

import android.content.Context;
import android.graphics.Canvas;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.fragments.LineFragment;


public class MyMarkerView extends MarkerView {


	private TextView tvContent;
	private TextView tvDatasetName;

	public MyMarkerView(Context context, int layoutResource) {
		super(context, layoutResource);

		tvContent = (TextView) findViewById(R.id.tvContent);
		tvDatasetName = (TextView) findViewById(R.id.tv_marker_dataset_name);
//		mChart = (LineChart) context.findViewById(R.id.lineChart1);
	}


	// callbacks everytime the MarkerView is redrawn, can be used to update the
	// nome
	@Override
	public void refreshContent(Entry e, Highlight h) {


//			Log.e("MyMarkerView", "refreshContent (line 43): ");


				try {
					tvDatasetName.setText(LineFragment.mChart.getLegend().getLabel(h.getDataSetIndex()));
					tvDatasetName.setBackgroundColor(LineFragment.mChart.getLegend().getColors()[h.getDataSetIndex()]);
					tvContent.setBackgroundColor(LineFragment.mChart.getLegend().getColors()[h.getDataSetIndex()]);
					NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###,###,###.#");
					tvContent.setText("" + nf.format(e.getData()==null?(double) e.getVal():(BigDecimal) e.getData()) + " / {faw-calendar} " + LineFragment.mChart.getXValue(e.getXIndex()));
				} catch (Exception e1) {
					e1.printStackTrace();
				}





	}

	@Override
	public int getXOffset() {
		return				-(getWidth() / 2);
	}

	@Override
	public int getYOffset() {
		return -getHeight();
	}

	@Override
	public void draw(Canvas canvas, float posx, float posy) {

//			Log.e("MyMarkerView", "draw (line 59): pox " + posx + " poy " + posy);
			// take offsets into consideration
		if(posx>(LineFragment.mChart.getWidth()-(getWidth() / 2)))
			posx += (LineFragment.mChart.getWidth()-(getWidth() / 2))-posx+getXOffset();
		else if(posx<(getWidth() / 2))
			posx +=(getWidth() / 2)-posx+getXOffset();
		else
			posx += getXOffset();
			posy += getYOffset();

			// translate to the correct position and draw
			canvas.translate(posx, posy);
			draw(canvas);
			canvas.translate(-posx, -posy);

	}


}
