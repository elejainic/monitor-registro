package guineaecuatorial.gov.monitorregistros.util;//package cv.gov.nosi.monitor.util;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.support.v4.app.FragmentActivity;
//import android.util.Log;
//
//import com.squareup.timessquare.CalendarPickerView;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.Locale;
//
//import cv.gov.nosi.monitor.R;
//
//public class DateRange {
//
//    public static String firstData;
//    public static String lastDate;
//    private String TAG = DateRange.class.getSimpleName();
//    private CalendarPickerView dialogView;
//    private AlertDialog theDialog;
//
//
//    public DateRange() {
//
//    }
//
//    public void DateRange(Context c) {
//
//
//        final Calendar nextYear = Calendar.getInstance();
//        // nextYear.add(Calendar.YEAR, 1);
//        final Calendar lastYear = Calendar.getInstance();
//        // lastYear.add(Calendar.YEAR, -1);
//        final Calendar today = Calendar.getInstance();
//
//        ArrayList<Date> dates = new ArrayList<>();
//
//
//        dates.add(today.getTime());
//
//
//        dialogView = (CalendarPickerView) ((FragmentActivity) c)
//                .getLayoutInflater() //
//                .inflate(R.layout.dialog, dialogView, false);
//
//
//        lastYear.add(Calendar.MONTH, -7);
//
//
//        nextYear.add(Calendar.MONTH, 4);
//        dialogView.init(lastYear.getTime(), nextYear.getTime())
//                .inMode(CalendarPickerView.SelectionMode.RANGE)
//                .withSelectedDates(dates);
//
//        theDialog = new AlertDialog.Builder(c)
//                .setTitle("Escolha 2 datas:")
//                .setView(dialogView)
//                .setNeutralButton("Feito.",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(
//                                    DialogInterface dialogInterface, int i) {
//                                Log.d(TAG, "onShow: fix the dimens!");
//                                Date first = dialogView.getSelectedDates().get(
//                                        0);
//                                Date last = dialogView.getSelectedDates()
//                                        .get(dialogView.getSelectedDates()
//                                                .size() - 1);
//
//                                Calendar cal = new GregorianCalendar();
//                                SimpleDateFormat df = new SimpleDateFormat(
//                                        "dd-MM-yyyy", Locale.getDefault());
//                                // Convert Date em Calendar
//                                cal.setTime(first);
//                                firstData = df.format(first);
//                                 lastDate = df.format(last);
////                                Log.e(TAG, "Startdate " + firstData + "e last " + lastDate);
////                                Log.e("DateRange", "onClick (line 78): ");
//////                                cal.setTime(last);
////                                Log.e("DateRange", "onClick (line 84): ");
////                                Log.e(TAG, "Enddate " + lastDate);
//
//                                // Toast.makeText(mContext, toast,
//                                // Toast.LENGTH_SHORT).show();
//
//                                dialogInterface.dismiss();
//
//                            }
//                        }).create();
//        theDialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialogInterface) {
//                Log.d(TAG, "onShow: fix the dimens!");
//                dialogView.fixDialogDimens();
//            }
//        });
//        theDialog.getWindow().getAttributes().windowAnimations = R.style.DataRangeAnimation;
//        theDialog.show();
//
//    }
//}
