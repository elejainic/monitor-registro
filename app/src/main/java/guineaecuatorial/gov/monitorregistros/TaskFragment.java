package guineaecuatorial.gov.monitorregistros;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mikepenz.actionitembadge.library.ActionItemBadge;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.helper.ItemTouchStuffs.SimpleItemTouchHelperCallback;
import guineaecuatorial.gov.monitorregistros.helper.ItemTouchStuffs.SimpleSectionedItemTouchHelperRecyclerViewAdapter;
import guineaecuatorial.gov.monitorregistros.helper.taskAvail.REQ5003947_Helper;
import guineaecuatorial.gov.monitorregistros.helper.taskAvail.RESP5003947_Helper;
import guineaecuatorial.gov.monitorregistros.helper.taskMy.REQ5003945_Helper;
import guineaecuatorial.gov.monitorregistros.helper.taskMy.RESP5003945_Helper;
import guineaecuatorial.gov.monitorregistros.model.taskAvail.RESP5003947_Model;
import guineaecuatorial.gov.monitorregistros.model.taskMy.RESP5003945_Model;
import guineaecuatorial.gov.monitorregistros.network.Networkhelper;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the
 * interface.
 */
public class TaskFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

private static final String TAG = "TaskFragment";
// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private static final String ARG_PARAM1 = "param1";
//private static final String ARG_PARAM2 = "param2";
private static ProgressBar progressBar;
// TODO: Rename and change types of parameters
//private String mParam1;
//private OnFragmentInteractionListener mListener;
//private String mParam2;
/**
 * The fragment's ListView/Grid
 */
private RecyclerView recyclerView;
private JSONObject json;
private List<RESP5003945_Model.minhas_tarefasModel> myTasksArrayList = new ArrayList<>();
private List<RESP5003947_Model.tarefas_disponiveisModel> availTasksArrayList = new ArrayList<>();
private SwipeRefreshLayout swipeToRefesh;
private SimpleSectionedItemTouchHelperRecyclerViewAdapter mSectionedAdapter;
private View.OnClickListener mOnClickListener;

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
public TaskFragment() {
}

/**
 * The Adapter which will be used to populate the ListView/GridView with
 * Views.
 */
// TODO: Rename and change types of parameters
public static Fragment newInstance() {
	//	Bundle args = new Bundle();
//	args.putString(ARG_PARAM1, param1);
//	args.putString(ARG_PARAM2, param2);
//	fragment.setArguments(args);
	return new TaskFragment();
}

@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
//	if (getArguments() != null) {
//		mParam1 = getArguments().getString(ARG_PARAM1);
//		mParam2 = getArguments().getString(ARG_PARAM2);
//	}

	// TODO: Change Adapter to display your content
//	mAdapter = new ArrayAdapter<>(getActivity(), R.layout.list_item, R.id.listagem_tv_name, DummyContent.ITEMS);
}

@Override
public void onAttach(Context context) {
	super.onAttach(context);
	progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_spinner);

}

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View view = inflater.inflate(R.layout.fragment_task_list, container, false);
	// Set the adapter
	recyclerView = (RecyclerView) view.findViewById(R.id.list);
	swipeToRefesh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_task_list);
	swipeToRefesh.setOnRefreshListener(this);
	// Set OnItemClickListener so we can be notified on item clicks
	return view;
}

@Override
public void onViewCreated(View view, Bundle savedInstanceState) {
	super.onViewCreated(view, savedInstanceState);

	if (savedInstanceState == null) {
		swipeToRefesh.setRefreshing(true);
		progressBar.setVisibility(View.VISIBLE);
		LinearLayoutManager llm = new LinearLayoutManager(recyclerView.getContext());
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		recyclerView.setLayoutManager(llm);
		mSectionedAdapter = new SimpleSectionedItemTouchHelperRecyclerViewAdapter(getContext(), R.layout.section, R.id.section_text, myTasksArrayList, availTasksArrayList);
		recyclerView.setAdapter(mSectionedAdapter);
		DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
		defaultItemAnimator.setAddDuration(1000);
		defaultItemAnimator.setRemoveDuration(500);
		recyclerView.setItemAnimator(defaultItemAnimator);
		ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mSectionedAdapter);
		ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(callback);
		mItemTouchHelper.attachToRecyclerView(recyclerView);
		networkCall4ListTask();
	}

}

private void networkCall4ListTask() {
	if (!LoginActivity.sessid.equals("")) {
		new Thread(new Runnable() {
			public void run() {
				new Networkhelper(getActivity()).callWebservice(REQ5003945_Helper.prepareModel(0, LoginActivity.sessid, 0, 0, 0, "", "", "", "", "", "30-05-2015", "30-10-2015", ""), getTaskListFragment(0));
			}
		}).start();
		new Thread(new Runnable() {
			public void run() {
				new Networkhelper(getActivity()).callWebservice(REQ5003947_Helper.prepareModel(0, LoginActivity.sessid, 0, 0, "", "", "", "", "", "30-05-2015", "30-10-2015", ""), getTaskListFragment(1));
			}
		}).start();
	} else {
		progressBar.setVisibility(View.GONE);
		swipeToRefesh.setRefreshing(false);
		Snackbar snackbar = Snackbar.make(progressBar, "Por favor, autenticar", Snackbar.LENGTH_LONG).setAction("Aqui", mOnClickListener).setActionTextColor(Color.RED);
		snackbar.show();
	}
	mOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (isAdded()) {
				getActivity().startActivityForResult(new Intent(getActivity(), LoginActivity.class), 666);
				getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
			}
		}
	};
}

private void setupRecyclerView() {
//	if (recyclerView.getAdapter() == null) {
////		recyclerView.setHasFixedSize(true);
//
//	} else {
	if (!myTasksArrayList.isEmpty()) {
		MenuItem menuItem = ((Toolbar) getActivity().findViewById(R.id.tool_bar)).getMenu().findItem(R.id.item_samplebadge);
		ActionItemBadge.update(getActivity(), menuItem, GoogleMaterial.Icon.gmd_account_box, ActionItemBadge.BadgeStyles.RED, myTasksArrayList.size());
		mSectionedAdapter.setMyTasks(myTasksArrayList);
	}
		if (!availTasksArrayList.isEmpty()) mSectionedAdapter.setAvailTask(availTasksArrayList);
//	}
//	availTasks.addAll(availTasksArrayList);
//	myTasks.addItem(new Row(12, "15.2 Processo alfa 2", "fsad", "", "fd", null, "", ""));
//	myTasks.addItem(new Row(12,"15.3 Geral process","fsad","","fd","",null,""));
}

@NonNull
private Callback getTaskListFragment(final int type) {
	return new Callback() {
		@Override
		public void onFailure(Request request, IOException e) {
			if (isAdded()) getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					progressBar.setVisibility(View.GONE);
					swipeToRefesh.setRefreshing(false);
					Toast.makeText(getActivity(), "Não foi possivel conectar com servidor", Toast.LENGTH_LONG).show();

				}
			});
		}

		@Override
		public void onResponse(Response response) throws IOException {
			try {
				json = new JSONObject(response.body().string());
				if (BuildConfig.DEBUG) Log.e(TAG, "onResponse (line 87): " + json);
				if (type == 0) {
					ArrayList<RESP5003945_Model.minhas_tarefasModel> minhas_tarefasModels = RESP5003945_Helper.parseModel(json);
					if (minhas_tarefasModels != null) {
						myTasksArrayList.clear();
						myTasksArrayList.addAll(minhas_tarefasModels);
					}
				} else
				if (type == 1) {
					ArrayList<RESP5003947_Model.tarefas_disponiveisModel> tarefas_disponiveisModels = RESP5003947_Helper.parseModel(json);
					if (tarefas_disponiveisModels != null) {
						availTasksArrayList.clear();
						availTasksArrayList.addAll(tarefas_disponiveisModels);
					}
				}
				try {
					if (isAdded()) getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							progressBar.setVisibility(View.GONE);
							swipeToRefesh.setRefreshing(false);
							if (type == 0) {
								if (null != myTasksArrayList) setupRecyclerView();
							} else if (type == 1) if (null != availTasksArrayList)
								setupRecyclerView();

						}
					});
				} catch (Exception e) {
					e.printStackTrace();
					if (isAdded()) getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getContext(), "Erro ", Toast.LENGTH_LONG).show();
							progressBar.setVisibility(View.GONE);
							swipeToRefesh.setRefreshing(false);
						}
					});
				}

			} catch (JSONException e) {
				e.printStackTrace();
				if (isAdded()) getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getContext(), "Erro ", Toast.LENGTH_LONG).show();
						progressBar.setVisibility(View.GONE);
						swipeToRefesh.setRefreshing(false);
					}
				});

			}

		}
	};
}


@Override
public void onRefresh() {
	networkCall4ListTask();
}

}
