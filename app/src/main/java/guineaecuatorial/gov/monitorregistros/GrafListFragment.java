/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package guineaecuatorial.gov.monitorregistros;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;
import com.anupcowkur.reservoir.ReservoirPutCallback;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.gson.reflect.TypeToken;
import com.joanzapata.iconify.widget.IconTextView;
import com.mikepenz.iconics.IconicsDrawable;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.adapter.SimpleSectionedRecyclerViewAdapter;
import guineaecuatorial.gov.monitorregistros.helper.subarea.REQ5003529_Helper;
import guineaecuatorial.gov.monitorregistros.helper.subarea.RESP5003529_Helper;
import guineaecuatorial.gov.monitorregistros.model.GrafItemContent;
import guineaecuatorial.gov.monitorregistros.model.subarea.RESP5003529_Model;
import guineaecuatorial.gov.monitorregistros.model.subarea.Row;
import guineaecuatorial.gov.monitorregistros.network.Connectivity;
import guineaecuatorial.gov.monitorregistros.network.Networkhelper;


public class GrafListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


private static final String ARG_PARAM1 = "xVals";
public static int idSection = 0;

public String idSub = "58";
JSONObject json;
private List<RESP5003529_Model.subareasModel> subareasModelArrayList = new ArrayList<>();

private RecyclerView rv;
private SwipeRefreshLayout swipeToRefesh;
private SimpleSectionedRecyclerViewAdapter mSectionedAdapter;

public static Fragment newInstance(String id) {
	GrafListFragment fragment = new GrafListFragment();
	Bundle args = new Bundle();
	args.putString(ARG_PARAM1, id);
	fragment.setArguments(args);
	return fragment;

}


@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	Log.e("GrafListFragment", "onCreate (line 132): ");
	GraficoListActivity.progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_spinner);
	//Make progress bar appear when you need it
	if (getArguments() != null) {
		idSub = getArguments().getString(ARG_PARAM1);
	}
}

@Override
public void onResume() {
	super.onResume();
//		MyApplication.getInstance().trackScreenView("Graf List Fragment");
	if (null != subareasModelArrayList) setupRecyclerView();
}

@Nullable
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	Log.e("GrafListFragment", "onCreateView (line 141): ");
	View view = inflater.inflate(R.layout.fragment_list_subarea, container, false);
	swipeToRefesh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_task_list);
	swipeToRefesh.setOnRefreshListener(this);
	return view;
	}

@Override
public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
	super.onViewCreated(view, savedInstanceState);
	rv = (RecyclerView) view.findViewById(R.id.recyclerview);
	if (savedInstanceState == null) {
		if (!BuildConfig.FLAVOR.equals("eBau")) {
			GraficoListActivity.progressBar.setVisibility(View.VISIBLE);
			swipeToRefesh.setRefreshing(true);
			onRefresh();

		} else {
			String JSON = "{\"Result\":{\"rows\":{\"row\": {\"status\":true, \"subareas\": [{\"nome\":\"Pedidos\",\"grafs\":{\n" +
					              "\"rows\": [" +
					              "{ \"nome\": \"Evolução dos Pedidos\", \"tipo\": \"LINHA\", \"serv\": \"GRAF4\", \"periodo\": \"SEMANA\" }," +
					              " { \"nome\": \"Pedidos Licença Por Orgânica\", \"tipo\": \"PIE\", \"serv\": \"GRAF9\", \"periodo\": \"SEMANA\" }, " +
					              "{ \"nome\": \"Pedidos Licença Por Utilizador\", \"tipo\": \"PIE\", \"serv\": \"GRAF8\", \"periodo\": \"SEMANA\" }, " +
					              "{ \"nome\": \"Pedidos Por Tipo Licença\", \"tipo\": \"PIE\", \"serv\": \"GRAF12\", \"periodo\": \"SEMANA\" }," +
					              " { \"nome\": \"Pedidos Por Valor Investimento\", \"tipo\": \"BAR\", \"serv\": \"GRAF13\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Pedidos Por Nr Trabalhadores\", \"tipo\": \"PIE\", \"serv\": \"GRAF14\", \"periodo\": \"SEMANA\" }," +
					              " { \"nome\": \"Processos pendentes por Orgânica\", \"tipo\": \"LISTA\", \"serv\": \"GRAF19\", \"periodo\": \"SEMANA\" }]}}, " +
					              "{\"nome\":\"Finanças\",\"grafs\":{" +
					              "\"rows\":[{ \"nome\": \"Receitas por Orgânica\", \"tipo\": \"BAR\", \"serv\": \"GRAF18\", \"periodo\": \"SEMANA\" },{ \"nome\": \"Evolucao receitas por Orgânica\", \"tipo\": \"LINHA\", \"serv\": \"GRAF20\", \"periodo\": \"SEMANA\" }]}}," +
					              "{\"nome\":\"Licenças\",\"grafs\":{\n" +
					              "\"rows\":[{ \"nome\": \"Evolução Licenças Por Província\", \"tipo\": \"LINHA\", \"serv\": \"GRAF5\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Licenças Autorizadas Por Orgânica\", \"tipo\": \"PIE\", \"serv\": \"GRAF10\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Licenças Autorizadas Por Utilizador\", \"tipo\": \"BAR\", \"serv\": \"GRAF7\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Licenças Por Tipo Licença\", \"tipo\": \"PIE\", \"serv\": \"GRAF1\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Licenças Por Valor Investimento\", \"tipo\": \"PIE\", \"serv\": \"GRAF2\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Licenças Por Nr Trabalhadores\", \"tipo\": \"BAR\", \"serv\": \"GRAF3\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Tempo médio decisão por Orgânica\", \"tipo\": \"BAR\", \"serv\": \"GRAF15\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Tempo médio tipo licença por Orgânica\", \"tipo\": \"BAR\", \"serv\": \"GRAF16\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Tempo médio parecer por Orgânica\", \"tipo\": \"BAR\", \"serv\": \"GRAF17\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Evolução do tempo de espera por Orgânica\", \"tipo\": \"LINHA\", \"serv\": \"GRAF21\", \"periodo\": \"SEMANA\" }," +
					              "{ \"nome\": \"Empresas Licenciadas\", \"tipo\": \"LISTA\", \"serv\": \"GRAF6\", \"periodo\": \"SEMANA\" }]}}]}}}}";
			try {
				if (null != subareasModelArrayList) subareasModelArrayList.clear();
				subareasModelArrayList = RESP5003529_Helper.parseModel(new JSONObject(JSON));
				Log.e("GrafListFragment", "onCreateView (line 130): ");
			} catch (JSONException e) {
				e.printStackTrace();
				Log.e("GrafListFragment", "onCreateView (line 132): ");
			}
			if (null != subareasModelArrayList) setupRecyclerView();
			}
		}
	rv.setAdapter(mSectionedAdapter);
	}

@Override
public void onStart() {
	super.onStart();
	Log.e("GrafListFragment", "onStart (line 190): ");
	rv.setAdapter(mSectionedAdapter);
}

private void setupRecyclerView() {
	rv.setHasFixedSize(true);
	LinearLayoutManager llm = new LinearLayoutManager(rv.getContext());
	llm.setOrientation(LinearLayoutManager.VERTICAL);
	rv.setLayoutManager(llm);
	List<SimpleSectionedRecyclerViewAdapter.Section> sections = new ArrayList<>();
	int i = 0;
	GrafItemContent grafItemContent = new GrafItemContent();
	for (RESP5003529_Model.subareasModel subareasModel : subareasModelArrayList) {
		sections.add(new SimpleSectionedRecyclerViewAdapter.Section(i, subareasModel.getNome(), subareasModel.getId()));
		for (int j = 0; j < subareasModel.getGrafs().getRows().size(); j++) {
			grafItemContent.addItem(subareasModel.getGrafs().getRows().get(j));
			i++;
		}
		}
	SimpleStringRecyclerViewAdapter mAdapter = new SimpleStringRecyclerViewAdapter(getContext(), grafItemContent.ITEMS, sections);
	SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
	mSectionedAdapter = new SimpleSectionedRecyclerViewAdapter(getContext(), R.layout.section, R.id.section_text, mAdapter);
	mSectionedAdapter.setSections(sections.toArray(dummy));
	rv.setAdapter(mSectionedAdapter);
//	if (!subareasModelArrayList.isEmpty())
//		mAdapter.setmValues(grafItemContent.ITEMS,sections);
}


@Override
public void onDetach() {
	super.onDetach();

}

@Override
public void onRefresh() {
	Type resultType = new TypeToken<List<RESP5003529_Model.subareasModel>>() {
	}.getType();
	Reservoir.getAsync(idSub, resultType, new ReservoirGetCallback<List<RESP5003529_Model.subareasModel>>() {

		@Override
		public void onSuccess(List<RESP5003529_Model.subareasModel> subareasModel) {
			//success
			subareasModelArrayList.clear();
			subareasModelArrayList.addAll(subareasModel);
			setupRecyclerView();

		}

		@Override
		public void onFailure(Exception e) {
			//error
		}
	});
	if (Connectivity.isConnected(getContext())) {
		new Thread(new Runnable() {
				public void run() {
					new Networkhelper(getActivity()).callWebservice(REQ5003529_Helper.prepareModel(LoginActivity.sessid, Integer.parseInt(idSub)), getRESP5003529Callback());
				}
		}).start();
	} else {
		if (getView() != null)
			Snackbar.make(getView(), R.string.no_internet, Snackbar.LENGTH_SHORT).show();
		}
}

@NonNull
private Callback getRESP5003529Callback() {
	return new Callback() {
		@Override
		public void onFailure(Request request, IOException e) {
			if (isAdded()) getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					GraficoListActivity.progressBar.setVisibility(View.GONE);
					swipeToRefesh.setRefreshing(false);
					Toast.makeText(getActivity(), "Modo offline!", Toast.LENGTH_LONG).show();

				}
			});

		}

		@Override
		public void onResponse(Response response) throws IOException {
				try {
					json = new JSONObject(response.body().string());
					if (idSub.equals("404"))
						json = new JSONObject("{\"Status\":\"true\",\"Result\":\"{\\\"rows\\\":{\\\"row\\\":{\\\"status\\\":true,\\\"subareas\\\":[{\\\"nome\\\":\\\"Nacimiento Por Fecha\\\",\\\"id\\\":407,\\\"grafs\\\":\\\"{\\\"rows\\\":{\\\"row\\\":{\\\"id\\\":733,\\\"nome\\\":\\\"Fecha\\\",\\\"tipo\\\":\\\"BAR\\\",\\\"serv\\\":\\\"reg_ge.MV_REG_RCNF\\\",\\\"periodo\\\":\\\"SEMANA\\\",\\\"progress\\\":\\\"74\\\",\\\"total\\\":130,\\\"icon\\\":\\\"https://cdn0.iconfinder.com/data/icons/icostrike-characters/512/baby-512.png\\\",\\\"cor\\\":\\\"#FE9507\\\"}}}\\\"},{\\\"nome\\\":\\\"Nacimiento Por Localidad\\\",\\\"id\\\":405,\\\"grafs\\\":\\\"{\\\"rows\\\":{\\\"row\\\":{\\\"id\\\":734,\\\"nome\\\":\\\"Localidad\\\",\\\"tipo\\\":\\\"BAR\\\",\\\"serv\\\":\\\"reg_ge.MV_REG_RCNL\\\",\\\"periodo\\\":\\\"SEMANA\\\",\\\"progress\\\":132,\\\"total\\\":90,\\\"icon\\\":\\\"http://static1.squarespace.com/static/50b167efe4b094e9770357c2/t/54b407dde4b0418eb564fe7b/1421084637519/new-mom-support-group_icon.png\\\",\\\"cor\\\":\\\"#FF2552\\\"}}}\\\"},{\\\"nome\\\":\\\"Nacimientos Por Oficina De Registro\\\",\\\"id\\\":416,\\\"grafs\\\":\\\"{\\\"rows\\\":{\\\"row\\\":{\\\"id\\\":735,\\\"nome\\\":\\\" Oficina de registro\\\",\\\"tipo\\\":\\\"BAR\\\",\\\"serv\\\":\\\"reg_ge.MV_RGE_RCO\\\",\\\"periodo\\\":\\\"SEMANA\\\",\\\"progress\\\":165,\\\"total\\\":180,\\\"icon\\\":\\\"http://goshenvalley.org/wp-content/themes/goshen/landing_page_assets/img/icon-birth-family.png\\\",\\\"cor\\\":\\\"#009688\\\"}}}\\\"}]}}}\"}");
					if (BuildConfig.DEBUG)
						Log.e("GrafListFragment", "onResponse (line 87): " + json);
					List<RESP5003529_Model.subareasModel> subareasModel = RESP5003529_Helper.parseModel(json);
					if (null != subareasModel) {
						subareasModelArrayList.clear();
						subareasModelArrayList.addAll(subareasModel);
						Reservoir.putAsync(idSub, subareasModel, new ReservoirPutCallback() {
							@Override
							public void onSuccess() {
								//success
								Log.e("GrafListFragment", "Reservoir onSuccess (line 270): ");
							}

							@Override
							public void onFailure(Exception e) {
								//error
								Log.e("GrafListFragment", "Reservoir onFailure (line 276): ");
							}
						});
					}
					try {
						if (isAdded()) getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								GraficoListActivity.progressBar.setVisibility(View.GONE);
								swipeToRefesh.setRefreshing(false);
								if (null != subareasModelArrayList) setupRecyclerView();
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
						if (isAdded()) getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(getContext(), "Erro ", Toast.LENGTH_LONG).show();
								GraficoListActivity.progressBar.setVisibility(View.GONE);
								swipeToRefesh.setRefreshing(false);
							}
						});
					}

				} catch (JSONException e) {
					e.printStackTrace();
					if (isAdded()) getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getContext(), "Erro ", Toast.LENGTH_LONG).show();
							GraficoListActivity.progressBar.setVisibility(View.GONE);
							swipeToRefesh.setRefreshing(false);
						}
					});

				}

			}
	};
}

public class SimpleStringRecyclerViewAdapter extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

	private TypedValue mTypedValue = new TypedValue();
	private List<SimpleSectionedRecyclerViewAdapter.Section> mSections = new ArrayList<>();

	private int mBackground;
	private List<Row> mValues = new ArrayList<>();


	public SimpleStringRecyclerViewAdapter(Context context, List<Row> items, List<SimpleSectionedRecyclerViewAdapter.Section> sections) {
		try {
			context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
			mBackground = mTypedValue.resourceId;
			mValues = items;
			mSections = sections;
		} catch (Exception e) {
			e.printStackTrace();
		}

		}

	private int positionToSectionedPosition(int position) {
		int offset = 0;
		for (int i = 0; i < mSections.size(); i++) {
			if (mSections.get(i).getFirstPosition() > position) {
				break;
			}
//		++offset;
			offset = mSections.get(i).getIdSection();
		}
		return offset;
//	return position + offset;
	}

	public Row getValueAt(int position) {
		return mValues.get(position);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_graf_item, parent, false);
		view.setBackgroundResource(mBackground);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, final int position) {
		holder.mBoundString = getValueAt(position).getServ();
		holder.mNomeTextView.setText(getValueAt(position).getNome());
		try {
			if (getValueAt(position).getProgress() != null) {
				holder.mProgressTextView.setText(getValueAt(position).getProgress());
				if (getValueAt(position).getTotal() != null) {
					holder.mProgressLinearListGrafItem.setVisibility(View.VISIBLE);
//					holder.mTotalTextView.setVisibility(View.VISIBLE);
					int colorProgress = 0;
					try {
						colorProgress = Color.parseColor(getValueAt(position).getCor());
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (colorProgress == 0)
						colorProgress = ContextCompat.getColor(getContext(), R.color.colorAccent);
					holder.numberProgressBar.setReachedBarColor(colorProgress);
					holder.numberProgressBar.setProgressTextColor(colorProgress);
					holder.mTotalTextView.setText(String.format("%s", getValueAt(position).getTotal()));
					holder.mIconFontView.setTextColor(colorProgress);
					if (getValueAt(position).getIcon().contains("http")) {
						holder.mImageView.setVisibility(View.VISIBLE);
						holder.mImageView.setColorFilter(new PorterDuffColorFilter(colorProgress, PorterDuff.Mode.SRC_IN));
						Picasso.with(holder.itemView.getContext()).load(getValueAt(position).getIcon()).placeholder(new IconicsDrawable(getActivity(), "faw-area-chart").sizeDp(24)).fit().centerInside().into(holder.mImageView);
						holder.mIconFontView.setVisibility(View.GONE);
					}
					holder.numberProgressBar.setMax(Integer.parseInt(getValueAt(position).getTotal()));
					if (Integer.parseInt(getValueAt(position).getProgress()) > Integer.parseInt(getValueAt(position).getTotal())) {
						holder.numberProgressBar.setProgress(Integer.parseInt(getValueAt(position).getTotal()));
						holder.numberProgressBar.setPrefix("> ");
					} else
						holder.numberProgressBar.setProgress((Integer.parseInt(getValueAt(position).getProgress())));

				}
			} else {
				holder.mIconFontView.setVisibility(View.VISIBLE);
				holder.mImageView.setVisibility(View.GONE);
				}
		} catch (NumberFormatException e) {
			e.printStackTrace();
			}
		holder.mView.setOnClickListener(new View.OnClickListener() {


			@Override
			public void onClick(View v) {
				Row valueAt = getValueAt(position);
//					MyApplication.getInstance().trackEvent("[" + GraficoListActivity.ambientAtual + "] Escolha Graf!", "«" + valueAt.getNome() + "» ID: " + valueAt.getId(), "Track event em Graf List Fragment");
				idSection = positionToSectionedPosition(position);
				Intent intent = new Intent(getContext(), GraficoDetailActivity.class);
				Bundle bundle = new Bundle();
				bundle.putParcelable(GraficoDetailActivity.ARG_POSITION, new GrafItemContent.GrafItem(valueAt.getId(), valueAt.getNome(), valueAt.getTipo(), valueAt.getPeriodo()));
				intent.putExtras(bundle);
				startActivity(intent);
				(getActivity()).overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

			}
		});
//		.setDrawable(R.id.action_bar_subtitle);
//			new IconDrawable(holder.mView.getContext(), Iconify.IconValue.fa_PIE_chart).color(0xff137916).sizeRes(R.dimen.list_item_avatar_size)
//			holder.mIconFontView.setCompoundDrawablePadding(30);
		if (mValues.get(position).getTipo().equals("PIE")) {
			holder.mIconFontView.setText("{fa-pie-chart}");
//				holder.mIconFontView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorful_4));
		}
		if (mValues.get(position).getTipo().equals("LISTA")) {
			holder.mIconFontView.setText("{fa-list-alt}");
//				holder.mIconFontView.setTextColor(Color.GRAY);
		}
		if (mValues.get(position).getTipo().equals("BAR")) {
			holder.mIconFontView.setText("{fa-bar-chart}");
//				holder.mIconFontView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
		}
		if (mValues.get(position).getTipo().equals("STACKBAR")) {
			holder.mIconFontView.setText("{fa-bar-chart-o}");
//				holder.mIconFontView.setTextColor(Color.argb(255, 255, 125, 21));
			holder.mIconFontView.setShadowLayer(3, 3, 1, R.color.colorAccent);

		}
		if (mValues.get(position).getTipo().equals("LINHA")) {
			holder.mIconFontView.setText("{fa-line-chart}");
//				holder.mIconFontView.setTextColor(Color.BLUE);
		}
		}

	@Override
	public int getItemCount() {
		return mValues.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public final View mView;
		public final ImageView mImageView;
		public IconTextView mIconFontView;
		public LinearLayout mProgressLinearListGrafItem;
		public TextView mTotalTextView;
		public TextView mProgressTextView;
		public TextView mNomeTextView;
		public String mBoundString;
		public NumberProgressBar numberProgressBar;

		public ViewHolder(View view) {
			super(view);
			mView = view;
			mImageView = (ImageView) view.findViewById(R.id.avatar_list_graf);
			mIconFontView = (IconTextView) view.findViewById(R.id.joan_iconFont);
			mNomeTextView = (TextView) view.findViewById(R.id.txt_nome_list_graf_item);
			mTotalTextView = (TextView) view.findViewById(R.id.txt_total_list_graf);
			mProgressLinearListGrafItem = (LinearLayout) view.findViewById(R.id.progress_line_list_graf_item);
			mProgressTextView = (TextView) view.findViewById(R.id.txt_progress_graf);
			numberProgressBar = (NumberProgressBar) view.findViewById(R.id.number_progress_bar);
			mImageView.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN));
//			mNomeTextView.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), "Raleway-Regular.ttf"));
			mProgressTextView.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), "Roboto-Regular.ttf"));
		}

		@Override
		public String toString() {
			return super.toString() + " '" + mIconFontView.getText();
		}

	}
	}
}
