package guineaecuatorial.gov.monitorregistros.dashboardchartitem;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.util.ColorTemplate;


public class PieChartItem extends ChartItem {

private final boolean bool;
private final String descName;
private Typeface mTf;
private SpannableString mCenterText;

public PieChartItem(ChartData<?> cd, Context c, boolean b, String descName) {
	super(cd);
	bool = b;
	this.descName = descName;
	mTf = Typeface.createFromAsset(c.getAssets(), "OpenSans-Regular.ttf");
//		mCenterText = generateCenterText();
}

@Override
public int getItemType() {
	return TYPE_PIECHART;
}

@Override
public View getView(int position, View convertView, final Context c) {
	ViewHolder holder;
	if (convertView == null) {
		holder = new ViewHolder();
		convertView = LayoutInflater.from(c).inflate(R.layout.list_item_piechart, null, false);
		holder.chart = (PieChart) convertView.findViewById(R.id.chart);
		convertView.setTag(holder);

	} else {
		holder = (ViewHolder) convertView.getTag();
		}
	holder.chart.setTouchEnabled(bool);
	// apply styling
	holder.chart.setDescription("");
	holder.chart.setHoleRadius(52f);
	holder.chart.setTransparentCircleRadius(57f);
	holder.chart.setDragDecelerationFrictionCoef(0.95f);
	holder.chart.setCenterText(mCenterText);
	holder.chart.setCenterTextTypeface(mTf);
	holder.chart.setCenterTextSize(12f);
	holder.chart.setUsePercentValues(true);
	holder.chart.setDrawSliceText(false);
	holder.chart.setExtraOffsets(5, 10, 50, 10);
	holder.chart.setCenterTextRadiusPercent(0.9f);
	holder.chart.setNoDataText(c.getString(R.string.no_data_text_description));
	final ViewHolder finalHolder = holder;
	mChartData.setValueFormatter(new PercentFormatter());
	mChartData.setValueTypeface(mTf);
	mChartData.setValueTextSize(9f);
	mChartData.setValueTextColor(ContextCompat.getColor(c, R.color.colorAccent));
	// set data
	holder.chart.setData((PieData) mChartData);
	if (holder.chart.getData().getYValueSum() != 0) setCenterTextTotal(finalHolder);
	holder.chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
		@Override
		public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
			NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###,###,###,###,###.#");
			finalHolder.chart.setCenterText(finalHolder.chart.getXValue(e.getXIndex()) + "\n" + nf.format((e.getData() == null ? e.getVal() : (BigDecimal) e.getData())) + " = " + nf.format((double) finalHolder.chart.getPercentOfTotal(e.getVal())) + "% \n");
			finalHolder.chart.setCenterTextColor(ContextCompat.getColor(c, ColorTemplate.COLORFUL_COLORS[e.getXIndex()]));
		}

		@Override
		public void onNothingSelected() {
			setCenterTextTotal(finalHolder);
		}
	});
	holder.chart.highlightValues(null);
	Legend l = holder.chart.getLegend();
	l.setPosition(LegendPosition.RIGHT_OF_CHART);
	l.setWordWrapEnabled(true);
	l.setYEntrySpace(0f);
	l.setYOffset(0f);
	holder.chart.setDescriptionPosition(descName.length() * 3 + 310f, 20f);
	holder.chart.setDescription(descName);
	holder.chart.setDescriptionTypeface(mTf);
	holder.chart.setDescriptionTextSize(11f);
	// do not forget to refresh the chart
	// holder.chart.invalidate();
	holder.chart.animateY(1700, Easing.EasingOption.EaseOutBack);
	holder.chart.animateX(700);
	return convertView;
}

private void setCenterTextTotal(ViewHolder finalHolder) {
	NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###,###,###.###");
	double yValueSum = (double) finalHolder.chart.getData().getYValueSum();
	finalHolder.chart.setCenterText("Total:\n" + nf.format(yValueSum));
	finalHolder.chart.setCenterTextColor(Color.DKGRAY);
}
//private SpannableString generateCenterText() {
//	SpannableString s = new SpannableString("MPAndroidChart\ncreated by\nPhilipp Jahoda");
//	s.setSpan(new RelativeSizeSpan(1.6f), 0, 14, 0);
//	s.setSpan(new ForegroundColorSpan(ColorTemplate.VORDIPLOM_COLORS[0]), 0, 14, 0);
//	s.setSpan(new RelativeSizeSpan(.9f), 14, 25, 0);
//	s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, 25, 0);
//	s.setSpan(new RelativeSizeSpan(1.4f), 25, s.length(), 0);
//	s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), 25, s.length(), 0);
//	return s;
//}


private static class ViewHolder {
	PieChart chart;
}
}