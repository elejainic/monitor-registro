package guineaecuatorial.gov.monitorregistros.dashboardchartitem;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.ChartData;

import guineaecuatorial.gov.monitorregistros.R;


public class BarChartItem extends ChartItem {

private static final String TAG = "BarchartItem";
private final boolean bool;
private final String descName;
private Typeface mTf;


public BarChartItem(ChartData<?> cd, Context c, boolean b, String descName) {
	super(cd);
	bool = b;
	this.descName = descName;
	mTf = Typeface.createFromAsset(c.getAssets(), "OpenSans-Regular.ttf");
}

@Override
public int getItemType() {
	return TYPE_BARCHART;
	}

@Override
public View getView(int position, View convertView, Context c) {
	ViewHolder holder;
	if (convertView == null) {
		holder = new ViewHolder();
		convertView = LayoutInflater.from(c).inflate(R.layout.list_item_barchart, null, false);
		holder.chart = (BarChart) convertView.findViewById(R.id.chart);
		convertView.setTag(holder);

	} else {
		holder = (ViewHolder) convertView.getTag();
	}
	Legend l = holder.chart.getLegend();
	l.setWordWrapEnabled(true);
	l.setTypeface(mTf);
	l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
	l.setTextColor(Color.BLACK);
	l.setEnabled(false);
	// apply styling
	holder.chart.setDrawGridBackground(false);
	holder.chart.setDrawBarShadow(false);
	holder.chart.setTouchEnabled(bool);
	XAxis xAxis = holder.chart.getXAxis();
	xAxis.setPosition(XAxisPosition.BOTTOM);
	xAxis.setTypeface(mTf);
	xAxis.setDrawGridLines(false);
	xAxis.setDrawAxisLine(true);
	xAxis.setSpaceBetweenLabels(1);
	YAxis leftAxis = holder.chart.getAxisLeft();
	leftAxis.setTypeface(mTf);
	leftAxis.setLabelCount(5, false);
	leftAxis.setSpaceTop(20f);
	leftAxis.setEnabled(false);
	YAxis rightAxis = holder.chart.getAxisRight();
	rightAxis.setTypeface(mTf);
	rightAxis.setLabelCount(5, false);
	rightAxis.setSpaceTop(20f);
	rightAxis.setEnabled(false);
	mChartData.setValueTypeface(mTf);
	// set data
	holder.chart.setData((BarData) mChartData);
	Log.e(TAG, "getView (line 65): mchaWith " + holder.chart.getWidth());
	holder.chart.setDescriptionPosition(descName.length() * 3 + 310f, 20f);
	holder.chart.setDescription(descName);
	holder.chart.setDescriptionTypeface(mTf);
	holder.chart.setDescriptionTextSize(11f);
//		holder.chart.setDescriptionColor(ContextCompat.getColor(c,R.color.colorPrimaryDark));
	// do not forget to refresh the chart
//        holder.chart.invalidate();
	holder.chart.animateY(700);
	return convertView;
}

private static class ViewHolder {
	BarChart chart;
}
}
