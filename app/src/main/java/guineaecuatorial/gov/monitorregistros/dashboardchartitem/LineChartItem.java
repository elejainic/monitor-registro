package guineaecuatorial.gov.monitorregistros.dashboardchartitem;


import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.LineData;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.util.MyValueFormatter;


public class LineChartItem extends ChartItem {

private final boolean bool;
private final String descName;
private Typeface mTf;

public LineChartItem(ChartData<?> cd, Context c, boolean b, String descName) {
	super(cd);
	bool = b;
	this.descName = descName;
	mTf = Typeface.createFromAsset(c.getAssets(), "OpenSans-Regular.ttf");
}

@Override
public int getItemType() {
	return TYPE_LINECHART;
}

@Override
public View getView(int position, View convertView, Context c) {
	ViewHolder holder;
	if (convertView == null) {
		holder = new ViewHolder();
		convertView = LayoutInflater.from(c).inflate(R.layout.list_item_linechart, null, false);
		holder.chart = (LineChart) convertView.findViewById(R.id.chart);
		convertView.setTag(holder);

	} else {
		holder = (ViewHolder) convertView.getTag();
	}
	// apply styling
	// holder.chart.setValueTypeface(mTf);
	holder.chart.setDescription("");
	holder.chart.setDrawGridBackground(false);
	holder.chart.setTouchEnabled(bool);
	YAxis leftAxis = holder.chart.getAxisLeft();
	leftAxis.setTypeface(mTf);
	leftAxis.setTextSize(4);
	leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
	leftAxis.setValueFormatter(new MyValueFormatter());
	leftAxis.setDrawLabels(true);
	leftAxis.setStartAtZero(false);
	leftAxis.setEnabled(false);
	holder.chart.getAxisRight().setEnabled(false);
	XAxis xAxis = holder.chart.getXAxis();
	xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
	xAxis.setTypeface(mTf);
	xAxis.setDrawAxisLine(false);
	xAxis.setAvoidFirstLastClipping(true);
	// set data
	holder.chart.setData((LineData) mChartData);
	holder.chart.setDescriptionPosition(descName.length() * 3 + 310f, 20f);
	holder.chart.setDescription(descName);
	holder.chart.setDescriptionTypeface(mTf);
	holder.chart.setDescriptionTextSize(11f);
	// do not forget to refresh the chart
	// holder.chart.invalidate();
	holder.chart.animateX(1050);
	holder.chart.animateY(1000, Easing.EasingOption.EaseOutBack);
	return convertView;
	}

private static class ViewHolder {
	LineChart chart;
}
}