package guineaecuatorial.gov.monitorregistros;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.ArrayList;
import java.util.List;


/**
 * An activity representing a list of Graficos. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link } representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link GrafListFragment} and the item details
 * (if present) is a {@link }.
 * <p/>
 * This activity also implements the required
 * interface
 * to listen for item selections.
 * implements GraficoListFragment.Callbacks
 */
public class GraficoListActivity extends AppCompatActivity {


/**
 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
 * device.
 */

public final static int DEV = 0, DEP = 1, STAGE = 2, PROD = 3;
private static final String TAG = "GraficoListActivity";
public static int ambientAtual = DEV;
public static Context c;
public static ProgressBar progressBar;
private static long back_pressed;
public Toolbar toolbar;
TabLayout tabLayout;
private boolean mTwoPane;
private DrawerLayout mDrawerLayout;
private View viewById;
private NavigationView navigationView;

private Menu navMenu;
private ViewPager viewPager;
private TextView tvlabel;
private TextView loginTxt;
private SharedPreferences.Editor editor;
private SharedPreferences appPreferences;
private TextView t;
private MenuItem logoutMenuItem;

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	c = GraficoListActivity.this;
	setContentView(R.layout.activity_grafico_list);
	toolbar = (Toolbar) findViewById(R.id.tool_bar); // Attaching the layout to the toolbar object
	// Setting toolbar as the ActionBar with setSupportActionBar() call
	t = (TextView) findViewById(R.id.toolbar_title);
	setSupportActionBar(toolbar);                   // Setting toolbar as the ActionBar with setSupportActionBar() call
	mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	navigationView = (NavigationView) findViewById(R.id.nav_view);
	mTwoPane = mDrawerLayout == null;
	progressBar = (ProgressBar) findViewById(R.id.progress_spinner);
	ActionBar ab = getSupportActionBar();
	if (ab != null) {
		ab.setDisplayShowTitleEnabled(false);
		if (!mTwoPane) {
			ab.setHomeAsUpIndicator(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_menu).color(Color.WHITE).actionBar());
			ab.setDisplayHomeAsUpEnabled(true);
		}
	}
	setupLoginHeader(savedInstanceState);
	navMenu = navigationView.getMenu();
	if (!mTwoPane) {
		mDrawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);
		mDrawerLayout.openDrawer(GravityCompat.START);
	}
	try {
		navMenu.findItem(R.id.nav_Inicio).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_home));
		navMenu.findItem(R.id.nav_lista_tarefas).setIcon(new IconicsDrawable(c, FontAwesome.Icon.faw_tasks));
		navMenu.findItem(R.id.nav_consulta_processo).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_search_in_file));
		navMenu.findItem(R.id.nav_empleado_ver_rendimento).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_accounts));
		//// tambien se puede añadir imagenes así poniendo o escribiendo el nombre del icono o imagen
		navMenu.findItem(R.id.nav_Registro_Comercial).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_shopping_cart));
		navMenu.findItem(R.id.nav_registro_civil).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_face));
		navMenu.findItem(R.id.nav_Propiedad).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_city));
		navMenu.findItem(R.id.nav_Recursos_Humanos).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_male_female));
		navMenu.findItem(R.id.nav_Protocolo).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_play_for_work));
		navMenu.findItem(R.id.nav_Comunicacion).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_repeat));
		navMenu.findItem(R.id.nav_GestionNIF).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_receipt));
		navMenu.findItem(R.id.nav_Acompañamiento_Procesos).setIcon(new IconicsDrawable(c, GoogleMaterial.Icon.gmd_panorama_wide_angle));
		logoutMenuItem = navMenu.findItem(R.id.action_menu_logout);
		logoutMenuItem.setIcon(new IconicsDrawable(c, FontAwesome.Icon.faw_sign_out));
		logoutMenuItem.setVisible(!LoginActivity.email.equals(""));
		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(MenuItem item) {
//				item.setChecked(true);
				navMenu.setGroupCheckable(0, true, true);
				switch (item.getItemId()) {
					case R.id.nav_lista_tarefas:
						callListadeTarefas(viewPager);
						if (!mTwoPane) mDrawerLayout.closeDrawers();
						break;
					case R.id.nav_empleado_ver_rendimento:
						callTasksByColaboradores(viewPager);
						if (!mTwoPane) mDrawerLayout.closeDrawers();
						break;
					case R.id.nav_registro_civil:
						setupViewPager(viewPager, 1);
						break;
					case R.id.nav_Registro_Comercial:
						setupViewPager(viewPager, 2);
						break;
//					case R.id.nav_Propiedad:
//						setupViewPager(viewPager, 3);
//						break;
					case R.id.nav_Recursos_Humanos:
						setupViewPager(viewPager, 4);
						break;
					case R.id.nav_Protocolo:
						setupViewPager(viewPager, 5);
						break;
					case R.id.nav_Comunicacion:
						setupViewPager(viewPager, 6);
						break;
//					case R.id.nav_GestionNIF:
//						setupViewPager(viewPager, 7);
//						break;
//					case R.id.nav_Acompañamiento_Procesos:
//						setupViewPager(viewPager, 8);
//						break;
					case R.id.action_menu_logout:
						logoutSnnipet();
						break;
					default:
						Toast.makeText(getApplicationContext(), "Não está implementado", Toast.LENGTH_LONG).show();
				}
				return false;
			}
		});
//		Marcos: truque para fazer refresh e funcionar;
		navMenu.removeItem(R.id.loading);
		navMenu.setGroupCheckable(1, true, true);
	} catch (Exception e) {
		e.printStackTrace();
	}
	// fragment4 = new MainActivityFragment();
//        getSupportFragmentManager().beginTransaction().replace(R.id.grafico_list, fragment).commit();
	toolbar = (Toolbar) findViewById(R.id.tool_bar);
	TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
	Typeface myTypeface = Typeface.createFromAsset(getAssets(), "Raleway-Regular.ttf");
	toolbarTitle.setTypeface(myTypeface);
//
//        TextView fecha_header = (TextView) findViewById(R.id.fecha);
//        Typeface myTypeface1 = Typeface.createFromAsset(getAssets(), "Raleway-SemiBold.ttf");
//        fecha_header.setTypeface(myTypeface1);
//        getSupportFragmentManager().beginTransaction().replace(R.id.grafico_list, fragment4).commit();
//        SpannableString s = new SpannableString("My Title");
//        s.setSpan(new TypefaceSpan(this, "God"), 0, s.length(),
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        // Update the action bar title with the TypefaceSpan instance
//        ActionBar actionBar = getActionBar();
//        actionBar.setTitle(s);
	//setContentView(R.layout.fragment_main);
	// View vista =  inflater.inflate(R.layout.fragment_main, container, false);
//
	// TextView texto = (TextView) findViewById(R.id.fecha);
//        String date= DateFormat.getDateTimeInstance().format(new Date());
//        Calendar c = Calendar.getInstance();
//        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
//        String formattedDate = df.format(c.getTime());
//        texto.setText(formattedDate);
	viewPager = (ViewPager) findViewById(R.id.htab_viewpager);
	tabLayout = (TabLayout) findViewById(R.id.tabLayout);
	setupViewPager(viewPager, 0);
//	findViewById(R.id.htab_viewpager).setOnClickListener(new View.OnClickListener() {
//		@Override
//		public void onClick(View v) {
//			startActivity(new Intent(GraficoListActivity.this, LoginActivity.class));
//		}
//	});
//	findViewById(R.id.tool_bar).setOnClickListener(new View.OnClickListener() {
//		@Override
//		public void onClick(View v) {
//			startActivity(new Intent(GraficoListActivity.this, GraficoDetailActivity.class));
//		}
//	});
}

private void callTasksByColaboradores(ViewPager viewPager) {
	t.setText("Colaboradores");
	viewPager.removeAllViews();
	ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
	adapter.addFrag(TaskByColaboradoresFragment.newInstance(), "Colaboradores");
	viewPager.setAdapter(adapter);
	tabLayout.setupWithViewPager(viewPager);
	tabLayout.setVisibility(View.GONE);
}

private void callListadeTarefas(ViewPager viewPager) {
	t.setText("Lista de tarefas");
	viewPager.removeAllViews();
	ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
	adapter.addFrag(TaskFragment.newInstance(), "Lista de tarefas");
	viewPager.setAdapter(adapter);
	tabLayout.setupWithViewPager(viewPager);
	tabLayout.setVisibility(View.GONE);
}

private void setupLoginHeader(Bundle savedInstanceState) {

//	Marcos acrescentou para o header -------------------------------
		appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		try {
			LoginActivity.email = appPreferences.getString("user", "");
			LoginActivity.sessid = appPreferences.getString("sessid", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header);
		tvlabel = (TextView) headerLayout.findViewById(R.id.nav_header_label_login);
		loginTxt = (TextView) headerLayout.findViewById(R.id.email);
	headerLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(GraficoListActivity.this, LoginActivity.class), 666);
				overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
			}
		});
		if (!LoginActivity.email.equals("")) {
			tvlabel.setText(getString(R.string.navigation_drawer_label_username));
			loginTxt.setText(LoginActivity.email);
		}
//	-------------------------------


}

@Override
public void onBackPressed() {
	if (back_pressed + 2000 > System.currentTimeMillis()) super.onBackPressed();
	else
		Toast.makeText(getBaseContext(), "Carregue mais uma vez para sair!", Toast.LENGTH_SHORT).show();

}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
	getMenuInflater().inflate(R.menu.menu_main, menu);
//	ActionItemBadge.update(this, menu.findItem(R.id.item_samplebadge), GoogleMaterial.Icon.gmd_account_box, ActionItemBadge.BadgeStyles.RED, 0);
	menu.findItem(R.id.item_samplebadge).setIcon(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_account_box).color(Color.WHITE).actionBar());
	return super.onCreateOptionsMenu(menu);
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
		case android.R.id.home:
			if (!mTwoPane) mDrawerLayout.openDrawer(GravityCompat.START);
			return true;
		case R.id.item_samplebadge:
			callListadeTarefas(viewPager);
			break;
		case R.id.action_update: {
			Toast.makeText(getApplicationContext(), "Abrindo para Download....", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("https://sites.google.com/site/marcosmbbrito/POC-debug.apk?attredirects=0&d=1"));
			if (intent.resolveActivity(getPackageManager()) != null) {
				startActivity(intent);
			}
			break;
		}
		case R.id.action_about:
			try {
				String mens = "Versão da aplicação: " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName + "\n";
				Toast.makeText(this, mens, Toast.LENGTH_LONG).show();
			} catch (PackageManager.NameNotFoundException e) {
				e.printStackTrace();
			}
			break;

	}
	return super.onOptionsItemSelected(item);
}

private void setupViewPager(ViewPager viewPager, int opcion) {
	tabLayout.setVisibility(View.VISIBLE);
//        fragment = GrafListFragment.newInstance("16");
	switch (opcion) {
		case 0:
		case 1:
//                mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
//                setSupportActionBar(mActionBarToolbar);
//                getSupportActionBar().setTitle("Toolbar Title");
			t.setText("Registro Civil");
			viewPager.removeAllViews();
			ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
			adapter.addFrag(GrafListFragment.newInstance("404"), "NACIMIENTOS");
			adapter.addFrag(GrafListFragment.newInstance("412"), "MATRIMONIOS");
			adapter.addFrag(GrafListFragment.newInstance("408"), "DEFUNCIONES");
			viewPager.setAdapter(adapter);
			tabLayout.setupWithViewPager(viewPager);
			if (!mTwoPane) mDrawerLayout.closeDrawers();
			break;
		case 2:
			t.setText("Registros Mercantil");
			viewPager.removeAllViews();
			ViewPagerAdapter adapter2 = new ViewPagerAdapter(getSupportFragmentManager());
			adapter2.addFrag(GrafListFragment.newInstance("598"), "");
			viewPager.setAdapter(adapter2);
			tabLayout.setupWithViewPager(viewPager);
			tabLayout.setVisibility(View.GONE);
			if (!mTwoPane) mDrawerLayout.closeDrawers();
			break;
		case 3:
			t.setText("Registro Propiedad");
			break;
		case 4:
			t.setText("Recursos Humanos");
			viewPager.removeAllViews();
			ViewPagerAdapter adapter4 = new ViewPagerAdapter(getSupportFragmentManager());
			adapter4.addFrag(GrafListFragment.newInstance("560"), "");
			viewPager.setAdapter(adapter4);
			tabLayout.setupWithViewPager(viewPager);
			tabLayout.setVisibility(View.GONE);
			if (!mTwoPane) mDrawerLayout.closeDrawers();
			break;
		case 5:
			t.setText("Gestión de Protocolo");
			viewPager.removeAllViews();
			ViewPagerAdapter adapter5 = new ViewPagerAdapter(getSupportFragmentManager());
			adapter5.addFrag(GrafListFragment.newInstance("551"), "Audiencias");
			adapter5.addFrag(GrafListFragment.newInstance("553"), "Eventos");
			viewPager.setAdapter(adapter5);
			tabLayout.setupWithViewPager(viewPager);
			if (!mTwoPane) mDrawerLayout.closeDrawers();
			break;
		case 6:
			t.setText("Comunicaciones Oficiales");
			viewPager.removeAllViews();
			ViewPagerAdapter adapter6 = new ViewPagerAdapter(getSupportFragmentManager());
			adapter6.addFrag(DashboardFragment.newInstance("498"), "Dashboard");
			adapter6.addFrag(GrafListFragment.newInstance("545"), "Subscrición");
			adapter6.addFrag(GrafListFragment.newInstance("539"), "Comunicaciones Oficiales");
			viewPager.setAdapter(adapter6);
			tabLayout.setupWithViewPager(viewPager);
			if (!mTwoPane) mDrawerLayout.closeDrawers();
			break;
		case 7:
			t.setText("Gestión de NIF");
		case 8:
			t.setText("Acompañamiento de Processos");

	}

}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	super.onActivityResult(requestCode, resultCode, data);
	if (666 == requestCode) {
		if (resultCode == RESULT_OK) {
			editor = appPreferences.edit();
			editor.putString("user", LoginActivity.email);
			editor.putString("sessid", LoginActivity.sessid);
			editor.apply();
			tvlabel.setText(getString(R.string.navigation_drawer_label_username));
			loginTxt.setText(LoginActivity.email);
			logoutMenuItem.setVisible(true);
//			CallNetwebGetArea();
		}
//			else{
//				Toast.makeText(this, "Result not OK", Toast.LENGTH_LONG).show();
//			}
	}
	Log.e("GraficoListActivity", "onActivityResult (line 377):int requestCode, int resultCode " + requestCode + " fdsfa " + resultCode);

}

private void logoutSnnipet() {
	LoginActivity.email = "";
	LoginActivity.sessid = "";
	editor = appPreferences.edit();
	editor.remove("user");
	editor.remove("sessid");
	editor.apply();
	tvlabel.setText(getString(R.string.navigation_drawer_log_in_please));
	loginTxt.setText(LoginActivity.email);
	logoutMenuItem.setVisible(false);
	Snackbar.make(navigationView, "Terminou a sessão", Snackbar.LENGTH_SHORT).show();
//	CallNetwebGetArea();
}

class ViewPagerAdapter extends FragmentStatePagerAdapter {
	private List<Fragment> mFragmentList = new ArrayList<>();
	private List<String> mFragmentTitleList = new ArrayList<>();

	public ViewPagerAdapter(FragmentManager manager) {
		super(manager);
	}

	@Override
	public Fragment getItem(int position) {
		return mFragmentList.get(position);
	}

	@Override
	public int getCount() {
		return mFragmentList.size();
	}

	public void addFrag(Fragment fragment, String title) {
		if (title.equals("Dashboard")) {
			mFragmentList.add(0, fragment);
			mFragmentTitleList.add(0, title);
		} else {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

	}

	@Override
	public CharSequence getPageTitle(int position) {
		return mFragmentTitleList.get(position);
	}
}

}
