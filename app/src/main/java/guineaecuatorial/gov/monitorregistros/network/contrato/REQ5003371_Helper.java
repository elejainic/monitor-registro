package guineaecuatorial.gov.monitorregistros.network.contrato;


import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;


public   class   REQ5003371_Helper   extends GenericModelHelper {

//private      static   String   CONTRATO=GraficoListActivity.ambientAtual==GraficoListActivity.PROD?"":"MOVEL19841";
private      static   String   CONTRATO="";

private   final   static   String   SERVICE= "5003371";

public   final   static   String   METHODCALLBACK="REQ5003371_CallBack";

public   REQ5003371_Helper   (){}

public   static CallWebServiceModel prepareModel   (){return prepareModel(CONTRATO,SERVICE,new   REQ5003371_Model());}

public   static CallWebServiceModel prepareModel   (String   code_cliente,
                                                        String   code_servico
                                                        ){
    switch (GraficoListActivity.ambientAtual) {
        case GraficoListActivity.PROD:
            CONTRATO="MOVEL2824";
            break;
        case GraficoListActivity.STAGE:
            CONTRATO="MOVEL7481";
            break;
        case GraficoListActivity.DEP:
//            CONTRATO="MOVEL21141";
            code_cliente="MOVELDEP";
//            CONTRATO="MOVEL9721";
//            break;
        case GraficoListActivity.DEV:
            CONTRATO="MOVEL19841";
            break;

    }
	return   prepareModel(CONTRATO,SERVICE,new   REQ5003371_Model(code_cliente, code_servico));
}
}
