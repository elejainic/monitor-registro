package guineaecuatorial.gov.monitorregistros.network.contrato;


import guineaecuatorial.gov.monitorregistros.generics.GenericArgs;

public  class  RESP5003371_Model  extends  GenericArgs.Rows.Row{
private  String contrato;
private  String status;
private  String status_text;

public  RESP5003371_Model(){super();}
public  RESP5003371_Model(String  contrato, String  status, String  status_text){
	super();
	this.contrato=contrato;
	this.status=status;
	this.status_text=status_text;
}

public  void  setContrato(String  contrato){  this.contrato=contrato;  }
public  void  setStatus(String  status){  this.status=status;  }
public  void  setStatus_text(String  status_text){  this.status_text=status_text;  }


public  String  getContrato(){  return  this.contrato; }
public  String  getStatus(){  return  this.status; }
public  String  getStatus_text(){  return  this.status_text; }

}