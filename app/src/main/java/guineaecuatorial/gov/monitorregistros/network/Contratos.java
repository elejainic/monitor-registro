package guineaecuatorial.gov.monitorregistros.network;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import guineaecuatorial.gov.monitorregistros.BuildConfig;
import guineaecuatorial.gov.monitorregistros.network.contrato.REQ5003371_Helper;
import guineaecuatorial.gov.monitorregistros.network.contrato.RESP5003371_Helper;


/**
 * Created by MarcosM on 11/09/2014.
 */
public class Contratos {


public static String getContrato( String n_Servico) {
	String resp = null;
	try {
		Log.e("Contratos", "getContrato (line 21): ");
		resp = RESP5003371_Helper.parseModel(new JSONObject(new Networkhelper().callWebservice(REQ5003371_Helper.prepareModel("MOVEL", n_Servico))));
		if(BuildConfig.DEBUG)
		Log.e("Contratos", "getContrato (line 23): dps Resp "+resp);

	} catch (JSONException e) {
		e.printStackTrace();
		Log.e("Contratos", "getContrato (line 25): erro");
	}

	if(null!=resp)
		return resp;
	else
		Log.e("Contratos", "getContrato (line 56): resp é null");
		return null;

//	new RetrieveFeedTask().execute(n_Servico);
//	new Networkhelper().callWebservice(REQ5003371_Helper.prepareModel("MOVEL", n_Servico),REQ5003371_CallBack);


//	String strNumContrato = null;
//	switch (n_Servico) {
//		case "5003530":
//			strNumContrato = "MOVEL20581";
//			break;
//		case "5001877":
//			strNumContrato = "MOVEL10765";
//			break;
//		default:
//			Log.e("Contratos", "getContrato (line 14): tá mali nao encotrna nada");
//			break;
//	}
//	return strNumContrato;
}

}
