package guineaecuatorial.gov.monitorregistros.network;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encrypter {
	
	
	
	
	

	final protected static char[] hexArray = { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

//	public String decrypt(String cipherText, byte[] key,
//			byte[] initializationVector) throws Exception {
//		byte[] encData = Base64.decode(cipherText, Base64.DEFAULT);
//
//		Cipher c3des = Cipher.getInstance("DESede/CBC/PKCS5Padding");
//		SecretKeySpec myKey = new SecretKeySpec(key, "DESede");
//		IvParameterSpec ivspec = new IvParameterSpec(initializationVector);
//
//		c3des.init(Cipher.DECRYPT_MODE, myKey, ivspec);
//		byte[] plainText = c3des.doFinal(encData);
//
//		return new String(Base64.encode(plainText,  Base64.DEFAULT),);
//	}
//	
	
	public static String byteArrayToHexString(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		int v;

		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}

		return new String(hexChars);
	}

	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];

		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}

		return data;
	}

	public String encrypt(String plainText, byte[] key,
			byte[] initializationVector) throws Exception {


		Cipher c3des = Cipher.getInstance("AES/CBC/PKCS5Padding"); // PKCS5Padding
																	// NoPadding
		SecretKeySpec myKey = new SecretKeySpec(key, "AES");

		IvParameterSpec ivspec = new IvParameterSpec(initializationVector);

		c3des.init(Cipher.ENCRYPT_MODE, myKey, ivspec);
		byte[] cipherText = c3des.doFinal(plainText.getBytes("UTF-8"));

		return new String(Base64.encode(cipherText,  Base64.DEFAULT),"UTF-8");
	}

}
