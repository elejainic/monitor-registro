package guineaecuatorial.gov.monitorregistros.network.contrato;

import com.google.gson.Gson;

import org.json.JSONObject;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;


public  class  RESP5003371_Helper  extends GenericModelHelper {

public  RESP5003371_Helper(){
}

public  static String jsonResultTransform(JSONObject row){

//	Log.e("RESP5003371_Helper", "jsonResultTransform (line 18): ROW "+row);
	RESP5003371_Model  r;

	if (row != null) {
		r =  new Gson().fromJson(row.toString(), RESP5003371_Model.class);
		return r.getContrato();
	}else
	return null;

}

public  static String parseModel(JSONObject result) {


	JSONObject  row  =  getSucessJsonObject(result);

	return  jsonResultTransform(row);
}



}