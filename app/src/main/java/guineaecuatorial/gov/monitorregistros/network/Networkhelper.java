package guineaecuatorial.gov.monitorregistros.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import guineaecuatorial.gov.monitorregistros.BuildConfig;
import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;


public class  Networkhelper {
	
	public static final MediaType JSON = MediaType
			.parse("application/json; charset=utf-8");
    private  static String url ;
	private static final String TAG = "Networkhelper";
	private static OkHttpClient client;
	private Context c;



	public Networkhelper(Context caller_activity) {
c=caller_activity;
	new Networkhelper();
}

	public Networkhelper() {

			switch (GraficoListActivity.ambientAtual) {
			case GraficoListActivity.PROD:
			case GraficoListActivity.STAGE:
				url ="https://biztalk.gov.cv/NOSIRestServices/GenericServiceRequest.svc/GenericRequest";
				break;
			case GraficoListActivity.DEP:
			case GraficoListActivity.DEV:
				url="http://rai-gv-tbz-01.gov.cv/NOSIRestServices/GenericServiceRequest.svc/GenericRequest";

				break;

		}
		client = new OkHttpClient();
	}


	public <K> String callWebservice( K model) {
		Gson gson = new Gson();
		String json = gson.toJson(model);

		try {
			return post(url, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}


    /**
	 * @param model O JSON
	 * @param interfaceCallback que tratara da resposta.
	 *  
	 */

	public <K> void callWebservice( K model, Callback interfaceCallback) {


			Gson gson = new Gson();
			String json = gson.toJson(model);
		if (BuildConfig.DEBUG)
			if(model instanceof CallWebServiceModel)
				Log.e(TAG, "MODEL"+((CallWebServiceModel) model).getService() +":" + json);

			try {

				post(url, json, interfaceCallback);
			} catch (IOException e) {
				e.printStackTrace();
			}

	}
	

	
	private void post(String url, String json, Callback callback) throws IOException {
//		Log.e("Networkhelper", "post (line 58): json "+json+"URL "+url);
		certificate();
		RequestBody body = RequestBody.create(JSON, json);
		Request request = new Request.Builder().url(url)
				.post(body)
				.build();
		client.newCall(request).enqueue(callback);
	}

	public void postSoap(String url, String xml, Callback callback) throws IOException {
		RequestBody body = RequestBody.create( MediaType.parse("application/soap+xml"), xml);
		Request request = new Request.Builder()
				.url(url)
				.post(body)
				.build();
		client.newCall(request).enqueue(callback);
	}


	public void post(String xml, Callback goCallback) throws IOException  {


		String urlSoap = null;
//         String url= "http://biztalkqual.gov.cv/redws/nosiws.asmx";
//         String url= "http://govapps.gov.cv/bizws/NosiWSPort";
//        TODO:Mozamique



		if(BuildConfig.FLAVOR.equals("eBau"))
		 urlSoap="http://196.43.234.31:7001/redws/NosiWSPort";

		postSoap(urlSoap, xml, goCallback);
	}
	private String post(String url, String json) throws IOException {
		Log.e("Networkhelper", "post (line 130): json"+json);

		certificate();
		RequestBody body = RequestBody.create(JSON, json);
		Request request = new Request.Builder()
				.url(url)
				.post(body)
				.build();
		Response response = client.newCall(request).execute();
		String resp;
		resp = response.body().string();

		if(BuildConfig.DEBUG) {
			Log.e("Networkhelper", "post >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><(line 120): resp"+resp);
		}
		return resp ;
	}
	private void certificate() throws IOException {
		InputStream is;
		is =GraficoListActivity.c.getAssets().open("load_der.cer");

		InputStream caInput;  //new FileInputStream("load-der.cer")

			caInput = new BufferedInputStream(is);

//            caInput = new BufferedInputStream(new FileInputStream("load-der.crt"));

		Certificate ca = null;
		try {
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
// From https://www.washington.edu/itconnect/security/ca/load-der.crt
			ca = cf.generateCertificate(caInput);
			System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
		} catch (CertificateException e) {
			e.printStackTrace();
		} finally {
			caInput.close();
		}
// Create a KeyStore containing our trusted CAs
		SSLContext sslContext;
		String keyStoreType = KeyStore.getDefaultType();
		KeyStore keyStore;
		String tmfAlgorithm;
		TrustManagerFactory tmf;
		try {
			keyStore = KeyStore.getInstance(keyStoreType);
			keyStore.load(null, null);
			keyStore.setCertificateEntry("ca", ca);
			tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
			tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
			tmf.init(keyStore);
			sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, tmf.getTrustManagers(), null);
			client.setSslSocketFactory(sslContext.getSocketFactory());
		} catch (GeneralSecurityException e) {
			throw new AssertionError(); // The system has no TLS. Just give up.
		}

	}
}
