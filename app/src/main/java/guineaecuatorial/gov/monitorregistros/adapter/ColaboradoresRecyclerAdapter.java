package guineaecuatorial.gov.monitorregistros.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.model.taskByColaboradores.RESP5003950_Model;

/**
 * Created by MarcosM on 04/11/2015.
 */
public class ColaboradoresRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
private final LayoutInflater mLayoutInflater;

private List<RESP5003950_Model.colaboradoresModel> taskByColaborValues = new ArrayList<>();

public ColaboradoresRecyclerAdapter(Context context, List<RESP5003950_Model.colaboradoresModel> colaboradoresModel) {
	mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	if (!colaboradoresModel.isEmpty()) {
		taskByColaborValues.addAll(colaboradoresModel);
	}

}

@Override
public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
	final View view = mLayoutInflater.inflate(R.layout.list_task_by_colaboradores_item, parent, false);
	return new ColaboradoresVH(view);
}

@Override
public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
	ColaboradoresVH viewHolder = (ColaboradoresVH) holder;
	RESP5003950_Model.colaboradoresModel colaboradoresModel = taskByColaborValues.get(position);
	viewHolder.nome.setText(colaboradoresModel.getNome());
	viewHolder.contacto.setText(colaboradoresModel.getContacto());
	viewHolder.nTarefas.setText(String.format("N. Tarefas: %d", colaboradoresModel.getNtarefas()));
	viewHolder.nAtendimento.setText(String.format("N. Atendimento: %d", colaboradoresModel.getNatendimento()));
	viewHolder.mediatempo.setText(String.format("Média Tempo: %d", colaboradoresModel.getMediatempo()));
	viewHolder.ranking.setText(String.format("Ranking: %d", colaboradoresModel.getRanking()));
	viewHolder.percent.setText(String.format("Percentagem: %d", colaboradoresModel.getPercent()));

}

@Override
public int getItemCount() {
	return taskByColaborValues.size();
}

public void setColaboradores(List<RESP5003950_Model.colaboradoresModel> taskByColaborArrayList) {
	taskByColaborValues.clear();
	taskByColaborValues.addAll(taskByColaborArrayList);
	notifyDataSetChanged();
}

public static class ColaboradoresVH extends RecyclerView.ViewHolder {
	public TextView nome;
	public TextView contacto;
	public TextView nTarefas;
	public TextView nAtendimento;
	public TextView mediatempo;
	public TextView ranking;
	public TextView percent;

	public ColaboradoresVH(View convertView) {
		super(convertView);
		nome = (TextView) convertView.findViewById(R.id.RESP5003950_nome);
		contacto = (TextView) convertView.findViewById(R.id.RESP5003950_contacto);
		nTarefas = (TextView) convertView.findViewById(R.id.RESP5003950_n_tarefas);
		nAtendimento = (TextView) convertView.findViewById(R.id.RESP5003950_n_atendimento);
		mediatempo = (TextView) convertView.findViewById(R.id.RESP5003950_media_tempo);
		ranking = (TextView) convertView.findViewById(R.id.RESP5003950_ranking);
		percent = (TextView) convertView.findViewById(R.id.RESP5003950_percent);
//
	}
}
}
