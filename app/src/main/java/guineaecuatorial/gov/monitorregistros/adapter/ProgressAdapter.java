package guineaecuatorial.gov.monitorregistros.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;

import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.R;

/**
 * Created by JAIME on 01/10/2015.
 */
public class ProgressAdapter  extends BaseAdapter {

    private NumberProgressBar bnp;
    private static LayoutInflater mInflater;
    private ArrayList<String> mData = new ArrayList<>();
    private ArrayList<Integer> mTotal = new ArrayList<>();
    private ArrayList<Integer> mProgresso = new ArrayList<>();

    public ProgressAdapter(Context context  ) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final String item,final Integer progress,final Integer total) {
        mData.add(item);
        mProgresso.add(progress);
        mTotal.add(total);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder = null;

        int type = getItemViewType(position);

            if (type == 0) {
            // Inflate the layout with image
                int num = mProgresso.get(position);
                int denom =  mTotal.get(position);
                String strI2 = String.valueOf(num);

            convertView = mInflater.inflate(R.layout.progress_bar, parent,false);
                TextView textView = (TextView) convertView.findViewById(R.id.list_departamentos_textview);
                textView.setText(strI2);
                bnp = (NumberProgressBar)convertView.findViewById(R.id.progressBar1);
//                int num = mProgresso.get(position);
//                int denom =  mTotal.get(position);

                double porcentaje = ((double)num / denom) * 100;
                int  progress = (int)porcentaje;
                bnp.setProgress(progress);


            }
            else { convertView = mInflater.inflate(R.layout.no_bar, parent,false);
                TextView textView1 = (TextView) convertView.findViewById(R.id.departamentos_textview);
                textView1.setText("Aqui no hay");

            }
         //




////        Contact c = contactList.get(position);
//        TextView surname = (TextView) v.findViewById(R.id.surname);
//        TextView name = (TextView) v.findViewById(R.id.name);
//        TextView email = (TextView) v.findViewById(R.id.email);
////        if (type == 0) {
////            ImageView img = (ImageView) v.findViewById(R.id.img);
////            img.setImageResource(c.imageId);
////        }
////        surname.setText(c.surname);
////        name.setText(c.name);
////        email.setText(c.email);
        return convertView; }

//    public static class ViewHolder {
//        public TextView textView;
//    }

}
