package guineaecuatorial.gov.monitorregistros.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.util.ColorTemplate;


/**
 * Created by MarcosM on 30/08/2014.
 */


public class AdapterPieBar extends BaseAdapter {

    private Context context;
private ArrayList<String> mXvals;
private ArrayList<String> mEntries;


public AdapterPieBar(Context context, ArrayList<String> mXvals, ArrayList<String> mEntries) {
    this.context = context;
    this.mXvals = mXvals;
    this.mEntries = mEntries;
}

@Override
public int getCount() {
    return mXvals.size();
}

@Override
public String getItem(int position) {
    return mXvals.get(position);
}


@Override
public long getItemId(int position) {
    return 0;
}

@Override
    public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder holder;
        int color;
//
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_tabela_pie, parent, false);
            holder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            holder.tvTotal = (TextView) convertView.findViewById(R.id.tx_total_list_tabela);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
    color = ContextCompat.getColor(context, ColorTemplate.PASTEL_COLORS[position]);
    holder.tvName.setText(getItem(position));
        holder.tvName.setTextColor(color);
//        float num1 = Float.parseFloat(c.tota);


        NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###.###");
    holder.tvTotal.setText(nf.format(new BigDecimal(mEntries.get(position))));
//        holder.tvTotal.setText(String.format("%.2f", Float.parseFloat(c.tota)));


        holder.tvTotal.setBackgroundColor(color);

        return convertView;
    }

    private class ViewHolder {

        TextView tvName, tvTotal;
    }
}

