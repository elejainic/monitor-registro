package guineaecuatorial.gov.monitorregistros;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anupcowkur.reservoir.Reservoir;
import com.anupcowkur.reservoir.ReservoirGetCallback;
import com.anupcowkur.reservoir.ReservoirPutCallback;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import guineaecuatorial.gov.monitorregistros.fragments.BarChartFrag;
import guineaecuatorial.gov.monitorregistros.fragments.LineFragment;
import guineaecuatorial.gov.monitorregistros.fragments.ListagemFrag;
import guineaecuatorial.gov.monitorregistros.fragments.PieChartFrag;
import guineaecuatorial.gov.monitorregistros.helper.thegraf.REQ5003531_Helper;
import guineaecuatorial.gov.monitorregistros.helper.thegraf.RESP5003531_Helper;
import guineaecuatorial.gov.monitorregistros.model.ContentItem4;
import guineaecuatorial.gov.monitorregistros.model.GrafItemContent;
import guineaecuatorial.gov.monitorregistros.model.thegraf.RESP5003531_Model;
import guineaecuatorial.gov.monitorregistros.network.Connectivity;
import guineaecuatorial.gov.monitorregistros.network.Networkhelper;
import me.imid.swipebacklayout.lib.SwipeBackLayout;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;

/**
 * An activity representing a single Grafico detail screen. This
 * activity is only used on handset devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link }.
 * <p/>
 * This activity is mostly just a 'shell' activity containing nothing
 * more than a {@link }.
 */
public class GraficoDetailActivity extends SwipeBackActivity implements PieChartFrag.OnFragmentInteractionListener, BarChartFrag.OnBarFragmentInteractionListener {

public static final String ARG_POSITION = "position";
private static final String TAG = "GraficoDetailActivity";

public static List<ContentItem4> listagem;
private static Integer idGr = 16;
//Para data
private static String firstData;
private static String lastDate;
private static GrafItemContent.GrafItem grafItemContent;
private static String dataUltima = "";
private static Callback goCallback;
private static ProgressBar progressBar;

private static String iconf;
ActionBar ab = null;
JSONObject json;
TextView peridoTxt;
private Networkhelper net;
private Spinner spinner;
private int currentYear;
private int currentMonth;
private int currentDayOfMonth;
private int DeYear;
private int DeMonth;
private int DeDayOfMonth;
private SimpleDateFormat df;
private Snackbar snackBar;
Callback REQ5003531_CallBack = new Callback() {


	@Override
	public void onFailure(Request request, IOException e) {
		GraficoDetailActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(GraficoDetailActivity.this, "Modo offline!", Toast.LENGTH_LONG).show();
				progressBar.setVisibility(View.GONE);
			}
		});
	}

	@Override
	public void onResponse(Response response) throws IOException {
		ArrayList<String> leg = null;
		ArrayList<String> xVals = new ArrayList<>();
		ArrayList<String> entr = new ArrayList<>();
		switch (grafItemContent.getTipo()) {
			case "STACKBAR":
			case "LINHA":
				leg = new ArrayList<>();
				break;
			case "LISTA":
				listagem = new ArrayList<>();
				break;
		}
		try {
			GraficoDetailActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					progressBar.setVisibility(View.GONE);
				}
			});
			json = new JSONObject(response.body().string());
			if (BuildConfig.DEBUG)
				Log.e("GraficoDetailActivity", "onResponse (line 1120): json" + json);
//                json = new JSONObject("{ 'rows': [ { 'tipo_licenca': 'Licença Simplificado', 'total': 24 }, { 'tipo_licenca': 'Registo de Exportador', 'total': 1 }, { 'tipo_licenca': 'Licença de Instalação Comercial', 'total': 31 } ] }");
			ArrayList<RESP5003531_Model.dadosModel> dadosModelArrayList = null;
			boolean updateChart = false;
			try {
				RESP5003531_Model result = RESP5003531_Helper.parseModel(json);
				updateChart = !dataUltima.equals(result.getDt_ultima_act());
				dataUltima = result.getDt_ultima_act();
				dadosModelArrayList = result.getDados();
				if (snackBar.isShownOrQueued()) snackBar.dismiss();
				if (!getPeriodo().equals("PERIODO") && dadosModelArrayList != null)
					Reservoir.putAsync("" + idGr + getPeriodo(), result, new ReservoirPutCallback() {
						@Override
						public void onSuccess() {
							//success
							Log.e(TAG, "Reservoir onSuccess  (line 138): ");
						}

						@Override
						public void onFailure(Exception e) {
							//error
							Log.e(TAG, "Reservoir onFailure (line 144): ");

						}
					});

			} catch (Exception e) {
				Log.e(TAG, "try catch onResponse (line 156): try catch");
				e.printStackTrace();
				if (!snackBar.isShownOrQueued()) txomaRespetivoGraf(xVals, entr, null);
			}
			if (updateChart) {
				if (dadosModelArrayList != null)
					trataJSONResp(leg, xVals, entr, dadosModelArrayList);

			}
			//proggre.setVisibility(View.INVISIBLE);
		} catch (JSONException e) {
			Log.e(TAG, "try catch onResponse (line 168): try catch");
//				GraficoDetailActivity.this.runOnUiThread(new Runnable() {
//					@Override
//					public void run() {
////						Toast.makeText(GraficoDetailActivity.this, "Erro JSONException: " + json, Toast.LENGTH_LONG).show();
//					}
//				});e.p
			e.printStackTrace();
			txomaRespetivoGraf(xVals, entr, null);
			Log.e(TAG, "dento erro si");
		} catch (Exception e) {
//
//				GraficoDetailActivity.this.runOnUiThread(new Runnable() {
//					@Override
//					public void run() {
//						Toast.makeText(GraficoDetailActivity.this, "Erro Exception: " + json, Toast.LENGTH_LONG).show();
//					}
//				});
			e.printStackTrace();
			Log.e(TAG, "try catch onResponse (line 187): try catch ");

		}
	}

};

private void trataJSONResp(ArrayList<String> leg, ArrayList<String> xVals, ArrayList<String> entr, ArrayList<RESP5003531_Model.dadosModel> dadosModelArrayList) {
	Log.e(TAG, "REQ5003531_CallBack _________________________________---");
	for (int i = 0; i < dadosModelArrayList.size(); i++) {
		//					String format = String.format("%.2f", 1.20093);
		//					String format = f.format(dadosModelArrayList.get(i).getTotal());
		String format = dadosModelArrayList.get(i).getTotal();
		//							!= null ? dadosModelArrayList.get(i).getTotal() : 0;
		entr.add("" + format);
		switch (grafItemContent.getTipo()) {
			case "STACKBAR":
				if (leg != null) {
					leg.add(null != dadosModelArrayList.get(i).getNomelinha() ? dadosModelArrayList.get(i).getNomelinha() : "");
				}
			case "BAR":
			case "PIE":
				xVals.add(null != dadosModelArrayList.get(i).getXvals() ? dadosModelArrayList.get(i).getXvals() : "-");
				break;
			case "LINHA":
				xVals.add(null != dadosModelArrayList.get(i).getData_evol() ? dadosModelArrayList.get(i).getData_evol() : "");
				if (leg != null) {
					leg.add(null != dadosModelArrayList.get(i).getNomelinha() ? dadosModelArrayList.get(i).getNomelinha() : "");
				}
				break;
			case "LISTA":
				listagem.add(new ContentItem4(dadosModelArrayList.get(i).getTitulo(), dadosModelArrayList.get(i).getDatatext(), dadosModelArrayList.get(i).getSubtit(), "", "", ""));
				break;

		}
	}
//				Log.e("GraficoDetailActivity", "onResponse (line 172): xVals.size()" + xVals.size());
	if (xVals.size() == entr.size() && null != xVals.get(0)) txomaRespetivoGraf(xVals, entr, leg);

}

private void txomaRespetivoGraf(ArrayList<String> xVals, ArrayList<String> entr, ArrayList<String> legs) {
	Fragment fragment = null;
	Log.e("GraficoDetailActivity", "txomaRespetivoGraf (line 220): ");
	String iconf1;
	iconf1 = "{faw-area-chart}";
	Log.e("GraficoDetailActivity", "txomaRespetivoGraf (line 216): null != legs");
	switch (grafItemContent.getTipo()) {
		case "LINHA":
			fragment = LineFragment.newInstance(xVals, entr, legs, R.id.linechart_listview);
			iconf1 = "{faw-line-chart}";
			break;
		case "STACKBAR":
			fragment = BarChartFrag.newInstance(xVals, entr, R.id.bar_listviw, legs, "STACKBAR");
			iconf1 = "{faw-bar-chart}";
			break;
		case "BAR":
			fragment = BarChartFrag.newInstance(xVals, entr, R.id.bar_listviw, null, "BAR");
			iconf1 = "{faw-bar-chart}";
			break;
		case "PIE":
			fragment = PieChartFrag.newInstance(xVals, entr, R.id.pie_listviw);
			iconf1 = "{faw-pie-chart}";
			break;
		case "LISTA":
			fragment = ListagemFrag.newInstance();
			iconf1 = "{faw-list-alt}";
			break;
	}
	iconf = iconf1;
	runOnUiThread(new Runnable() {
		@Override
		public void run() {
			TextView txt = (TextView) findViewById(R.id.legenda_dt_ultim_actual);
			txt.setVisibility(View.VISIBLE);
			txt.setText(String.format("%s %s   {faw-clock-o} %s", iconf, grafItemContent.getNome(), dataUltima));
		}
	});
	getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_enter, R.anim.slide_exit, R.anim.move_left_in_activity, R.anim.move_left_out_activity).replace(R.id.grafico_detail_container, fragment).commit();

}

@Override
public void onBackPressed() {
	super.onBackPressed();
	overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
}

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_grafico_detail);
	SwipeBackLayout mSwipeBackLayout = getSwipeBackLayout();
	mSwipeBackLayout.setEdgeTrackingEnabled(SwipeBackLayout.EDGE_LEFT);
	net = new Networkhelper(this);
	Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
	setSupportActionBar(toolbar);                   // Setting toolbar as the ActionBar with setSupportActionBar() call
	((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("");
	initActionBar();
	progressBar = (ProgressBar) findViewById(R.id.progress_spinner);
	snackBar = Snackbar.make(progressBar, getString(R.string.last_update_in) + dataUltima, Snackbar.LENGTH_INDEFINITE);
	if (savedInstanceState == null) {
		// Create the detail fragment and add it to the activity
		// using a fragment transaction.
//			Bundle arguments = new Bundle();
//			arguments.putString(GraficoDetailFragment.ARG_ITEM_ID, getIntent().getStringExtra(GraficoDetailFragment.ARG_ITEM_ID));
//			Log.e("GraficoDetailActivity", "onCreate (line 70): getIntent().getStringExtra(GraficoDetailFragment.ARG_ITEM_ID)" + getIntent().getStringExtra(GraficoDetailFragment.ARG_ITEM_ID));
//			//proggre = (ProgressBar) findViewById(R.id.progressBar4);
		dataUltima = "";
		grafItemContent = getIntent().getParcelableExtra(GraficoDetailActivity.ARG_POSITION);
//			idServ = grafItemContent.getServ();
		if (grafItemContent != null) idGr = grafItemContent.getId();
//					GraficoDetailFragment fragment = new GraficoDetailFragment();
//			fragment.setArguments(arguments);
//			getSupportFragmentManager().beginTransaction().add(R.id.grafico_detail_container, fragment).commit();
	} else {
		TextView txt = (TextView) findViewById(R.id.legenda_dt_ultim_actual);
		txt.setVisibility(View.VISIBLE);
		txt.setText(String.format("%s %s   {faw-clock-o} %s", iconf, grafItemContent.getNome(), dataUltima));
	}
//		else {
	// do this for each of your text views
//			spinner.setSelection(savedInstanceState.getInt("yourSpinner", 1));
//		}
	initSpinner(savedInstanceState);

}

private void initActionBar() {
	ab = getSupportActionBar();
	if (ab != null) {
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowTitleEnabled(false);

	}
}

@Override
public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
	return super.onCreateView(parent, name, context, attrs);
}

private void switchooseGraf() {
	Log.e("GraficoDetailActivity", "switchooseGraf (line 1082): ");
	switch (grafItemContent.getTipo()) {
		case "LINHA":
		case "LISTA":
		case "PIE":
		case "BAR":
		case "STACKBAR":
			goCallback = REQ5003531_CallBack;
			getBizResp(idGr, goCallback);
			break;
	}

}


private void initSpinner(final Bundle savedInstanceState) {
	spinner = (Spinner) findViewById(R.id.spinner_data);
	spinner.setVisibility(View.VISIBLE);
	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.date_range, android.R.layout.simple_spinner_item);
	adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
//		 Create an ArrayAdapter using the string array and a default spinner
//		 layout
//		 Specify the layout to use when the list of choices appears
//		 Apply the adapter to the spinner
	spinner.setAdapter(adapter);
	if (null == savedInstanceState) {
		Log.e("GraficoDetailActivity", "initSpinner null->savedInstanceState(line 1184):NUUUUUUUUUULLLLLLLLLLLLL ");
		df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
		Calendar hj = Calendar.getInstance();
		int pos = 0;
		switch (grafItemContent.getPeriod()) {
			case "DIA":
				pos = 0;
				firstData = df.format(hj.getTime());
				lastDate = df.format(new GregorianCalendar().getTime());
				break;
			case "SEMANA":
				pos = 1;
				hj.add(Calendar.DAY_OF_MONTH, -6);
				firstData = df.format(hj.getTime());
				lastDate = df.format(new GregorianCalendar().getTime());
				break;
			case "MES":
				pos = 2;
				hj.add(Calendar.DAY_OF_MONTH, -28);
				firstData = df.format(hj.getTime());
				lastDate = df.format(new GregorianCalendar().getTime());
				break;
			case "ANO":
				pos = 3;
				hj.add(Calendar.DAY_OF_MONTH, -364);
				firstData = df.format(hj.getTime());
				lastDate = df.format(new GregorianCalendar().getTime());
				break;
			case "TODOS":
				pos = 4;
				hj.add(Calendar.YEAR, -10);
				firstData = df.format(hj.getTime());
				lastDate = df.format(new GregorianCalendar().getTime());
		}
		spinner.setSelection(pos, true);
		switchooseGraf();

	}
	spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			Log.e("GraficoDetailActivity", "onItemSelected (line 1207): ");
			if (position == 5) DatePickerFragment();
			else getBizResp(idGr, goCallback);

		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}

	});

}


@Override
public boolean onCreateOptionsMenu(Menu menu) {
	return super.onCreateOptionsMenu(menu);

}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
	int id = item.getItemId();
	if (id == android.R.id.home) {
		// This ID represents the Home or Up button. In the case of this
		// activity, the Up button is shown. Use NavUtils to allow users
		// to navigate up one level in the application structure. For
		// more details, see the Navigation pattern on Android Design:
		//
		// http://developer.android.com/design/patterns/navigation.html#up-vs-back
		//
//			ou getActivity().onBackPressed();
		finish();
		overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
//			NavUtils.navigateUpTo(this, new Intent(this, GraficoListActivity.class));
		return true;

	}
	return super.onOptionsItemSelected(item);
}

private void getBizResp(final int id, final Callback goCallback) {
	Log.e("GraficoDetailActivity", "getBizResp (line 1196): ");
	if (progressBar.getVisibility() != View.VISIBLE) {
		dataUltima = "";
		if (!getPeriodo().equals("PERIODO")) {
			Reservoir.getAsync("" + id + getPeriodo(), RESP5003531_Model.class, new ReservoirGetCallback<RESP5003531_Model>() {

				@Override
				public void onSuccess(RESP5003531_Model result) {
					Log.e(TAG, "_____ Reservoir ___ onSuccess (line 452): result= " + result.getDt_ultima_act());
					ArrayList<String> leg = null;
					ArrayList<String> xVals = new ArrayList<>();
					ArrayList<String> entr = new ArrayList<>();
					switch (grafItemContent.getTipo()) {
						case "STACKBAR":
						case "LINHA":
							leg = new ArrayList<>();
							break;
						case "LISTA":
							listagem = new ArrayList<>();
							break;
					}
					ArrayList<RESP5003531_Model.dadosModel> dadosModelArrayList;
					dataUltima = result.getDt_ultima_act();
					dadosModelArrayList = result.getDados();
					trataJSONResp(leg, xVals, entr, dadosModelArrayList);
					snackBar = Snackbar.make(progressBar, getString(R.string.last_update_in) + dataUltima, Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							snackBar.dismiss();
						}
					});
					snackBar.show();
				}

				@Override
				public void onFailure(Exception e) {
					//error
					Log.e(TAG, "Reservoir onFailure (line 488): dataulitma tem que ser nada para garantir nao carregar de novo");

				}
			});
		}
		if (Connectivity.isConnected(this)) {
			progressBar.setVisibility(View.VISIBLE);
			new Thread(new Runnable() {
				public void run() {
					net.callWebservice(REQ5003531_Helper.prepareModel("", String.valueOf(id), getPeriodo(), GraficoDetailActivity.firstData, GraficoDetailActivity.lastDate, GrafListFragment.idSection), goCallback);

				}
			}).start();
		} else Snackbar.make(progressBar, R.string.no_internet, Snackbar.LENGTH_LONG).show();

	} else progressBar.setVisibility(View.GONE);
}


private String getPeriodo() {
	String dia;
	switch (spinner.getSelectedItemPosition()) {
		case 0:
			dia = "DIA";
			break;
		case 1:
			dia = "SEMANA";
			break;
		case 2:
			dia = "MES";
			break;
		case 3:
			dia = "ANO";
			break;
		case 4:
			dia = "TODOS";
			break;
		case 5:
			dia = "PERIODO";
			break;
		default:
			dia = "SEMANA";
	}
	return dia;
}
//	public static class DatePickerFragment extends DialogFragment
//			implements DatePickerDialog.OnDateSetListener {
//		public static String firstData;
//		public static String lastDate;
//		@NonNull
//		@Override
//		public Dialog onCreateDialog(Bundle savedInstanceState) {
//			// Use the current date as the default date in the picker
//			final Calendar c = Calendar.getInstance();
//			int year = c.get(Calendar.YEAR);
//			int month = c.get(Calendar.MONTH);
//			int day = c.get(Calendar.DAY_OF_MONTH);
//
//			// Create a new instance of DatePickerDialog and return it
//			return new DatePickerDialog(getActivity(), this, year, month, day);
//		}
//
//		public void onDateSet(DatePicker view, int year, int month, int day) {
//			// Do something with the date chosen by the user
//
//			Log.e("DatePickerFragment", "onDateSet (line 424): year mont day"+year+" "+month+" "+day);
//		}
//	}

public void DatePickerFragment() {
//		View multiPickerLayout = LayoutInflater.from(GraficoDetailActivity.this).inflate(R.layout.dialog_range, spinner);
//		final DatePicker multiPickerDateDe = (DatePicker) multiPickerLayout.findViewById(R.id.datePicker_de);
//		final DatePicker multiPickerDateAte = (DatePicker) multiPickerLayout.findViewById(R.id.datePicker_ate);
//
//		peridoTxt = (TextView) multiPickerLayout.findViewById(R.id.tv_data_periodo);
////					Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
//
//		Calendar cal = new GregorianCalendar();
//		df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
//		// Convert Date em Calendar
//		if (null == firstData) firstData = df.format(cal.getTime());
//		if (null == lastDate) lastDate = df.format(cal.getTime());
//
//
//		try {
//			cal.setTime(df.parse(firstData));
//			DeYear = cal.get(Calendar.YEAR);
//			DeMonth = cal.get(Calendar.MONTH);
//			DeDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
//			cal.clear();
//			cal.setTime(df.parse(lastDate));
//			currentYear = cal.get(Calendar.YEAR);
//			currentMonth = cal.get(Calendar.MONTH);
//			currentDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//
//
//		multiPickerDateDe.init(DeYear, DeMonth, DeDayOfMonth, new DatePicker.OnDateChangedListener() {
//			@Override
//			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//				DeYear = year;
//				DeMonth = monthOfYear;
//				DeDayOfMonth = dayOfMonth;
//
//				if (new GregorianCalendar(year, monthOfYear, dayOfMonth).getTimeInMillis() > new GregorianCalendar(currentYear, currentMonth, currentDayOfMonth).getTimeInMillis()) {
//					multiPickerDateAte.updateDate(year, monthOfYear, dayOfMonth);
//				}
//
//				firstData = df.format(new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime());
//				peridoTxt.setText("De: " + firstData + "      Até: " + lastDate);
//
//			}
//		});
//		multiPickerDateAte.init(currentYear, currentMonth, currentDayOfMonth, new DatePicker.OnDateChangedListener() {
//			@Override
//			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//				currentYear = year;
//				currentMonth = monthOfYear;
//				currentDayOfMonth = dayOfMonth;
//				if (new GregorianCalendar(DeYear, DeMonth, DeDayOfMonth).getTimeInMillis() > new GregorianCalendar(currentYear, currentMonth, currentDayOfMonth).getTimeInMillis()) {
//					multiPickerDateDe.updateDate(year, monthOfYear, dayOfMonth);
//				}
//
//				lastDate = df.format(new GregorianCalendar(currentYear, currentMonth, currentDayOfMonth).getTime());
//				peridoTxt.setText("De: " + firstData + " / Até: " + lastDate);
//			}
//
//		});
//
////					multiPickerDateAte.updateDate(currentYear, maxCal.get(Calendar.MONTH), maxCal.get(Calendar.DAY_OF_MONTH));
////					multiPickerDateAte.setMinDate(minCal.getTimeInMillis());
////					multiPickerDateAte.updateDate(minCal.get(Calendar.YEAR), minCal.get(Calendar.MONTH), minCal.get(Calendar.DAY_OF_MONTH));
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//			multiPickerDateAte.setMaxDate(new Date().getTime());
//			multiPickerDateDe.setMaxDate(new Date().getTime());
//
//		}
//
//		// Restore the current date, this will keep the min/max settings
//		// previously set into account.
////					multiPickerDateAte.updateDate(currentYear, currentMonth, currentDayOfMonth);
//
//
//		DialogInterface.OnClickListener dialogButtonListener = new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				switch (which) {
//					case DialogInterface.BUTTON_NEGATIVE: {
//						// user tapped "cancel"
//						dialog.dismiss();
//						break;
//					}
//					case DialogInterface.BUTTON_POSITIVE: {
//						// user tapped "set"
//						// here, use the "multiPickerDateAte" and "multiPickerTime" objects to retreive the date/time the user selected
//						getBizResp(idGr, goCallback);
//						break;
//					}
//					default: {
//						dialog.dismiss();
//						break;
//					}
//				}
//			}
//		};
//
//		AlertDialog.Builder builder;
//		builder = new AlertDialog.Builder(this);
//		builder.setView(multiPickerLayout);
//		builder.setPositiveButton("OK", dialogButtonListener);
//		builder.setNegativeButton("Cancel", dialogButtonListener);
//		builder.show();
}

@Override
public void onFragmentInteraction(ArrayList<String> xVals, ArrayList<String> entries, int listv) {
	grafItemContent.setTipo("BAR");
	getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.move_left_in_activity, android.R.anim.slide_out_right, R.anim.move_left_in_activity, R.anim.move_right_out_activity).replace(R.id.grafico_detail_container, BarChartFrag.newInstance(xVals, entries, R.id.bar_listviw, null, "BAR")).commit();

}

@Override
public void onBarFragmentInteraction(ArrayList<String> xVals, ArrayList<String> entries, int listv) {
	grafItemContent.setTipo("PIE");
	getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, R.anim.move_left_in_activity, android.R.anim.slide_out_right).replace(R.id.grafico_detail_container, PieChartFrag.newInstance(xVals, entries, R.id.pie_listviw)).commit();
}
}
