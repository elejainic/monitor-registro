package guineaecuatorial.gov.monitorregistros;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.helper.login.REQ5001186_Helper;
import guineaecuatorial.gov.monitorregistros.helper.login.RESP5001186_Helper;
import guineaecuatorial.gov.monitorregistros.model.login.RESP5001186_Model;
import guineaecuatorial.gov.monitorregistros.network.Networkhelper;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

/**
 * Represents an asynchronous login/registration task used to authenticate
 * the user.
 */
//	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
//
//		private final String mEmail;
//		private final String mPassword;
//
//		UserLoginTask(String email, String password) {
//			mEmail = email;
//			mPassword = password;
//		}
//
//		@Override
//		protected Boolean doInBackground(Void... params) {
//			// TODO: attempt authentication against a network service.
//
//			try {
//				// Simulate network access.
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				return false;
//			}
//
//			for (String credential : DUMMY_CREDENTIALS) {
//				String[] pieces = credential.split(":");
//				if (pieces[0].equals(mEmail)) {
//					// Account exists, return true if the password matches.
//					return pieces[1].equals(mPassword);
//				}
//			}
//
//			// TODO: register the new account here.
//			return true;
//		}
//
//		@Override
//		protected void onPostExecute(final Boolean success) {
//			mAuthTask = null;
//			showProgress(false);
//
//			if (success) {
//				finish();
//			} else {
//				mPasswordView.setError(getString(R.string.error_incorrect_password));
//				mPasswordView.requestFocus();
//			}
//		}
//
//		@Override
//		protected void onCancelled() {
//			mAuthTask = null;
//			showProgress(false);
//		}
//	}
private static final int READ_CONTACTS_PERMISSIONS_REQUEST = 1;
private static final String TAG = "LOGINActivity";
/**
 * A dummy authentication store containing known user names and passwords.
 * TODO: remove after connecting to a real authentication system.
 */

public static String sessid = "";
public static String email = "";
private static EditText mPasswordView;
ActionBar ab = null;
/**
 * Keep track of the login task to ensure we can cancel it if requested.
 */
//	private UserLoginTask mAuthTask = null;
// UI references.
private AutoCompleteTextView mEmailView;
private View mProgressView;
private View mLoginFormView;
Callback REQ5001186_Callback = new Callback() {


	@Override
	public void onFailure(Request request, IOException e) {
		Log.e("LoginActivity", "onFailure (line 192): ");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
//					mAuthTask = null;
				showProgress(false);
			}
		});
	}

	@Override
	public void onResponse(Response response) throws IOException {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
//					mAuthTask = null;
				showProgress(false);
			}
		});
		JSONObject json;
		try {
			json = new JSONObject(response.body().string());
			if (BuildConfig.DEBUG) Log.e("LoginActivity", "onResponse (line 214): json" + json);
			Log.e("GrafListFragment", "onResponse (line 87): ");
			RESP5001186_Model resp = RESP5001186_Helper.parseModel(json);
			if (resp != null) {
				sessid = resp.getSessid();
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (null != sessid) {
							Log.e("LoginActivity", "run (line 111): sessid " + sessid);
							Intent intent = new Intent();
							intent.setData(Uri.parse(sessid));
							setResult(RESULT_OK, intent);
							finish();
						} else {
							Log.e(TAG, "run (line 184): ERRRRRRRAAAAAAAAAAADOOOOOOOOOO");
							Toast.makeText(LoginActivity.this, getString(R.string.error_incorrect_user_or_password), Toast.LENGTH_LONG).show();
							Snackbar.make(mLoginFormView, getString(R.string.error_incorrect_user_or_password), Snackbar.LENGTH_INDEFINITE).setAction("OK", new OnClickListener() {
								@Override
								public void onClick(View v) {
									//on action click code
								}
							}).show();
						}
					}
				});

			} else {
				Log.e(TAG, "run (line 193): ERRRRRRRAAAAAAAAAAADOOOOOOOOOO");
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Snackbar.make(mLoginFormView, getString(R.string.error_incorrect_user_or_password), Snackbar.LENGTH_INDEFINITE).setActionTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorful_1)).setAction("OK", new OnClickListener() {
							@Override
							public void onClick(View v) {
								//on action click code
							}
						}).show();
					}
				});
			}
		} catch (JSONException e) {
			e.printStackTrace();
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Snackbar.make(mLoginFormView, "Algo correu mal! Tente de novo", Snackbar.LENGTH_INDEFINITE).setActionTextColor(ContextCompat.getColor(getBaseContext(), R.color.colorful_1)).setAction("OK", new OnClickListener() {
						@Override
						public void onClick(View v) {
							//on action click code
						}
					}).show();

				}
			});
		}

	}
};

@Override
public void onBackPressed() {
	super.onBackPressed();
	overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
}

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_login);
	initToolbar();
	// Set up the login form.
	mEmailView = (AutoCompleteTextView) findViewById(R.id.activity_login_email);
	populateAutoComplete();
	mPasswordView = (EditText) findViewById(R.id.password);
	mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
		@Override
		public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
			if (id == R.id.login || id == EditorInfo.IME_NULL) {
				attemptLogin();
				return true;
			}
			return false;
		}
	});
	Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
	mEmailSignInButton.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View view) {
			attemptLogin();
		}
	});
	mLoginFormView = findViewById(R.id.login_form);
	mProgressView = findViewById(R.id.login_progress);
}

private void populateAutoComplete() {
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) getPermissionToReadUserContacts();
	else if (Build.VERSION.SDK_INT >= 14) {
		// Use ContactsContract.Profile (API 14+)
		getSupportLoaderManager().initLoader(0, null, this);
	} else if (Build.VERSION.SDK_INT >= 8) {
		// Use AccountManager (API 8+)
		new SetupEmailAutoCompleteTask().execute(null, null);
	}
}

/**
 * Set up the {@link android.app.ActionBar}, if the API is available.
 */
private void initToolbar() {
	Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
	setSupportActionBar(toolbar);                   // Setting toolbar as the ActionBar with setSupportActionBar() call
	ab = getSupportActionBar();
	if (ab != null) {
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowTitleEnabled(true);

	}
}

/**
 * Attempts to sign in or register the account specified by the login form.
 * If there are form errors (invalid email, missing fields, etc.), the
 * errors are presented and no actual login attempt is made.
 */
public void attemptLogin() {
//		if (mAuthTask != null) {
//			return;
//		}
	// Reset errors.
	mEmailView.setError(null);
	mPasswordView.setError(null);
	// Store values at the time of the login attempt.
	email = mEmailView.getText().toString();
	final String password = mPasswordView.getText().toString();
	boolean cancel = false;
	View focusView = null;
	// Check for a valid password, if the user entered one.
	if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
		mPasswordView.setError(getString(R.string.error_invalid_password));
		focusView = mPasswordView;
		cancel = true;
	}
	// Check for a valid email address.
	if (TextUtils.isEmpty(email)) {
		mEmailView.setError(getString(R.string.error_field_required));
		focusView = mEmailView;
		cancel = true;
	}
//		else if (!isEmailValid(email)) {
//			mEmailView.setError(getString(R.string.error_invalid_email));
//			focusView = mEmailView;
//			cancel = true;
//		}
	if (cancel) {
		// There was an error; don't attempt login and focus the first
		// form field with an error.
		focusView.requestFocus();
	} else {
		// Show a progress spinner, and kick off a background task to
		// perform the user login attempt.
		showProgress(true);
		new Thread(new Runnable() {
			public void run() {
				new Networkhelper(LoginActivity.this).callWebservice(REQ5001186_Helper.prepareModel(email, password, "", "MGESTAO"), REQ5001186_Callback);
			}
		}).start();
//			mAuthTask = new UserLoginTask(email, password);
//			mAuthTask.execute((Void) null);
	}
}

private boolean isEmailValid(String email) {
	//TODO: Replace this with your own logic
	return email.contains("@");
}

private boolean isPasswordValid(String password) {
	//TODO: Replace this with your own logic
	return password.length() > 4;
}

/**
 * Shows the progress UI and hides the login form.
 */

public void showProgress(final boolean show) {
	// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
	// for very easy animations. If available, use these APIs to fade-in
	// the progress spinner.
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
		int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
		mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		mLoginFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			}
		});
		mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
		mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			}
		});
	} else {
		// The ViewPropertyAnimator APIs are not available, so simply show
		// and hide the relevant UI components.
		mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
		mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
	}
}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
	return super.onCreateOptionsMenu(menu);

}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
	int id = item.getItemId();
	if (id == android.R.id.home) {
		// This ID represents the Home or Up button. In the case of this
		// activity, the Up button is shown. Use NavUtils to allow users
		// to navigate up one level in the application structure. For
		// more details, see the Navigation pattern on Android Design:
		//
		// http://developer.android.com/design/patterns/navigation.html#up-vs-back
		//
//			ou getActivity().onBackPressed();
		finish();
		overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
		return true;

	}
	return super.onOptionsItemSelected(item);
}

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
@Override
public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
	return new CursorLoader(this,
			                       // Retrieve data rows for the device user's 'profile' contact.
			                       Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI, ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,
			                       // Select only email addresses.
			                       ContactsContract.Contacts.Data.MIMETYPE + " = ?", new String[]{ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE},
			                       // Show primary email addresses first. Note that there won't be
			                       // a primary email address if the user hasn't specified one.
			                       ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
}

@Override
public void onLoadFinished(android.support.v4.content.Loader<Cursor> cursorLoader, Cursor cursor) {
	List<String> emails = new ArrayList<>();
	cursor.moveToFirst();
	while (!cursor.isAfterLast()) {
		emails.add(cursor.getString(ProfileQuery.ADDRESS));
		cursor.moveToNext();
	}
	addEmailsToAutoComplete(emails);
}

@Override
public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
}

private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
	//Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
	ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoginActivity.this, android.R.layout.simple_dropdown_item_1line, emailAddressCollection);
	mEmailView.setAdapter(adapter);
}

// Called when the user is performing an action which requires the app to read the
// user's contacts
@TargetApi(Build.VERSION_CODES.M)
public void getPermissionToReadUserContacts() {
	// 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
	// checking the build version since Context.checkSelfPermission(...) is only available
	// in Marshmallow
	// 2) Always check for permission (even if permission has already been granted)
	// since the user can revoke permissions at any time through Settings
	if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
		// The permission is NOT already granted.
		// Check if the user has been asked about this permission already and denied
		// it. If so, we want to give more explanation about why the permission is needed.
		if (!ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.READ_CONTACTS)) {
			// Show our own UI to explain to the user why we need to read the contacts
			// before actually requesting the permission and showing the default UI
			showMessageOKCancel("Precisa de autorizar o acesso aos contactos para obter sugestões de email.", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, READ_CONTACTS_PERMISSIONS_REQUEST);
				}

			});
			return;
		}
		// Fire off an async request to actually get the permission
		// This will show the standard permission request dialog UI
		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, READ_CONTACTS_PERMISSIONS_REQUEST);

	}
}

private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
	new AlertDialog.Builder(LoginActivity.this).setMessage(message).setPositiveButton("OK", okListener).setNegativeButton("Cancel", null).create().show();
}

// Callback with the request from calling requestPermissions(...)
@Override
public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
	// Make sure it's our original READ_CONTACTS request
	if (requestCode == READ_CONTACTS_PERMISSIONS_REQUEST) {
		if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			Toast.makeText(this, "Read Contacts permission granted", Toast.LENGTH_SHORT).show();
			getSupportLoaderManager().initLoader(0, null, this);
		} else {
			Toast.makeText(this, "Read Contacts permission denied", Toast.LENGTH_SHORT).show();
		}
	} else {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}
}

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
private interface ProfileQuery {
	String[] PROJECTION = {ContactsContract.CommonDataKinds.Email.ADDRESS, ContactsContract.CommonDataKinds.Email.IS_PRIMARY,};

	int ADDRESS = 0;
	int IS_PRIMARY = 1;
}

/**
 * Use an AsyncTask to fetch the user's email addresses on a background thread, and update
 * the email text field with results on the main UI thread.
 */
class SetupEmailAutoCompleteTask extends AsyncTask<Void, Void, List<String>> {

	@Override
	protected List<String> doInBackground(Void... voids) {
		ArrayList<String> emailAddressCollection = new ArrayList<String>();
		// Get all emails from the user's contacts and copy them to a list.
		ContentResolver cr = getContentResolver();
		Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null, null);
		if (emailCur != null) {
			while (emailCur.moveToNext()) {
				String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				emailAddressCollection.add(email);
			}
		}
		if (emailCur != null) {
			emailCur.close();
		}
		return emailAddressCollection;
	}

	@Override
	protected void onPostExecute(List<String> emailAddressCollection) {
		addEmailsToAutoComplete(emailAddressCollection);
	}
}

}

