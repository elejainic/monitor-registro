package guineaecuatorial.gov.monitorregistros;

import android.app.Application;

import com.anupcowkur.reservoir.Reservoir;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.joanzapata.iconify.fonts.MaterialModule;

/**
 * Created by MarcosM on 03/08/2015.
 */
public class CustomApplication extends Application {
//	@Override
//	public void onCreate() {
//		super.onCreate();
//		Iconics.registerFont(new GoogleMaterial());
//		Iconics.registerFont(new FontAwesome());
//		//and all other fonts you want to use via your layouts
//	}


    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Reservoir.init(this, 2048000); //in bytes
        } catch (Exception e) {
            //failure
        }
        Iconify.with(new FontAwesomeModule())

                .with(new MaterialModule());

    }
}

