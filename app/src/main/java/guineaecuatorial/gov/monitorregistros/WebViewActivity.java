package guineaecuatorial.gov.monitorregistros;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class WebViewActivity extends AppCompatActivity {

private static final String TAG = "WebViewActivity";
private WebView webView;
private ProgressBar progressBar;

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_web_view);
	Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
	progressBar = (ProgressBar) findViewById(R.id.progress_horizontal);
	setSupportActionBar(toolbar);
	ActionBar ab = getSupportActionBar();
	if (ab != null) {
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowTitleEnabled(true);

	}
	webView = (WebView) findViewById(R.id.webView);
//	String htmlguia="<h2><font color='orange'>Como obter o Subsídio de Maternidade?\n</font></h2>" +
//			                "<p>Subsídio de Maternidade é um subsídio pecuniário, substituto do salário, concedido\n" +
//			                "às seguradas do INPS pelo período de 60 dias, por ocasião do parto nado vivo, e por\n" +
//			                "ocasião de parto nado morto ou interrupção da gravidez pelo número de dias prescritos\n" +
//			                "pelo médico. O pedido deve ser feito no INPS mediante a apresentação dos seguintes\n" +
//			                "documentos:\n" +
//			                "<li> Comprovativo da Baixa Médica.\n</li>" +
//			                "<li> Declaração comprovativa do parto.\n</li>" +
//			                "<p>O subsídio é atribuído durante quatro meses seguidos ou interpolados. É suspenso, em\n" +
//			                "caso de doença, enquanto a segurada tiver direito ao subsídio de doença.</p>";
//	String finalHtml = "<html><head>"
//			                   + "<style type=\"text/css\">li{color: #FFA500} span {color: #000}"
//			                   + "</style></head>"
//			                   + "<body>"
//			                   + htmlguia
//			                   + "</body></html>";
//
//
//	webView.loadDataWithBaseURL("", finalHtml, "text/html", "utf-8", "");
//	if(mParam1.equals("1"))
	webView.getSettings().setJavaScriptEnabled(true);
	webView.setWebChromeClient(new WebChromeClient() {
		public void onProgressChanged(WebView view, int progress) {
			// Activities and WebViews measure progress with different scales.
			// The progress meter will automatically disappear when we reach 100%
			Log.e(TAG, "onProgressChanged (line 68): pr" + progress);
			progressBar.setProgress(progress);
//			activity.setProgress(progress * 100);
		}
	});
	webView.setWebViewClient(new WebViewClient() {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setProgress(0);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			progressBar.setVisibility(View.GONE);
			progressBar.setProgress(100);
			super.onPageFinished(view, url);
		}

	});
//	webView.loadUrl("https://medium.com/@eshan/the-rise-of-the-ux-torturer-7fba47ba6f22#.j2jrr4rp8");
	String link = getIntent().getStringExtra("link");
	Log.e(TAG, "onCreate (line 93): link" + link);
	webView.loadUrl("https://nosiappsdev.gov.cv/redglobal_dev/" + link);
//	FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//	fab.setOnClickListener(new View.OnClickListener() {
//		@Override
//		public void onClick(View view) {
//			Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
//		}
//	});
}

public boolean onKeyDown(int keyCode, KeyEvent event) {
	if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
		webView.goBack();
		return true;
	}
	return super.onKeyDown(keyCode, event);
}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
	return super.onCreateOptionsMenu(menu);

}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
	int id = item.getItemId();
	if (id == android.R.id.home) {
		// This ID represents the Home or Up button. In the case of this
		// activity, the Up button is shown. Use NavUtils to allow users
		// to navigate up one level in the application structure. For
		// more details, see the Navigation pattern on Android Design:
		//
		// http://developer.android.com/design/patterns/navigation.html#up-vs-back
		//
//			ou getActivity().onBackPressed();
		finish();
		overridePendingTransition(R.anim.slide_enter, R.anim.slide_exit);
		return true;

	}
	return super.onOptionsItemSelected(item);
}
}
