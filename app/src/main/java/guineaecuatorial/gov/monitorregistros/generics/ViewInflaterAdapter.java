package guineaecuatorial.gov.monitorregistros.generics;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Marcos Brito
 *
 *  BaseAdapter generico para alistar items numa ListView
 *
 * @param <T>
 */
public class ViewInflaterAdapter<T> extends BaseAdapter
{
    private ArrayList<T> m_data;
    private ViewInflater m_inflater;
 
    public ViewInflaterAdapter (ViewInflater inflater)
    {
        m_inflater = inflater;
        m_data = new ArrayList<T>();
    }
 
    public ViewInflaterAdapter (ViewInflater inflater, List<T> data)
    {
        m_inflater = inflater;
        m_data = new ArrayList<T>(data);
    }
 
    /////////////////////////////////////////////////////
    //Typicial adapter code goes here
    /////////////////////////////////////////////////////
 
    public void setInflater(ViewInflater inflater)
    {
        m_inflater = inflater;
    }
 
    public T getItem(int pos)
    {
        return m_data.get(pos);
    }
    
	@Override
	public int getCount() {
		return m_data.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
 
    @Override
    public View getView(int pos, View convertView, ViewGroup parent)
    {
        return m_inflater != null ? m_inflater.inflate(this, pos, convertView, parent) : null;
    }
 
    public interface ViewInflater<T>
    {
        View inflate(ViewInflaterAdapter<T> adapter, int pos, View ConvertView, ViewGroup parent);
    }
}
