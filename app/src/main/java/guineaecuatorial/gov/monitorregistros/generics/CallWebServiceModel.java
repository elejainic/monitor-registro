package guineaecuatorial.gov.monitorregistros.generics;


import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.network.Encrypter;

public class CallWebServiceModel {

    private  String ClientId = "MOVEL";
    private String Service;
    private String Args;
    private String Transaction;


    public CallWebServiceModel(GenericArgs genericArgs, String args) {
        super();
        this.Service = genericArgs.getService();
        this.Args = args;
        this.Transaction = (generateTransactionHash(genericArgs.getContrato()));

    }

    private String generateTransactionHash(String contrato) {
        //TODO: Producao
        String  criptoKey = null;
        switch ( GraficoListActivity.ambientAtual){
//            case GraficoListActivity.DEP:

            case GraficoListActivity.DEV:
                criptoKey ="053433A8B91E7405857A39D15218D1DB";
                break;
            case GraficoListActivity.DEP: 

                if (contrato.equals("MOVEL19841"))
                    criptoKey = "053433A8B91E7405857A39D15218D1DB";

                else {
                    criptoKey = "445D2FDD9E26151197E1B7210D995726";
//                    criptoKey = "96C104DCA54178E0B23750D3605F936D";
                    ClientId="MOVELDEP";
                }

//                criptoKey =contrato.equals("MOVEL21141")?"053433A8B91E7405857A39D15218D1DB":"96C104DCA54178E0B23750D3605F936D";
                break;
            case GraficoListActivity.STAGE:
//                criptoKey ="94C1757FDEAB94DFE1A0035AC5306A66";
//                break;
            case GraficoListActivity.PROD:
                criptoKey = "246088EDBB6B85B375C3905FFF4FAC4B";
        }

//        String  criptoKey = "053433A8B91E7405857A39D15218D1DB" ;


        byte[] keyByte;
        keyByte = criptoKey != null ? criptoKey.getBytes() : new byte[0];
        byte[] initializationVectorByte = new byte[16];
        Encrypter mEncrypter = new Encrypter();

        try {
//            Log.e("CallWebServiceModel", "generateTransactionHash (line 36):  System.currentTimeMillis()" +System.currentTimeMillis()/1000);
            return mEncrypter.encrypt(ClientId + ";" + contrato
                            + ";<REFERENCE>;<"+
                            System.currentTimeMillis()
//                            "TIMESTAMP"
                            + ">", keyByte,
                    initializationVectorByte);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getService() {
        return Service;
    }

    public String getArgs() {
        return Args;
    }

    public String getTransaction() {
        return Transaction;
    }
}
