package guineaecuatorial.gov.monitorregistros.generics;


public class GenericArgs {
	
	public static class Rows{	
		
		public static class Row {
				private final String mimetype = "json";
				public String getMimetype() {
					return mimetype;
				}
		}
		
		public Rows(Row row) {
			super();
			this.row = row;
		}
		
		public Rows() {
			super();
		}

		private Row row;

		public Row getRow() {
			return row;
		}
		public void setRow(Row row) {
			 this.row=row;
		}
	}
	
	private Rows rows;
	private transient String contrato;
	private transient String service;

	public GenericArgs(String contrato, String service,Rows rows) {
		super();
		this.rows = rows;
		this.contrato = contrato;
		this.service = service;
	}
    
	public String getContrato() {
		return contrato;
	}

	public String getService() {
		return service;
	}

	public Rows getRows() {
		return rows;
	}
	
}
