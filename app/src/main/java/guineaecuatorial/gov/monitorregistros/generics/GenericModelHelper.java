package guineaecuatorial.gov.monitorregistros.generics;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * @author Marcos Brito
 * 
 * Class Helper que serve para fazer o JSON de acordo com o modelo que esta no GenericArgs
 *
 */
public class GenericModelHelper {

	public GenericModelHelper() {
	}

	public static CallWebServiceModel prepareModel(String contrato, String service, GenericArgs.Rows.Row row) {

//			new Networkhelper().callWebservice(REQ5003371_Helper.prepareModel("MOVEL", service),REQ5003371_CallBack);


		GenericArgs gm = new GenericArgs(contrato, service, new GenericArgs.Rows(row));

		String json = new Gson().toJson(gm);

		return new CallWebServiceModel(gm, json);
	}
	
	public static JSONObject getSucessJsonObject(JSONObject result) {

		try {
			String result1 = result.getString("Result");
			String replace = result1.replace("\"{\"", "{\"").replace("}\"", "}");
			JSONObject res = new JSONObject(replace);
			JSONObject rows = res.getJSONObject("rows");

			return rows.getJSONObject("row");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

//	static Callback REQ5003371_CallBack = new Callback() {
//
//		@Override
//		public void onFailure(Request request, IOException e) {
//			Log.e("Contratos", "onFailure (line 43): erro" + e.getMessage());
//		}
//
//		@Override
//		public void onResponse(Response response) throws IOException {
//			final JSONObject json;
//			try {
//				json = new JSONObject(response.body().string());
//
//				Log.e("Contratos", "onResponse (line 38): r"+ RESP5003371_Helper.parseModel(json));
//
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//
//		}
//	};
	
}
