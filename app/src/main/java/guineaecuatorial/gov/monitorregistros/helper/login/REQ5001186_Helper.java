package guineaecuatorial.gov.monitorregistros.helper.login;


import android.util.Log;

import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.login.REQ5001186_Model;
import guineaecuatorial.gov.monitorregistros.network.Contratos;

public class REQ5001186_Helper extends GenericModelHelper {

	public final static String METHODCALLBACK = "REQ5001186_CallBack";
	private final static String SERVICE = "5001186";
	private final static String[] contrato = new String[4];


	public REQ5001186_Helper() {
	}


	public static CallWebServiceModel prepareModel(String username, String password, String google_id, String forma_login) {
		if (null == contrato[GraficoListActivity.ambientAtual]) {
			Log.e("REQ5003529_Helper", "prepareModel (line 29):contrato[GraficoListActivity.ambientAtual] is empty");
			String tmpContrato = Contratos.getContrato(SERVICE);
			if (tmpContrato != null && tmpContrato.contains("MOVEL"))
				contrato[GraficoListActivity.ambientAtual] = tmpContrato;
		}
		String CONTRATO = contrato[GraficoListActivity.ambientAtual];
		if (CONTRATO == null) return null;

		return prepareModel(CONTRATO, SERVICE, new REQ5001186_Model(username, password, google_id, forma_login));

	}
}
