package guineaecuatorial.gov.monitorregistros.helper.subarea;

import android.util.Log;

import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.subarea.REQ5003529_Model;
import guineaecuatorial.gov.monitorregistros.network.Contratos;


public   class   REQ5003529_Helper   extends GenericModelHelper {

	private   final   static   String   SERVICE="5003529";

	private   static   String   CONTRATO = "";
//	private   final   static   String   CONTRATO="MOVEL20583";

	public   final   static   String   METHODCALLBACK="REQ5003529_CallBack";
	public   final   static   String[] contrato = new String[4];

	public   REQ5003529_Helper   (){}

	public   static CallWebServiceModel prepareModel   (){return prepareModel(CONTRATO,SERVICE,new REQ5003529_Model());}

	public   static   CallWebServiceModel   prepareModel   (String   session,
	                                                        int   idsubareas
	                                                       ){



		if(null ==contrato[GraficoListActivity.ambientAtual])
		{
			Log.e("REQ5003529_Helper", "prepareModel (line 29): is empty");
			String tmpContrato = Contratos.getContrato(SERVICE);
			if(tmpContrato != null && tmpContrato.contains("MOVEL"))
				contrato[GraficoListActivity.ambientAtual]=tmpContrato ;
		}
		CONTRATO= contrato[GraficoListActivity.ambientAtual];
		Log.e("REQ5003529_Helper", "prepareModel (line 38): _____________________");
		return   prepareModel(CONTRATO,SERVICE,new REQ5003529_Model(session,
				idsubareas));

	}
}

