package guineaecuatorial.gov.monitorregistros.helper.taskAvail;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskAvail.RESP5003947_Model;

public  class  RESP5003947_Helper  extends GenericModelHelper {

public  RESP5003947_Helper(){
}

public  static ArrayList<RESP5003947_Model.tarefas_disponiveisModel> parseModel(JSONObject result) {
	JSONObject  row  =  getSucessJsonObject(result);
	try{
		JSONObject  tarefas_disponiveis;
		if (row != null) {
			tarefas_disponiveis = row.getJSONObject("tarefas_disponiveis");

		JSONObject  js     =  tarefas_disponiveis.getJSONObject("row");
		row.remove("tarefas_disponiveis");
		JSONArray ja  =  new  JSONArray();
		row.put("tarefas_disponiveis",  ja.put(js));
		}
	}  catch  (JSONException e)  {
		Log.e("TENTA", "NAO E LISTA RECOVER");
	}
	return  jsonResultTransform(row);
}

public  static  ArrayList<RESP5003947_Model.tarefas_disponiveisModel>  jsonResultTransform(JSONObject row){

	if (null == row) return null;
	RESP5003947_Model r;
	try {
		r = new Gson().fromJson(row.toString(),RESP5003947_Model.class);
		return r.getTarefas_disponiveis();
	} catch (JsonSyntaxException e) {
		e.printStackTrace();
	}
	return null;
}


//private  class  ViewHolder  {
//	private TextView priority;
//	private  TextView  categoria_processo;
//	private  TextView  tarefa;
//	private  TextView  dt__entrada;
//	private  TextView  id;
//	private  TextView  proc_fk;
//	private  TextView  proc_num;
//	private  TextView  search;
//	private  TextView  env_fk;
//	private  TextView  prof_tp_fk;
//	private  TextView  org_fk;
//	private  TextView  user_assign_fk;
//}



}