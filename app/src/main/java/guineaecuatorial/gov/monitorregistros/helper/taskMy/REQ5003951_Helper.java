package guineaecuatorial.gov.monitorregistros.helper.taskMy;

import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskMy.REQ5003951_Model;
import guineaecuatorial.gov.monitorregistros.network.Contratos;

/**
 * Created by MarcosM on 02/11/2015.
 */

public class REQ5003951_Helper extends GenericModelHelper {

private final static String[] contrato = new String[4];

private final static String SERVICE = "5003951";


public static CallWebServiceModel prepareModel(ArrayList<REQ5003951_Model.lista_taskModel> lista_task) {
	if (null == contrato[GraficoListActivity.ambientAtual]) {
		String tmpContrato = Contratos.getContrato(SERVICE);
		if (tmpContrato != null && tmpContrato.contains("MOVEL"))
			contrato[GraficoListActivity.ambientAtual] = tmpContrato;
	}
	String CONTRATO = contrato[GraficoListActivity.ambientAtual];
	if (CONTRATO == null) return null;
	return prepareModel(CONTRATO, SERVICE, new REQ5003951_Model(lista_task));

}
}
