package guineaecuatorial.gov.monitorregistros.helper.taskAvail;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskAvail.RESP5003952_Model;

public class RESP5003952_Helper extends GenericModelHelper {

public RESP5003952_Helper() {
}

public static String parseModel(JSONObject result) {
	JSONObject row = getSucessJsonObject(result);
	return jsonResultTransform(row);
}

public static String jsonResultTransform(JSONObject row) {
	if (null == row) return null;
	RESP5003952_Model r;
	try {
		r = new Gson().fromJson(row.toString(), RESP5003952_Model.class);
		return r.getV_mensagem();
	} catch (JsonSyntaxException e) {
		e.printStackTrace();
	}
	return null;
}

}