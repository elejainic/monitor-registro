package guineaecuatorial.gov.monitorregistros.helper.taskAvail;

import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskAvail.REQ5003952_Model;
import guineaecuatorial.gov.monitorregistros.network.Contratos;

public class REQ5003952_Helper extends GenericModelHelper {


private final static String SERVICE = "5003952";

private final static String[] contrato = new String[4];


public static CallWebServiceModel prepareModel(String session_id, ArrayList<REQ5003952_Model.lista_taskModel> lista_task) {
	if (null == contrato[GraficoListActivity.ambientAtual]) {
		String tmpContrato = Contratos.getContrato(SERVICE);
		if (tmpContrato != null && tmpContrato.contains("MOVEL"))
			contrato[GraficoListActivity.ambientAtual] = tmpContrato;
	}
	String CONTRATO = contrato[GraficoListActivity.ambientAtual];
	if (CONTRATO == null) return null;
	return prepareModel(CONTRATO, SERVICE, new REQ5003952_Model(session_id, lista_task));

}
}
