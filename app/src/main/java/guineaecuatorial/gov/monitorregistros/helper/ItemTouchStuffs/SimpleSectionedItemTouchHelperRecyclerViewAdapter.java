package guineaecuatorial.gov.monitorregistros.helper.ItemTouchStuffs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.BuildConfig;
import guineaecuatorial.gov.monitorregistros.LoginActivity;
import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.WebViewActivity;
import guineaecuatorial.gov.monitorregistros.helper.taskAvail.REQ5003952_Helper;
import guineaecuatorial.gov.monitorregistros.helper.taskAvail.RESP5003952_Helper;
import guineaecuatorial.gov.monitorregistros.helper.taskMy.REQ5003951_Helper;
import guineaecuatorial.gov.monitorregistros.helper.taskMy.RESP5003951_Helper;
import guineaecuatorial.gov.monitorregistros.model.taskAvail.REQ5003952_Model;
import guineaecuatorial.gov.monitorregistros.model.taskAvail.RESP5003947_Model;
import guineaecuatorial.gov.monitorregistros.model.taskMy.REQ5003951_Model;
import guineaecuatorial.gov.monitorregistros.model.taskMy.RESP5003945_Model;
import guineaecuatorial.gov.monitorregistros.network.Networkhelper;

/**
 * Created by MarcosM on 15/06/2015.
 */
public class SimpleSectionedItemTouchHelperRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemTouchHelperAdapter {

private static final int SECTION_TYPE = 0;
private static final int MYTASK_TYPE = 1;
private static final int AVAILTASK_TYPE = 2;
private static final String TAG = "SimplSectIToucHRV";
private final LayoutInflater mLayoutInflater;
private Context mContext;
//private boolean mValid = true;
private int mSectionResourceId;
private int mTextResourceId;

private List<RESP5003945_Model.minhas_tarefasModel> myTasksValues = new ArrayList<>();
private List<RESP5003947_Model.tarefas_disponiveisModel> availTasksValues = new ArrayList<>();


public SimpleSectionedItemTouchHelperRecyclerViewAdapter(Context context, int sectionResourceId, int textResourceId, List<RESP5003945_Model.minhas_tarefasModel> items, List<RESP5003947_Model.tarefas_disponiveisModel> availTasks) {
	mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	mSectionResourceId = sectionResourceId;
	mTextResourceId = textResourceId;
	if (!items.isEmpty()) myTasksValues.addAll(items);
	if (!availTasks.isEmpty()) {
		availTasksValues.addAll(availTasks);
	}
	mContext = context;
//	mBaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//		@Override
//		public void onChanged() {
//			mValid = mBaseAdapter.getItemCount() > 0;
//			notifyDataSetChanged();
//		}
//
//		@Override
//		public void onItemRangeChanged(int positionStart, int itemCount) {
//			mValid = mBaseAdapter.getItemCount() > 0;
//			notifyItemRangeChanged(positionStart, itemCount);
//		}
//
//		@Override
//		public void onItemRangeInserted(int positionStart, int itemCount) {
//			mValid = mBaseAdapter.getItemCount() > 0;
//			notifyItemRangeInserted(positionStart, itemCount);
//		}
//
//		@Override
//		public void onItemRangeRemoved(int positionStart, int itemCount) {
//			mValid = mBaseAdapter.getItemCount() > 0;
//			notifyItemRangeRemoved(positionStart, itemCount);
//		}
//	});
}

@Override
public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int typeView) {
	if (typeView == SECTION_TYPE) {
		final View view = mLayoutInflater.inflate(mSectionResourceId, parent, false);
		return new SectionViewHolder(view, mTextResourceId);
	} else if (typeView == MYTASK_TYPE) {
		final View view = mLayoutInflater.inflate(R.layout.list_task_item, parent, false);
		return new MyTaskVH(view);
	} else if (typeView == AVAILTASK_TYPE) {
		final View view = mLayoutInflater.inflate(R.layout.list_task_item, parent, false);
		return new AvailTaskVH(view);
	}
	return null;
}

@Override
public void onBindViewHolder(RecyclerView.ViewHolder theViewHolder, final int position) {
	if (theViewHolder instanceof SectionViewHolder) {
		if (position == 0) ((SectionViewHolder) theViewHolder).title.setText("Minhas tarefas");
		else if (position == myTasksValues.size() + 1) {
			((SectionViewHolder) theViewHolder).title.setText("Tarefas disponíveis");
		}

	} else if (theViewHolder instanceof MyTaskVH) {
		MyTaskVH holder = (MyTaskVH) theViewHolder;
		RESP5003945_Model.minhas_tarefasModel valueAt = myTasksValues.get(sectionedPositionToPosition(position));
		int color;
		String letter;
		switch (Integer.valueOf(valueAt.getPriority())) {
			case 1:
				color = ContextCompat.getColor(mContext, R.color.colorful_1);
				letter = "U";
				break;
			case 2:
				color = ContextCompat.getColor(mContext, R.color.colorful_2);
				letter = "M";
				break;
			case 3:
			default:
				letter = "N";
				color = ContextCompat.getColor(mContext, R.color.colorful_4);
				break;
		}
//        Create a new TextDrawable for our image's background
		TextDrawable drawable = TextDrawable.builder().buildRound(letter, color);
		holder.mPriotityView.setImageDrawable(drawable);
		holder.mTipoTextView.setText(String.format("[%s] %s", valueAt.getProc_num(), valueAt.getTipo()));
		holder.mDescricaoTextView.setText(valueAt.getDescricao());
		holder.mDateTextView.setText(valueAt.getData_entrada());
		holder.actionOverflowMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(mContext, v);
				//Inflating the Popup using xml file
				popup.getMenuInflater().inflate(R.menu.popup_menu_my_task, popup.getMenu());
				//registering popup with OnMenuItemClickListener
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
							case R.id.see_execute_task:
								openWebview(position);
//									Intent intent = new Intent(context, WidgetContentActivity.class);
//									intent.putExtra(WidgetContentActivity.EXTRA_WIDGET_NAME, WidgetManager.WIDGET_SYNC_EXPORT);
//									intent.putExtra(WidgetContentActivity.EXTRA_LOCAL_DATA_UUID, localData.getUuid());
//									context.startActivity(intent);
//									Log.e("LocalDataAdapter", "onMenuItemClick (line 87): ");
								break;
							case R.id.dismiss_my_task:
//								Toast.makeText(mContext, "Não está implementado.", Toast.LENGTH_LONG).show();
								onItemDismiss(position);
//									Log.e("LocalDataAdapter", "onMenuItemClick (line 90): ");
//									break;
							default:
								break;

						}
						return true;
					}
				});
				popup.show();
			}

		});
		holder.mView.setOnClickListener(new View.OnClickListener() {


			@Override
			public void onClick(View v) {
				openWebview(position);




			}
		});
//		if (myTasksValues.get(position).getTipo().equals("PIE")) {
//			holder.mIconFontView.setText("{fa-pie-chart}");
//			holder.mIconFontView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorful_4));
//		}
//		if (myTasksValues.get(position).getTipo().equals("STACKBAR")) {
//			holder.mIconFontView.setText("{fa-bar-chart-o}");
//			holder.mIconFontView.setTextColor(Color.RED);
//			holder.mIconFontView.setShadowLayer(10, 3, 3, R.color.colorAccent);
//		}
	} else if (theViewHolder instanceof AvailTaskVH) {
		AvailTaskVH holder = (AvailTaskVH) theViewHolder;
		RESP5003947_Model.tarefas_disponiveisModel valueAt = availTasksValues.get(sectionedPositionToPosition(position));
		int color;
		String letter;
		switch (Integer.valueOf(valueAt.getPriority())) {
			case 1:
				color = ContextCompat.getColor(mContext, R.color.colorful_1);
				letter = "U";
				break;
			case 2:
				color = ContextCompat.getColor(mContext, R.color.colorful_2);
				letter = "M";
				break;
			case 3:
			default:
				letter = "N";
				color = ContextCompat.getColor(mContext, R.color.colorful_4);
				break;
		}
//        Create a new TextDrawable for our image's background
		TextDrawable drawable = TextDrawable.builder().buildRound(letter, color);
		holder.mPriotityView.setImageDrawable(drawable);
		holder.mTipoTextView.setText(String.format("[%s] %s", valueAt.getProc_num(), valueAt.getCategoria_processo()));
		holder.mDescricaoTextView.setText(valueAt.getTarefa());
		holder.mDateTextView.setText(valueAt.getDt__entrada());
		holder.actionOverflowMenu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				PopupMenu popup = new PopupMenu(mContext, v);
				//Inflating the Popup using xml file
				popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
				//registering popup with OnMenuItemClickListener
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {

							case R.id.dismiss_my_task:
//								Toast.makeText(mContext, "Não está implementado.", Toast.LENGTH_LONG).show();
								onItemDismiss(position);
//									Log.e("LocalDataAdapter", "onMenuItemClick (line 90): ");
//									break;
							default:
								break;

						}
						return true;
					}
				});
				popup.show();
			}

		});
		holder.mView.setOnClickListener(new View.OnClickListener() {


			@Override
			public void onClick(View v) {
//				int idSection = positionToSectionedPosition(position);
//				Intent intent = new Intent(mContext, WebViewActivity.class);
//				Bundle bundle = new Bundle();
//				bundle.putParcelable(GraficoDetailActivity.ARG_POSITION, new GrafItemContent.GrafItem(getValueAt(position).getId(), getValueAt(position).getNome(), getValueAt(position).getTipo(), getValueAt(position).getPeriodo()));
//				intent.putExtras(bundle);
//				mContext.startActivity(intent);
//				((Activity) mContext).overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
			}
		});
//		if (myTasksValues.get(position).getTipo().equals("PIE")) {
//			holder.mIconFontView.setText("{fa-pie-chart}");
//			holder.mIconFontView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorful_4));
//		}
//		if (myTasksValues.get(position).getTipo().equals("STACKBAR")) {
//			holder.mIconFontView.setText("{fa-bar-chart-o}");
//			holder.mIconFontView.setTextColor(Color.RED);
//			holder.mIconFontView.setShadowLayer(10, 3, 3, R.color.colorAccent);
//		}
	}

}

private void openWebview(int position) {
	Log.e("SimpleStringr", "onClick (line 398): ");
	Intent intent = new Intent(mContext, WebViewActivity.class);
	Bundle bundle = new Bundle();
	bundle.putString("link", myTasksValues.get(sectionedPositionToPosition(position)).getExecutar());
	intent.putExtras(bundle);
	mContext.startActivity(intent);
	((Activity) mContext).overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
}

@Override
public int getItemViewType(int position) {
	if (position == 0 || position == myTasksValues.size() + 1) return SECTION_TYPE;
	else if (position > 0 && position <= myTasksValues.size()) return MYTASK_TYPE;
	else if (position > myTasksValues.size() + 1) return AVAILTASK_TYPE;
	return position;
}

public int sectionedPositionToPosition(int sectionedPosition) {
	if (isSectionHeaderPosition(sectionedPosition)) {
		return RecyclerView.NO_POSITION;
	} else {
		int dispoPosition = myTasksValues.size() + 1;
		if (sectionedPosition > dispoPosition) {
			return sectionedPosition - 1 - dispoPosition;
		} else return sectionedPosition - 1;
	}
}

public boolean isSectionHeaderPosition(int position) {
	return position == 0 || position == myTasksValues.size() + 1;

}

@Override
public int getItemCount() {
	return myTasksValues.size() + availTasksValues.size() + 2;
}

@Override
public void onItemDismiss(int position) {
	if (position > myTasksValues.size() + 1) {
		final RESP5003947_Model.tarefas_disponiveisModel tarefas_disponiveisModel = availTasksValues.get(sectionedPositionToPosition(position));
		myTasksValues.add(0, new RESP5003945_Model.minhas_tarefasModel(tarefas_disponiveisModel.getPriority(), tarefas_disponiveisModel.getCategoria_processo(), tarefas_disponiveisModel.getTarefa(), "", tarefas_disponiveisModel.getDt__entrada(), "", tarefas_disponiveisModel.getId(), "", tarefas_disponiveisModel.getProc_num(), "", "", "", tarefas_disponiveisModel.getOrg_fk(), "", "", ""));
		availTasksValues.remove(sectionedPositionToPosition(position) + 1);
		notifyItemRemoved(position);
		notifyItemInserted(1);
		new Thread(new Runnable() {
			public void run() {
				ArrayList<REQ5003952_Model.lista_taskModel> templista_taskModel = new ArrayList<>();
				templista_taskModel.add(new REQ5003952_Model.lista_taskModel(tarefas_disponiveisModel.getId()));
				new Networkhelper(mContext).callWebservice(REQ5003952_Helper.prepareModel(LoginActivity.sessid, templista_taskModel), new Callback() {
					@Override
					public void onFailure(Request request, IOException e) {
						((Activity) mContext).runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext, "Não foi possivel conectar com servidor e assumir", Toast.LENGTH_LONG).show();
							}
						});
					}

					@Override
					public void onResponse(Response response) throws IOException {
						JSONObject json;
						try {
							json = new JSONObject(response.body().string());
							if (BuildConfig.DEBUG)
								Log.e("SimpleSecito", "onResponse (line 214): json" + json);
							Log.e("GrafListFragment", "onResponse (line 87): ");
							String resp = RESP5003952_Helper.parseModel(json);
							if (resp != null) {
								Log.e(TAG, "onResponse (line 366): ");

							}
						} catch (JSONException e) {
							e.printStackTrace();
							Log.e(TAG, "onResponse (line 366): ");
						}

					}
				});
			}
		}).start();

	} else if (position > 0 && position <= myTasksValues.size()) {
		final RESP5003945_Model.minhas_tarefasModel minhas_tarefasModel = myTasksValues.get(sectionedPositionToPosition(position));
		availTasksValues.add(0, new RESP5003947_Model.tarefas_disponiveisModel(minhas_tarefasModel.getPriority(), minhas_tarefasModel.getTipo(), minhas_tarefasModel.getDescricao(), minhas_tarefasModel.getData_entrada(), minhas_tarefasModel.getId(), "", minhas_tarefasModel.getProc_num(), "", "", 0, 0, 0));
		myTasksValues.remove(sectionedPositionToPosition(position));
		notifyItemRemoved(position);
		notifyItemInserted(myTasksValues.size() + 2);
		new Thread(new Runnable() {
			public void run() {
				ArrayList<REQ5003951_Model.lista_taskModel> temp = new ArrayList<>();
				temp.add(new REQ5003951_Model.lista_taskModel(minhas_tarefasModel.getId()));
				new Networkhelper(mContext).callWebservice(REQ5003951_Helper.prepareModel(temp), new Callback() {
					@Override
					public void onFailure(Request request, IOException e) {
						((Activity) mContext).runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(mContext, "Não foi possivel conectar com servidor e libertar", Toast.LENGTH_LONG).show();
							}
						});
					}

					@Override
					public void onResponse(Response response) throws IOException {
						JSONObject json;
						try {
							json = new JSONObject(response.body().string());
							if (BuildConfig.DEBUG)
								Log.e("SimpleSecito", "onResponse (line 214): json" + json);
							Log.e("GrafListFragment", "onResponse (line 87): ");
							String resp = RESP5003951_Helper.parseModel(json);
							if (resp != null) {
								Log.e(TAG, "onResponse (line 366): ");

							}
						} catch (JSONException e) {
							e.printStackTrace();
							Log.e(TAG, "onResponse (line 366): ");
						}

					}
				});
			}
		}).start();

	}
//	notifyDataSetChanged();

}

@Override
public boolean onItemMove(int fromPosition, int toPosition) {
	//TODO: Marcos fazer o caso de passar de disponiveis para minhas e vice versa
	if (fromPosition > 0 && fromPosition <= myTasksValues.size() && toPosition > 0 && toPosition <= myTasksValues.size()) {
		Collections.swap(myTasksValues, sectionedPositionToPosition(fromPosition), sectionedPositionToPosition(toPosition));
		notifyItemMoved(fromPosition, toPosition);
	} else if (fromPosition > myTasksValues.size() + 1 && toPosition > myTasksValues.size() + 1) {
		Collections.swap(availTasksValues, sectionedPositionToPosition(fromPosition), sectionedPositionToPosition(toPosition));
		notifyItemMoved(fromPosition, toPosition);
	}
	return true;
}

public void setMyTasks(List<RESP5003945_Model.minhas_tarefasModel> myTasksArrayList) {
//	if(myTasksValues.isEmpty())
//		myTasksValues.addAll(myTasksArrayList);
//	else {
	myTasksValues.clear();
	myTasksValues.addAll(myTasksArrayList);
//	}

	notifyDataSetChanged();
}

public void setAvailTask(List<RESP5003947_Model.tarefas_disponiveisModel> availTasksArrayList) {
//	if(availTasksValues.isEmpty())
//		availTasksValues.addAll(availTasksArrayList);
//	else {
	availTasksValues.clear();
	availTasksValues.addAll(availTasksArrayList);
//	}
	notifyDataSetChanged();
}

public static class SectionViewHolder extends RecyclerView.ViewHolder {

	public TextView title;

	public SectionViewHolder(View view, int mTextResourceid) {
		super(view);
		title = (TextView) view.findViewById(mTextResourceid);
		title.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), "Roboto-Medium.ttf"));
	}
}


public static class MyTaskVH extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
	public View mView;
	public ImageView mPriotityView;

	public TextView mDateTextView;
	public TextView mDescricaoTextView;
	public TextView mTipoTextView;

	public TextView actionOverflowMenu;


	public MyTaskVH(View view) {
		super(view);
		mView = view;
		mPriotityView = (ImageView) view.findViewById(R.id.img_priority_item_letter);
		mDescricaoTextView = (TextView) view.findViewById(R.id.txt_descricao_task);
		mTipoTextView = (TextView) view.findViewById(R.id.txt_tipo_task_name);
		mDateTextView = (TextView) view.findViewById(R.id.txt_date_task);
		actionOverflowMenu = (TextView) itemView.findViewById(R.id.action_overflow_menu_list_item);
//			mIconFontView.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), "Raleway-Bold.ttf"));
		mTipoTextView.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), "Raleway-Regular.ttf"));
	}

	@Override
	public void onItemSelected() {
//			itemView.setBackgroundColor(Color.LTGRAY);
	}

	@Override
	public void onItemClear() {
//			itemView.setBackgroundColor(0);
	}

}

public static class AvailTaskVH extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
	public View mView;
	public ImageView mPriotityView;
	public TextView mDateTextView;
	public TextView mDescricaoTextView;
	public TextView mTipoTextView;
	public TextView actionOverflowMenu;


	public AvailTaskVH(View view) {
		super(view);
		mView = view;
		mPriotityView = (ImageView) view.findViewById(R.id.img_priority_item_letter);
		mDescricaoTextView = (TextView) view.findViewById(R.id.txt_descricao_task);
		mTipoTextView = (TextView) view.findViewById(R.id.txt_tipo_task_name);
		mDateTextView = (TextView) view.findViewById(R.id.txt_date_task);
		actionOverflowMenu = (TextView) itemView.findViewById(R.id.action_overflow_menu_list_item);
//			mIconFontView.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), "Raleway-Bold.ttf"));
		mTipoTextView.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), "Raleway-Regular.ttf"));
	}

	@Override
	public void onItemSelected() {
//			itemView.setBackgroundColor(Color.LTGRAY);
	}



	@Override
	public void onItemClear() {
//			itemView.setBackgroundColor(0);
	}

}

}