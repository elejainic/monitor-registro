package guineaecuatorial.gov.monitorregistros.helper.taskAvail;

import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskAvail.REQ5003947_Model;
import guineaecuatorial.gov.monitorregistros.network.Contratos;

public class REQ5003947_Helper extends GenericModelHelper {

private final static String[] contrato = new String[4];

private final static String SERVICE = "5003947";


public static CallWebServiceModel prepareModel(int p_env_fk, String p_session_id, int p_prof_type_fk, int p_org_fk, String p_status_notstarted, String numero_processo, String tipo_processo, String organica, String prioridade, String data_inicio, String data_fim, String search) {
	if (null == contrato[GraficoListActivity.ambientAtual]) {
		String tmpContrato = Contratos.getContrato(SERVICE);
		if (tmpContrato != null && tmpContrato.contains("MOVEL"))
			contrato[GraficoListActivity.ambientAtual] = tmpContrato;
	}
	String CONTRATO = contrato[GraficoListActivity.ambientAtual];
	if (CONTRATO == null) return null;
	return prepareModel(CONTRATO, SERVICE, new REQ5003947_Model(p_env_fk, p_session_id, p_prof_type_fk, p_org_fk, p_status_notstarted, numero_processo, tipo_processo, organica, prioridade, data_inicio, data_fim, search));

}
}
