package guineaecuatorial.gov.monitorregistros.helper.dashboard;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import guineaecuatorial.gov.monitorregistros.BuildConfig;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.dashboard.RESP5003940_Model;


public class RESP5003940_Helper extends GenericModelHelper {

public RESP5003940_Helper() {
}

public static List<RESP5003940_Model.grafsModel> parseModel(JSONObject result) throws JSONException {
	JSONObject row = getSucessJsonObject(result);
	try {
		JSONObject grafs;
		if (row != null) {
			grafs = row.getJSONObject("grafs");
			JSONObject js = grafs.getJSONObject("row");
			row.remove("grafs");
			JSONArray ja = new JSONArray();
			row.put("grafs", ja.put(js));
		} else return null;
	} catch (JSONException e) {
		Log.e("TENTA", "NAO E LISTA RECOVER");
	}
	JSONArray grafs = null;
	try {
		grafs = row.getJSONArray("grafs");
	} catch (JSONException e) {
		e.printStackTrace();
	}
	if (grafs != null) {
		for (int i = 0; i < grafs.length(); i++) {
			try {
				JSONObject dados = grafs.getJSONObject(i).getJSONObject("dados").getJSONObject("rows");
				JSONObject js = dados.getJSONObject("row");
				grafs.getJSONObject(i).getJSONObject("dados").remove("rows");
				JSONArray ja = new JSONArray();
				grafs.getJSONObject(i).getJSONObject("dados").put("rows", ja.put(js));
				dados = grafs.getJSONObject(i).getJSONObject("dados").getJSONArray("rows").getJSONObject(0).getJSONObject("dados");
				js = dados.getJSONObject("row");
				grafs.getJSONObject(i).getJSONObject("dados").getJSONArray("rows").getJSONObject(0).remove("dados");
				ja = new JSONArray();
				grafs.getJSONObject(i).getJSONObject("dados").getJSONArray("rows").getJSONObject(0).put("dados", ja.put(js));

			} catch (JSONException e) {
				Log.e("TENTA", "NAO E LISTA RECOVER");
			}
		}
		row.remove("grafs");
		row.put("grafs", grafs);
	}
	return jsonResultTransform(row);
}

public static List<RESP5003940_Model.grafsModel> jsonResultTransform(JSONObject row) {
	if (BuildConfig.DEBUG) Log.e("RESP5003940_Helper", "jsonResultTransform (line 27): row" + row);
	if (null == row) return null;
	RESP5003940_Model r;
	try {
		r = new Gson().fromJson(row.toString(), RESP5003940_Model.class);
		return r.getGrafs();
	} catch (JsonSyntaxException e) {
		e.printStackTrace();
	}
	return null;
}


}