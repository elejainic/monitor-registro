package guineaecuatorial.gov.monitorregistros.helper.thegraf;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.thegraf.RESP5003531_Model;


/**
 * Created by MarcosM on 23/06/2015.
 */
public class RESP5003531_Helper extends GenericModelHelper {

    public RESP5003531_Helper() {
    }

    public static ArrayList<RESP5003531_Model.dadosModel> jsonResultTransform(JSONObject row) {
        RESP5003531_Model r = new Gson().fromJson(row.toString(), RESP5003531_Model.class);
        return r.getDados();
    }

    public static RESP5003531_Model jsonResultTransformTudo(JSONObject row) {

        if (null == row) return null;

        try {
            return new Gson().fromJson(row.toString(), RESP5003531_Model.class);

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

        return null;


    }

    public static RESP5003531_Model parseModel(JSONObject result) {
        JSONObject row = getSucessJsonObject(result);
        try {
            JSONObject dados;
            if (row != null) {
                dados = row.getJSONObject("dados");

                JSONObject js = dados.getJSONObject("row");
                row.remove("dados");
                JSONArray ja = new JSONArray();
                row.put("dados", ja.put(js));
            }
        } catch (JSONException e) {
            Log.e("TENTA", "NAO E LISTA RECOVER");
        }
        return jsonResultTransformTudo(row);
    }


}
