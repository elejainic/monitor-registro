package guineaecuatorial.gov.monitorregistros.helper.taskMy;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskMy.RESP5003945_Model;

public  class  RESP5003945_Helper  extends GenericModelHelper {

public  RESP5003945_Helper(){
}

public  static  ArrayList<RESP5003945_Model.minhas_tarefasModel>  parseModel(JSONObject result) {
	JSONObject  row  =  getSucessJsonObject(result);
	try{
		JSONObject  minhastarefas;
		if (row != null) {
			minhastarefas = row.getJSONObject("minhas_tarefas");

		JSONObject  js     =  minhastarefas.getJSONObject("row");
		row.remove("minhas_tarefas");
		JSONArray ja  =  new  JSONArray();
		row.put("minhas_tarefas",  ja.put(js));
		}
	}  catch  (JSONException e)  {
		Log.e("TENTA", "NAO E LISTA RECOVER");
	}
	return  jsonResultTransform(row);
}

public  static ArrayList<RESP5003945_Model.minhas_tarefasModel> jsonResultTransform(JSONObject row){
	if (null == row) return null;

	RESP5003945_Model r ;
	try {
		r = new Gson().fromJson(row.toString(),RESP5003945_Model.class);
		return r.getMinhas_tarefas();
	} catch (JsonSyntaxException e) {
		e.printStackTrace();
	}
	return null;
}




}