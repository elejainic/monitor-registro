package guineaecuatorial.gov.monitorregistros.helper.taskByColaboradores;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskByColaboradores.RESP5003950_Model;

public class RESP5003950_Helper extends GenericModelHelper {

public RESP5003950_Helper() {
}

public static ArrayList<RESP5003950_Model.colaboradoresModel> parseModel(JSONObject result) {
	JSONObject row = getSucessJsonObject(result);
	try {
		JSONObject colaboradores;
		if (row != null) {
			colaboradores = row.getJSONObject("colaboradores");
			JSONObject js = colaboradores.getJSONObject("row");
			row.remove("colaboradores");
			JSONArray ja = new JSONArray();
			row.put("colaboradores", ja.put(js));
		}
	} catch (JSONException e) {
		Log.e("TENTA", "NAO E LISTA RECOVER");
	}
	return jsonResultTransform(row);
}

public static ArrayList<RESP5003950_Model.colaboradoresModel> jsonResultTransform(JSONObject row) {
	if (null == row) return null;
	RESP5003950_Model r;
	try {
		r = new Gson().fromJson(row.toString(), RESP5003950_Model.class);
		return r.getColaboradores();
	} catch (JsonSyntaxException e) {
		e.printStackTrace();
	}
	return null;
}
//
//public  static  ViewInflaterAdapter<listaModel>  newAdapter(JSONObject  result,  Context context){
//	RESP5003950ListAdaperInflater  infrater  =  new  RESP5003950_Helper().new  RESP5003950ListAdaperInflater();
//	return  new  ViewInflaterAdapter<listaModel>(infrater,parseModel(result));
//}
//
//public  class  RESP5003950ListAdaperInflater  implements  ViewInflater<listaModel> {
//	@Override
//	public View inflate(ViewInflaterAdapter<listaModel>  adapter,  int  position,  View  convertView,  ViewGroup parent){
//		ViewHolder  viewHolder;
//		if  (convertView  ==  null) {
//			LayoutInflater inflater  =  LayoutInflater.from(parent.getContext());
//			convertView  =  inflater.inflate(R.layout.generic_pickers_listitem_layout,parent,  false);
//			viewHolder  =  new  ViewHolder();
//			viewHolder.nome  =  (TextView)  convertView.findViewById(android.R.id.TextView_nome);
//			viewHolder.contacto  =  (TextView)  convertView.findViewById(android.R.id.TextView_contacto);
//			viewHolder.n_tarefas  =  (TextView)  convertView.findViewById(android.R.id.TextView_n_tarefas);
//			viewHolder.n_atendimento  =  (TextView)  convertView.findViewById(android.R.id.TextView_n_atendimento);
//			viewHolder.media_tempo  =  (TextView)  convertView.findViewById(android.R.id.TextView_media_tempo);
//			viewHolder.ranking  =  (TextView)  convertView.findViewById(android.R.id.TextView_ranking);
//			viewHolder.percent  =  (TextView)  convertView.findViewById(android.R.id.TextView_percent);
//			convertView.setTag(viewHolder);
//		}  else  {
//			viewHolder  =  (ViewHolder)  convertView.getTag();
//		}
//
//		listaModel  item  =  adapter.getItem(position);
//		if  (item  != null)  {
//			viewHolder.nome.setText(item.getNome());
//			viewHolder.contacto.setText(item.getContacto());
//			viewHolder.n_tarefas.setText(item.getN_tarefas());
//			viewHolder.n_atendimento.setText(item.getN_atendimento());
//			viewHolder.media_tempo.setText(item.getMedia_tempo());
//			viewHolder.ranking.setText(item.getRanking());
//			viewHolder.percent.setText(item.getPercent());
//		}
//		return  convertView;
//	}
//}

public static void makeLog(String jsonger) {
	Log.e("SEVICO  RESP5003950:", "LOG:  " + jsonger);
}

}