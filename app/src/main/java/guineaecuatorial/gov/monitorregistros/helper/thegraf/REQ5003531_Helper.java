package guineaecuatorial.gov.monitorregistros.helper.thegraf;

import android.util.Log;

import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.thegraf.REQ5003531_Model;
import guineaecuatorial.gov.monitorregistros.network.Contratos;


/**
 * Created by MarcosM on 23/06/2015.
 */

public   class   REQ5003531_Helper   extends GenericModelHelper {

	private   final   static   String   SERVICE="5003531";
	private     static   String   CONTRATO= "";

//	private   final   static   String   CONTRATO="MOVEL20582";
public   final   static   String[] contrato = new String[4];


	public   REQ5003531_Helper   (){}

	public   static CallWebServiceModel prepareModel   (){return prepareModel(CONTRATO,SERVICE,new REQ5003531_Model());}

	public   static   CallWebServiceModel   prepareModel   (String   session,
	                                                        String   idgraf,
	                                                        String   periodo,
	                                                        String   data_de,
	                                                        String   data_ate, int id_subarea){
		if(null ==contrato[GraficoListActivity.ambientAtual])
		{
			Log.e("REQ5003531_Helper", "prepareModel (line 33): _________________________");
			String tmpContrato = Contratos.getContrato(SERVICE);
			if(tmpContrato != null && tmpContrato.contains("MOVEL"))
				contrato[GraficoListActivity.ambientAtual]=tmpContrato ;
		}
		CONTRATO= contrato[GraficoListActivity.ambientAtual];
		Log.e("REQ5003531_Helper", "prepareModel (line 40): ");
		return   prepareModel(CONTRATO,SERVICE,new   REQ5003531_Model(session,
				idgraf,
				periodo,
				data_de,
				data_ate, id_subarea));

	}
}
