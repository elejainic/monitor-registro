package guineaecuatorial.gov.monitorregistros.helper.subarea;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.subarea.RESP5003529_Model;


/**
 * Created by MarcosM on 20/06/2015.
 */
public class RESP5003529_Helper extends GenericModelHelper {

    public RESP5003529_Helper() {
    }

    public static List<RESP5003529_Model.subareasModel> parseModel(JSONObject result) throws JSONException {
//		Log.e("RESP5003529_Helper", "parseModel (line 31): " + result);
        JSONObject row = getSucessJsonObject(result);

        try {
            JSONObject SubAreas;
            if (row != null) {
                SubAreas = row.getJSONObject("subareas");
                JSONObject js = SubAreas.getJSONObject("row");
                row.remove("subareas");
                JSONArray ja = new JSONArray();
                row.put("subareas", ja.put(js));
            } else return null;
        } catch (JSONException e) {
            Log.e("TENTA", "NAO E LISTA RECOVER");
        }


        JSONArray SubAreas = null;
        try {
            SubAreas = row.getJSONArray("subareas");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (SubAreas != null) {
            for (int i = 0; i < SubAreas.length(); i++) {
                try {
                    JSONObject Grafs = null;

                    Grafs = SubAreas.getJSONObject(i).getJSONObject("grafs").getJSONObject("rows");
                    JSONObject js = Grafs.getJSONObject("row");
                    SubAreas.getJSONObject(i).getJSONObject("grafs").remove("rows");
                    JSONArray ja = new JSONArray();
                    SubAreas.getJSONObject(i).getJSONObject("grafs").put("rows", ja.put(js));

                } catch (JSONException e) {
                    Log.e("TENTA", "NAO E LISTA RECOVER");
                    continue;
                }
            }

            row.remove("subareas");

            row.put("subareas", SubAreas);
        }


        return jsonResultTransform(row);
    }

    public static List<RESP5003529_Model.subareasModel> jsonResultTransform(JSONObject row) {
        Log.e("RESP5003529_Helper", "jsonResultTransform (line 27): row" + row);
        if (null == row) return null;
        RESP5003529_Model r;
        try {
            r = new Gson().fromJson(row.toString(), RESP5003529_Model.class);
            return r.getSubareas();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
//	public static ViewInflaterAdapter<listaModel> newAdapter(JSONObject result, Context context) {
//		RESP5003529ListAdaperInflater infrater = new RESP5003529_Helper().new RESP5003529ListAdaperInflater();
//		return new ViewInflaterAdapter<listaModel>(infrater, parseModel(result));
//	}

    public static void makeLog(String jsonger) {
        Log.e("SEVICO  RESP5003529:", "LOG:  " + jsonger);
    }

//	public class RESP5003529ListAdaperInflater implements ViewInflater<listaModel> {
//		@Override
//		public View inflate(ViewInflaterAdapter<listaModel> adapter, int position, View convertView, ViewGroup parent) {
//			ViewHolder viewHolder;
//			if (convertView == null) {
//				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//				convertView = inflater.inflate(R.layout.generic_pickers_listitem_layout, parent, false);
//				viewHolder = new ViewHolder();
//				viewHolder.nome = (TextView) convertView.findViewById(android.R.id.TextView_nome);
//				viewHolder.grafs = (TextView) convertView.findViewById(android.R.id.TextView_grafs);
//				convertView.setTag(viewHolder);
//			} else {
//				viewHolder = (ViewHolder) convertView.getTag();
//			}
//
//			listaModel item = adapter.getItem(position);
//			if (item != null) {
//				viewHolder.nome.setText(item.getNome());
//				viewHolder.grafs.setText(item.getGrafs());
//			}
//			return convertView;
//		}
//	}
//
//	private class ViewHolder {
//		private TextView nome;
//		private TextView grafs;
//	}

}
