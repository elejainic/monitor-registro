package guineaecuatorial.gov.monitorregistros.helper.login;

import com.google.gson.Gson;

import org.json.JSONObject;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.login.RESP5001186_Model;


public class RESP5001186_Helper extends GenericModelHelper {

	public RESP5001186_Helper() {
	}


	public static RESP5001186_Model parseModel(JSONObject result) {
		JSONObject row = getSucessJsonObject(result);

		if (row != null) {
			return new Gson().fromJson(row.toString(), RESP5001186_Model.class);
		}
		return null;
	}


}