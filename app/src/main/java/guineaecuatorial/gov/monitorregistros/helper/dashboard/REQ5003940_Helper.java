package guineaecuatorial.gov.monitorregistros.helper.dashboard;


import android.util.Log;

import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.dashboard.REQ5003940_Model;
import guineaecuatorial.gov.monitorregistros.network.Contratos;


public class REQ5003940_Helper extends GenericModelHelper {


    public final static String[] contrato = new String[4];
    private final static String SERVICE = "5003940";


    public REQ5003940_Helper() {
    }


    public static CallWebServiceModel prepareModel(int dashbord_id, String session, String lingua) {


        if (null == contrato[GraficoListActivity.ambientAtual]) {
            Log.e("REQ5003940_Helper", "prepareModel (line 29):contrato[GraficoListActivity.ambientAtual] is empty");
            String tmpContrato = Contratos.getContrato(SERVICE);
            if (tmpContrato != null && tmpContrato.contains("MOVEL"))
                contrato[GraficoListActivity.ambientAtual] = tmpContrato;
        }
        String CONTRATO = contrato[GraficoListActivity.ambientAtual];
        if (CONTRATO == null) return null;

        return prepareModel(CONTRATO, SERVICE, new REQ5003940_Model(dashbord_id, session, lingua));

    }
}
