package guineaecuatorial.gov.monitorregistros.helper.taskByColaboradores;


import guineaecuatorial.gov.monitorregistros.GraficoListActivity;
import guineaecuatorial.gov.monitorregistros.generics.CallWebServiceModel;
import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskByColaboradores.REQ5003950_Model;
import guineaecuatorial.gov.monitorregistros.network.Contratos;

public class REQ5003950_Helper extends GenericModelHelper {


private final static String SERVICE = "5003950";


private final static String[] contrato = new String[4];


public static CallWebServiceModel prepareModel(String p_session, String data_inicio, String data_fim, String numero_processo, String prioridade, String tipo_processo, String tipo_etapa, String organica) {
	if (null == contrato[GraficoListActivity.ambientAtual]) {
		String tmpContrato = Contratos.getContrato(SERVICE);
		if (tmpContrato != null && tmpContrato.contains("MOVEL"))
			contrato[GraficoListActivity.ambientAtual] = tmpContrato;
	}
	String CONTRATO = contrato[GraficoListActivity.ambientAtual];
	if (CONTRATO == null) return null;
	return prepareModel(CONTRATO, SERVICE, new REQ5003950_Model(p_session, data_inicio, data_fim, numero_processo, prioridade, tipo_processo, tipo_etapa, organica));

}
}

