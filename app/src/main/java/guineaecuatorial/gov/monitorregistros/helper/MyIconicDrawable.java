package guineaecuatorial.gov.monitorregistros.helper;

import android.content.Context;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.typeface.IIcon;
import com.mikepenz.iconics.typeface.ITypeface;

/**
 * Created by MarcosM on 23/08/2015.
 */
public class MyIconicDrawable extends IconicsDrawable {
	private Context context;

	public MyIconicDrawable(Context context) {
		super(context);
	}

	public MyIconicDrawable(Context context, Character icon) {
		super(context, icon);
	}

	public MyIconicDrawable(Context context, String icon) {
		super(context, icon);
	}

	public MyIconicDrawable(Context context, IIcon icon) {
		super(context, icon);
		this.context=context;
	}

	public MyIconicDrawable(Context context, ITypeface typeface, IIcon icon) {
		super(context, typeface, icon);
	}

//	@Override
//	public ConstantState getConstantState() {
//		return new DrawableContainer.DrawableContainerState() {
//			@Override
//			public Drawable newDrawable() {
//				return null;
//			}
//		} ;
//	}
}
