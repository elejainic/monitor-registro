package guineaecuatorial.gov.monitorregistros.helper.taskMy;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import guineaecuatorial.gov.monitorregistros.generics.GenericModelHelper;
import guineaecuatorial.gov.monitorregistros.model.taskMy.RESP5003951_Model;

/**
 * Created by MarcosM on 02/11/2015.
 */
public class RESP5003951_Helper extends GenericModelHelper {

public RESP5003951_Helper() {
}

public static String parseModel(JSONObject result) {
	JSONObject row = getSucessJsonObject(result);
//	try{
//		JSONObject    =  row.getJSONObject("");
//		JSONObject  js     =  .getJSONObject("row");
//		row.remove("");
//		JSONArray ja  =  new  JSONArray();
//		row.put("",  ja.put(js));
//	}  catch  (JSONException e)  {
//		Log.e("TENTA", "NAO E LISTA RECOVER");
//	}
	return jsonResultTransform(row);
}

public static String jsonResultTransform(JSONObject row) {
	if (null == row) return null;
	RESP5003951_Model r;
	try {
		r = new Gson().fromJson(row.toString(), RESP5003951_Model.class);
		return r.getV_mensagem_sucess();
	} catch (JsonSyntaxException e) {
		e.printStackTrace();
	}
	return null;
}

}