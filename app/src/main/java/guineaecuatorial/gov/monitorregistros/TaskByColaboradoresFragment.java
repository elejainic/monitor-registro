package guineaecuatorial.gov.monitorregistros;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.adapter.ColaboradoresRecyclerAdapter;
import guineaecuatorial.gov.monitorregistros.helper.taskByColaboradores.REQ5003950_Helper;
import guineaecuatorial.gov.monitorregistros.helper.taskByColaboradores.RESP5003950_Helper;
import guineaecuatorial.gov.monitorregistros.model.taskByColaboradores.RESP5003950_Model;
import guineaecuatorial.gov.monitorregistros.network.Networkhelper;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TaskByColaboradoresFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TaskByColaboradoresFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
private ProgressBar progressBar;
private RecyclerView recyclerView;
private SwipeRefreshLayout swipeToRefesh;
private JSONObject json;
private List<RESP5003950_Model.colaboradoresModel> taskByColaborArrayList = new ArrayList<>();
private ColaboradoresRecyclerAdapter colaboTaskAdapter;
// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private static final String ARG_PARAM1 = "param1";
//private static final String ARG_PARAM2 = "param2";
// TODO: Rename and change types of parameters
//private String mParam1;
//private String mParam2;


public TaskByColaboradoresFragment() {
	// Required empty public constructor
}

/**
 * Use this factory method to create a new instance of
 * this fragment using the provided parameters.
 * <p/>
 * // * @param param1 Parameter 1.
 * // * @param param2 Parameter 2.
 *
 * @return A new instance of fragment TaskByColaboradoresFragment.
 */
// TODO: Rename and change types and number of parameters
public static TaskByColaboradoresFragment newInstance() {
//	TaskByColaboradoresFragment fragment = new TaskByColaboradoresFragment();
//	Bundle args = new Bundle();
//	args.putString(ARG_PARAM1, param1);
//	args.putString(ARG_PARAM2, param2);
//	fragment.setArguments(args);
	return new TaskByColaboradoresFragment();
}

@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
//	if (getArguments() != null) {
//		mParam1 = getArguments().getString(ARG_PARAM1);
//		mParam2 = getArguments().getString(ARG_PARAM2);
//	}
}

@Override
public void onAttach(Context context) {
	super.onAttach(context);
	progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_spinner);
}

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	// Inflate the layout for this fragment
	View view = inflater.inflate(R.layout.fragment_task_by_colaboradores, container, false);
	// Set the adapter
	recyclerView = (RecyclerView) view.findViewById(R.id.list);
	swipeToRefesh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_task_list);
	swipeToRefesh.setOnRefreshListener(this);
	// Set OnItemClickListener so we can be notified on item clicks
	return view;

}

@Override
public void onViewCreated(View view, Bundle savedInstanceState) {
	super.onViewCreated(view, savedInstanceState);
	if (savedInstanceState == null) {
		swipeToRefesh.setRefreshing(true);
		progressBar.setVisibility(View.VISIBLE);
		GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
//	llm.setOrientation(GridLayoutManager.VERTICAL);
		recyclerView.setLayoutManager(mLayoutManager);
		colaboTaskAdapter = new ColaboradoresRecyclerAdapter(getContext(), taskByColaborArrayList);
		recyclerView.setAdapter(colaboTaskAdapter);
		DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
		defaultItemAnimator.setAddDuration(1000);
		defaultItemAnimator.setRemoveDuration(500);
		recyclerView.setItemAnimator(defaultItemAnimator);
		networkCall4ListColaboradores();
	}

}

private void networkCall4ListColaboradores() {
	new Thread(new Runnable() {
		public void run() {
			new Networkhelper(getActivity()).callWebservice(REQ5003950_Helper.prepareModel(LoginActivity.sessid, "30-09-2015", "30-10-2015", "", "", "", "", ""), getTaskByColaboradores());
		}
	}).start();


}


@NonNull
private Callback getTaskByColaboradores() {
	return new Callback() {
		@Override
		public void onFailure(Request request, IOException e) {
			if (isAdded()) getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					progressBar.setVisibility(View.GONE);
					swipeToRefesh.setRefreshing(false);
					Toast.makeText(getActivity(), "Não foi possivel conectar com servidor", Toast.LENGTH_LONG).show();

				}
			});
		}

		@Override
		public void onResponse(Response response) throws IOException {
			try {
				json = new JSONObject(response.body().string());
				if (BuildConfig.DEBUG) Log.e("GrafListFragment", "onResponse (line 87): " + json);
				ArrayList<RESP5003950_Model.colaboradoresModel> colaboradoresModel = RESP5003950_Helper.parseModel(json);
				if (colaboradoresModel != null) {
					taskByColaborArrayList.clear();
					taskByColaborArrayList.addAll(colaboradoresModel);
				}
				try {
					if (isAdded()) getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							progressBar.setVisibility(View.GONE);
							swipeToRefesh.setRefreshing(false);
							if (null != taskByColaborArrayList) setupRecyclerView();

						}
					});
				} catch (Exception e) {
					e.printStackTrace();
					if (isAdded()) getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getContext(), "Erro ", Toast.LENGTH_LONG).show();
							progressBar.setVisibility(View.GONE);
							swipeToRefesh.setRefreshing(false);
						}
					});
				}

			} catch (JSONException e) {
				e.printStackTrace();
				if (isAdded()) getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getContext(), "Erro ", Toast.LENGTH_LONG).show();
						progressBar.setVisibility(View.GONE);
						swipeToRefesh.setRefreshing(false);
					}
				});

			}

		}
	};
}


private void setupRecyclerView() {
	if (!taskByColaborArrayList.isEmpty())
		colaboTaskAdapter.setColaboradores(taskByColaborArrayList);
}


@Override
public void onRefresh() {
	networkCall4ListColaboradores();
}
}
