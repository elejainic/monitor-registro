package guineaecuatorial.gov.monitorregistros.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.highlight.Range;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import java.math.BigDecimal;
import java.util.ArrayList;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.adapter.AdapterPieBar;
import guineaecuatorial.gov.monitorregistros.util.ColorTemplate;
import guineaecuatorial.gov.monitorregistros.util.MyMarkerViewBar;
import guineaecuatorial.gov.monitorregistros.util.MyValueFormatter;


public class BarChartFrag extends SimpleFragment implements OnChartValueSelectedListener {

    private static final String ARG_PARAM1 = "xVals";
    private static final String ARG_PARAM2 = "entries";
    private static final String ARG_PARAM3 = "legs";
    private static final String ARG_PARAM4 = "listv";
    public static BarChart mChart;
    public static boolean flagShowSoTotal = false;
private static String barType;
int mListv;
    private ArrayList<String> mEntries;
    private ArrayList<String> mXvals;
    private ArrayList<String> mLegs;
    private ListView listview;
    private ArrayList<String> mLegsNoRep;
    private OnBarFragmentInteractionListener mListener;
private Typeface tf;
    private AdapterPieBar adapter;
private ArrayList<String> mEntriesNoRepit;


public static Fragment newInstance(ArrayList<String> xVals, ArrayList<String> entries, int listv, ArrayList<String> legs, String barType) {
    BarChartFrag.barType = barType;
    BarChartFrag fragment = new BarChartFrag();
        Bundle args = new Bundle();
        args.putStringArrayList(ARG_PARAM1, xVals);
        args.putStringArrayList(ARG_PARAM2, entries);
        args.putStringArrayList(ARG_PARAM3, legs);
        args.putInt(ARG_PARAM4, listv);
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mXvals = getArguments().getStringArrayList(ARG_PARAM1);
            mEntries = getArguments().getStringArrayList(ARG_PARAM2);
            mLegs = getArguments().getStringArrayList(ARG_PARAM3);
            mListv = getArguments().getInt(ARG_PARAM4);

        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (isAdded()) tf = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Light.ttf");

        try {
            mListener = (OnBarFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        // create a new chart object
//        mChart = new BarChart(getActivity());


        return inflater.inflate(R.layout.frag_simple_bar, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

//		parentLayout = (RelativeLayout) v.findViewById(R.id.parentLayout);
        mChart = (BarChart) v.findViewById(R.id.barChart1);

//		mChart.setDescription(GrafItemContent.ITEMS.get(GraficoDetailActivity.intExtra).getNome() + " * " + GraficoDetailActivity.dataUltima);
//		mChart.setDescriptionPosition(400, 15);
        mChart.setDescription("");
        mChart.setOnChartValueSelectedListener(this);

        MyMarkerViewBar mv = new MyMarkerViewBar(getActivity(), R.layout.custom_marker_view);

        mChart.setMarkerView(mv);

        mChart.setDrawValueAboveBar(null == mLegs);
        mChart.setHighlightEnabled(false);
        mChart.setNoDataText(getActivity().getString(R.string.no_data_text_description));

        mChart.setDrawHighlightArrow(true);
        mChart.setDrawGridBackground(false);


//	    mChart.setBackgroundColor(getResources().getColor(R.color.amarelinho));

        Legend l = mChart.getLegend();

        l.setEnabled(null != mLegs);
        l.setWordWrapEnabled(true);
        l.setTypeface(tf);
        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setTextColor(Color.BLACK);
        l.setTextSize(14);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setTypeface(tf);
//	    xAxis.setTextColor(Color.BLACK);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setSpaceBetweenLabels(1);

//		xAxis.setEnabled(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(tf);
//	    YAxis yr = mChart.getAxisRight();
//	    yr.setTypeface(tf);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        leftAxis.setDrawLabels(null != mLegs);
        leftAxis.setTextSize(8);
        leftAxis.setValueFormatter(new MyValueFormatter());

        mChart.getAxisRight().setEnabled(false);


        try {
            mChart.setData(generateBarData(mXvals, mEntries));
//			mChart.setData(generateBarData(1, 20000, 12));
            if (mChart.getData().getYValueSum() > 99999999.0) mChart.setMaxVisibleValueCount(0);

            mChart.animateX(1300);
            mChart.animateY(1000, Easing.EasingOption.EaseOutBack);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Erro nos dados!", Toast.LENGTH_LONG).show();
        }


//		mChart.animateX(1000, Easing.EasingOption.EaseOutBack);


//        // programatically add the chart
//        FrameLayout parent = (FrameLayout) v.findViewById(R.id.parentLayout);
//        parent.addView(mChart);
        prepareLisView();
//        initSpinner();
        setHasOptionsMenu(true);
    }

    private BarData generateBarData(ArrayList<String> xVals, ArrayList<String> strEntries) {
        ArrayList<BarDataSet> sets = new ArrayList<>();
        ArrayList<BarEntry> entries = new ArrayList<>();
        ArrayList<String> mXvalsNoRepit = new ArrayList<>();
        mEntriesNoRepit = new ArrayList<>();
        mLegsNoRep = new ArrayList<>();
        mLegsNoRep.clear();
        mXvalsNoRepit.clear();
        for (int i = 0; i < xVals.size(); i++) {
            if (!mXvalsNoRepit.contains(xVals.get(i))) {
                mXvalsNoRepit.add(xVals.get(i));
            }

        }

        if (null != mLegs) {

            for (String leg : mLegs) {
                if (!mLegsNoRep.contains(leg) && leg != null) mLegsNoRep.add(leg);
            }
            int size = mLegsNoRep.size();
            //Marcos: para o caso de stack vir com um só valor
            if (size <= 1) for (int x = 0; x < strEntries.size(); x++) {

                if (strEntries.get(x).length() > 6) {
                    BigDecimal val = new BigDecimal(strEntries.get(x));
                    entries.add(new BarEntry(val.floatValue(), x, val));
                } else entries.add(new BarEntry(Float.valueOf(strEntries.get(x)), x));

            }
            else {

                //Faz um for entre todos os valores
                //Identifica os valores repetidos e constroi para cada barra
                for (int x = 0; x < mXvalsNoRepit.size(); x++) {
                    String xCurrentValue = mXvalsNoRepit.get(x);
                    //Marcos: para o caso de stack vir com um só valor

                    float[] vals = new float[size];
                    for (int l = 0; l < mLegsNoRep.size(); l++) {


                        String leg = mLegsNoRep.get(l);
                        for (int i = 0; i < xVals.size(); i++) {
                            if (!xVals.get(i).equals(xCurrentValue)) continue;
                            if (mLegs.get(i) != null && mLegs.get(i).equals(leg)) {

                                vals[l] = Float.valueOf(strEntries.get(i));


                            }
                        }

                    }
                    entries.add(new BarEntry(vals, x));


                }
            }

        } else {
            for (int x = 0; x < strEntries.size(); x++) {

                if (strEntries.get(x).length() > 6) {
                    BigDecimal val = new BigDecimal(strEntries.get(x));
                    entries.add(new BarEntry(val.floatValue(), x, val));
                } else entries.add(new BarEntry(Float.valueOf(strEntries.get(x)), x));

            }
        }


        String stackBar = "";
        String[] strStackLabels = new String[mLegsNoRep.size()];
        //para Stack bar
        if (null != mLegs) {
            if (mLegsNoRep.size() == 1) stackBar = mLegsNoRep.get(0);
            else stackBar = ".";
            for (int i = 0; i < mLegsNoRep.size(); i++) {
                strStackLabels[i] = mLegsNoRep.get(i);
            }


        }
        BarDataSet ds1 = new BarDataSet(entries, stackBar);
        ds1.setColors(getColors(), getActivity());

        ds1.setDrawValues(null == mLegs);
        ds1.setValueTextColor(Color.BLACK);

        ds1.setValueFormatter(new MyValueFormatter());
        ds1.setValueTextSize(12f);
        if (null != mLegs) ds1.setStackLabels(strStackLabels);
//           Marcos: depois do tratamento, hora de constroir o adapter

        for (int i = 0; i < mXvalsNoRepit.size(); i++) {
            mEntriesNoRepit.add(entries.get(i).getData() == null ? "" + ds1.getYValForXIndex(i) : entries.get(i).getData().toString());
        }
        adapter = new AdapterPieBar(getContext(), mXvalsNoRepit, mEntriesNoRepit);
        sets.add(ds1);
        return new BarData(mXvalsNoRepit, sets);
    }

    private void prepareLisView() {
        Log.e("BarChartFrag", "prepareLisView (line 248): ");
        if (-1 == mListv) {
            return;
        }
        if (!isAdded()) return;
        listview = (ListView) getActivity().findViewById(mListv);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                flagShowSoTotal = true;
                Log.e("BarChartFrag", "onItemClick (line 274): ");
                mChart.getBarData().getDataSetByIndex(0).setHighLightAlpha(59);
//				mChart.getBarData().getDataSetByIndex(0).setHighLightColor(ColorTemplate.PASTEL_COLORS[position]);
                Log.e("BarChartFrag", "onItemClick (line 276): ");
//				mChart.highlightValue(position, 0);
                Float yValue = Float.valueOf(mEntriesNoRepit.get(position));
                mChart.highlightValues(new Highlight[]{new Highlight(position, 0, mLegsNoRep.size() - 1, new Range(-500f, yValue))});

                mChart.centerViewTo(position, yValue, YAxis.AxisDependency.LEFT);
            }


        });

    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
    /*	String stackName;
        String totalStac;
		NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###,###,###.##");
		int backColor;
		if (h.getStackIndex() != -1) {

			totalStac = nf.format((double) ((BarEntry) e).getVals()[h.getStackIndex()]);
			stackName = mLegsNoRep.get(h.getStackIndex()) + ": " + totalStac + " / ";
			backColor = h.getStackIndex();
			Snackbar snackbar = Snackbar.make(getView(), stackName + "Total: " + nf.format(e.getVal())
//				+ mChart.getXValue(e.getXIndex())
					, Snackbar.LENGTH_LONG);
//		+ "  " + mChart.getXValue(e.getXIndex())

			View snackBarView = snackbar.getView();

//		int pastelColor = ColorTemplate.PASTEL_COLORS[e.getXIndex()];

			snackBarView.setBackgroundColor(getColors()[backColor]);
			snackbar.show();
		}
*/
        BarChartFrag.flagShowSoTotal = false;
//		listview.setAdapter(adapter);
//		listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setSelection(e.getXIndex());
//		listview.setItemChecked(e.getXIndex(), true);

    }


    @Override
    public void onNothingSelected() {

    }

    private int[] getColors() {
        int[] colors;
        if (null != mLegs) {
            int stacksize = mLegsNoRep.size();
            colors = new int[stacksize];
            System.arraycopy(ColorTemplate.VORDIPLOM_COLORS, 0, colors, 0, stacksize);
        } else colors = ColorTemplate.PASTEL_COLORS;
        return colors;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//		getActivity().getMenuInflater().inflate(R.menu.detail, menu);
        if (isAdded() && BarChartFrag.barType.equals("BAR"))
            menu.add(0, 3, 0, "Pie").setIcon(new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_pie_chart).actionBar().colorRes(R.color.spinnerColorPeriodo)).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 3:
                if (mListener != null) {
                    mListener.onBarFragmentInteraction(mXvals, mEntries, mListv);
                }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnBarFragmentInteractionListener {
        // TODO: Update argument type and name
        void onBarFragmentInteraction(ArrayList<String> xVals, ArrayList<String> entries, int listv);
    }
}
