package guineaecuatorial.gov.monitorregistros.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.TreeSet;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.util.ColorTemplate;
import guineaecuatorial.gov.monitorregistros.util.MyMarkerView;
import guineaecuatorial.gov.monitorregistros.util.MyValueFormatter;


public class LineFragment extends SimpleFragment {

    private static final String ARG_PARAM1 = "xVals";
    private static final String ARG_PARAM2 = "entries";
    private static final String ARG_PARAM3 = "legs";
    private static final String ARG_PARAM4 = "listv";
    private static final String ARG_PARAM5 = "forList";
    private static final String TAG = LineFragment.class.getSimpleName();
    public static ArrayList<String> mLegsNoRep;
    public static LineChart mChart;
    private static ArrayList<Integer> colors = new ArrayList<>();
    private static boolean mLegNoRepBool[] = new boolean[50];
    private ArrayList<String> mEntries;
    private ArrayList<String> mXvals;
    private ArrayList<String> mXvalsNoRepit;
    private ArrayList<String> mLegs;
    private ArrayList<ArrayList<Entry>> listEntries = null;
    private LineData d;
    private int mListvi;
    private ListView listview = null;
private Spinner spinner;
    private Typeface tf;
    private CustomAdapter adapter;

    public LineFragment() {
    }

    public static Fragment newInstance(ArrayList<String> xVals, ArrayList<String> entries, ArrayList<String> legs, int listvie) {

        LineFragment fragment = new LineFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(ARG_PARAM1, xVals);
        args.putStringArrayList(ARG_PARAM2, entries);
        args.putStringArrayList(ARG_PARAM3, legs);
        args.putInt(ARG_PARAM4, listvie);
        fragment.setArguments(args);
        return fragment;

    }

    public static Fragment newInstance() {
        return new LineFragment();
    }

//	@Override
//	public void onAttach(Activity activity) {
//		super.onAttach(activity);
//	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (null == savedInstanceState) {
            Log.e("LineFragment", "onCreate (line 103): ");
            setmLegNoRepBool(new boolean[50]);
        }


        if (getArguments() != null) {
            mXvals = getArguments().getStringArrayList(ARG_PARAM1);
            mEntries = getArguments().getStringArrayList(ARG_PARAM2);
            mLegs = getArguments().getStringArrayList(ARG_PARAM3);
            mListvi = getArguments().getInt(ARG_PARAM4);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


//		Button btnexpand= (Button) v.findViewById(R.id.btn_expand_line);
//
//		btnexpand.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				mChart.invalidate();
//				if(listview.getVisibility()==View.GONE)
//					listview.setVisibility(View.VISIBLE);
//				else
//					listview.setVisibility(View.GONE);
//			}
//		});

        return inflater.inflate(R.layout.frag_simple_line, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        tf = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        mChart = (LineChart) v.findViewById(R.id.lineChart1);
//		mChart.setDescription(GrafItemContent.ITEMS.get(GraficoDetailActivity.intExtra).getNome() + " * " + GraficoDetailActivity.dataUltima);
//		mChart.setDescriptionPosition(400, 10);
        mChart.setDescription("");

        mChart.setHighlightEnabled(false);
//		mChart.setBackgroundColor(getResources().getColor(R.color.amarelinho));
        mChart.setDrawGridBackground(false);
        mChart.setMaxVisibleValueCount(1);
//        mChart.setDrawVerticalGrid(false);

        mChart.setScaleYEnabled(true);
        mChart.setScrollbarFadingEnabled(true);
        mChart.setNoDataText(getActivity().getString(R.string.no_data_text_description));

        mChart.setDescriptionTextSize(20);


//        mChart.setNoDataTextDescription("Não foi possivel retirar dados");

        MyMarkerView mv;
        try {
            mChart.setData(generateLineData(mXvals, mEntries));
            //		Para filtro
            if (null == savedInstanceState) setmLegNoRepBool(new boolean[mLegsNoRep.size()]);

            mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);

            mChart.setMarkerView(mv);

            mChart.animateXY(1500, 2400);


            Legend l = mChart.getLegend();
            l.setTypeface(tf);
            l.setFormSize(10);
            l.setTextSize(11);
            l.setForm(Legend.LegendForm.SQUARE);
            l.setXEntrySpace(4f);
            l.setFormToTextSpace(2f);
            l.setWordWrapEnabled(true);
            l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
            l.setEnabled(true);


            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTypeface(tf);
            leftAxis.setTextSize(4);
            leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
            leftAxis.setValueFormatter(new MyValueFormatter());
            leftAxis.setDrawLabels(true);
            leftAxis.setStartAtZero(false);

            mChart.getAxisRight().setEnabled(false);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setTypeface(tf);
            if (!isAdded()) return;
            listview = (ListView) getActivity().findViewById(R.id.linechart_listview);
            adapter = new CustomAdapter(getActivity());

            for (String xVal : mXvalsNoRepit) {
                adapter.addItem(xVal);
            }
            if (!LineFragment.mChart.isEmpty()) {
                listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                listview.setAdapter(adapter);
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Toast.makeText(getContext(), "dsa", Toast.LENGTH_SHORT).show();
                        mChart.highlightValue(position, 0);
                    }
                });
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("LineFragment", "onCreateView (line 184): ERROOOOOOOOOOO");
        }

        setHasOptionsMenu(true);
    }

    private LineData generateLineData(ArrayList<String> xVals, ArrayList<String> strEntries) {

        LineDataSet set;
        colors.clear();
        ArrayList<Entry> entries = new ArrayList<>();
//        ArrayList<Entry> entries2 = new ArrayList<Entry>();
        listEntries = new ArrayList<>();
        listEntries.clear();
        ArrayList<LineDataSet> dataSets = new ArrayList<>();

        mXvalsNoRepit = new ArrayList<>();
        mXvalsNoRepit.clear();
        ArrayList<String> mDiasLeg = new ArrayList<>();
        mLegsNoRep = new ArrayList<>();
        ArrayList<LineDataSet> ds = new ArrayList<>();


        for (int i = 0; i < xVals.size(); i++) {
            if (!mXvalsNoRepit.contains(xVals.get(i)) && null != xVals.get(i)) {
                mXvalsNoRepit.add(xVals.get(i));
            }

        }
//        mLegsNoRep
        for (String leg : mLegs) {
            if (!mLegsNoRep.contains(leg) && leg != null) mLegsNoRep.add(leg);
        }

        int exclude = 0;
        for (int l = 0; l < mLegsNoRep.size(); l++) {
            String leg = mLegsNoRep.get(l);
            if (!mLegNoRepBool[l]) {
                entries.clear();
                mDiasLeg.clear();
                exclude++;
                continue;
            }
//			Log.e(TAG, "vmaosod odojdoasdosa " + leg);
            for (int i = 0; i < mLegs.size(); i++) {
                if (mLegs.get(i) != null && mLegs.get(i).equals(leg)) mDiasLeg.add(xVals.get(i));
                else mDiasLeg.add("0");
            }
            //TODO Marcos ver isto            entries.add(new Entry(Float.valueOf(strEntries.get(i)), i, "dsa"));
            for (int j = 0; j < mXvalsNoRepit.size(); j++) {
//                   if(.equals(leg)){
                int ind = mDiasLeg.indexOf(mXvalsNoRepit.get(j));
//
                if (ind != -1) {
                    if (strEntries.get(ind).length() > 6) {
                        BigDecimal val = new BigDecimal(strEntries.get(ind));
                        entries.add(new Entry(val.floatValue(), j, val));
                    } else entries.add(new Entry(Float.valueOf(strEntries.get(ind)), j));
                }

//				GraficoDetailActivity.objects.add(new ContentItem(mXvalsNoRepit.get(j), mXvalsNoRepit.get(j), leg));
//				else entries.add(new Entry(0, j));
            }


            listEntries.add((ArrayList<Entry>) entries.clone());
            set = new LineDataSet(listEntries.get(l - exclude), leg);
            int color = ContextCompat.getColor(getContext(), ColorTemplate.PASTEL_COLORS[l]);
            set.setColor(color);
            set.setCircleColor(color);

            set.setHighlightLineWidth(3);
            set.setValueFormatter(new MyValueFormatter());
//            set.setColor(ColorTemplate.PASTEL_COLORS[l]);
            ds.add(set);
            colors.add(ds.get(l - exclude).getColor(0));
            ds.get(l - exclude).setLineWidth(3f);
            ds.get(l - exclude).setCircleSize(5f);

            dataSets.add(ds.get(l - exclude));


            entries.clear();
            mDiasLeg.clear();


            if (l > 10) break;
        }

        d = new LineData(mXvalsNoRepit, dataSets);
        Log.e(TAG, "bom" + d.toString());
        return d;
    }

    public void setmLegNoRepBool(boolean[] mLegNoRepBool) {
        LineFragment.mLegNoRepBool = mLegNoRepBool;
        for (int i = 0; i < mLegNoRepBool.length; i++) {
            LineFragment.mLegNoRepBool[i] = true;
        }
        Log.e("LineFragment", "setmLegNoRepBool (line 296): ");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (isAdded()) {
            getActivity().getMenuInflater().inflate(R.menu.detail, menu);
            menu.findItem(R.id.action_filter).setIcon(new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_filter).actionBar().colorRes(R.color.spinnerColorPeriodo));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.action_filter:

                mChart.setDrawMarkerViews(false);
//				mChart.invalidate();
                if (!mChart.isEmpty())
                FilterDialogFragment().show();

        }

        return super.onOptionsItemSelected(item);
    }

    private void listViewPosTratamento() {
        CustomAdapter adapter = new CustomAdapter(getActivity());
        if (!LineFragment.mChart.isEmpty()) {
            mChart.setDrawMarkerViews(true);
            mChart.invalidate();
            mChart.refreshDrawableState();


            for (String xVal : mXvalsNoRepit) {
                adapter.addItem(xVal);
            }


            listview.setAdapter(adapter);
//					listview.invalidateViews();
//					listview.invalidate();
        } else listview.setAdapter(adapter);
    }


    public Dialog FilterDialogFragment() {

        DialogInterface.OnClickListener dialogButtonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                listViewPosTratamento();

//				switch (which) {
//					case DialogInterface.BUTTON_NEGATIVE: {
//						// user tapped "cancel"
//						dialog.dismiss();
//						break;
//					}
//					case DialogInterface.BUTTON_POSITIVE: {
//						// user tapped "set"
//						// here, use the "multiPickerDateAte" and "multiPickerTime" objects to retreive the date/time the user selected
//
//
//						break;
//					}
//					default: {
                dialog.dismiss();
//						break;
//					}
//				}
            }


        };


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //set dialog title
        builder.setTitle("Escolha as evoluções");
        CharSequence[] mLegNoRepChar = new CharSequence[mLegsNoRep.size()];

        for (int i = 0; i < mLegsNoRep.size(); i++) {
            String str = mLegsNoRep.get(i);
            SpannableString boldOption = new SpannableString(str);
            boldOption.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), ColorTemplate.PASTEL_COLORS[i])), 0, str.length(), 0);
            mLegNoRepChar[i] = boldOption;

        }


        //MultiChoice for double shot and mixed with soda
        builder.setMultiChoiceItems(mLegNoRepChar, mLegNoRepBool, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                mLegNoRepBool[which] = isChecked;
                mChart.setData(generateLineData(mXvals, mEntries));
                mChart.invalidate();

            }
        });

        //			dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        builder.setPositiveButton("OK", dialogButtonListener);
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                listViewPosTratamento();
            }
        });
//		builder.setNegativeButton("Cancel", dialogButtonListener);
        return builder.create();

    }

    class CustomAdapter extends BaseAdapter {

        private static final int TYPE_ITEM = 0;
        private static final int TYPE_SEPARATOR = 1;

        private ArrayList<String> mData = new ArrayList<>();
        private TreeSet<Integer> sectionHeader = new TreeSet<>();

        private LayoutInflater mInflater;

        public CustomAdapter(Context context) {


            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void addItem(final String item) {
            mData.add(item);

        }

        public void addSectionHeaderItem(final String item) {
            mData.add(item);
            sectionHeader.add(mData.size() - 1);
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public String getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public View getView(int posit, View convertView, ViewGroup parent) {

            int position = mData.size() - posit - 1;

//			if (getItemViewType(position) == TYPE_SEPARATOR) {
//				convertView = mInflater.inflate(R.layout.lv_section_header, parent, false);
//				TextView separatorView = (TextView) convertView.findViewById(R.id.separator);
//
//				separatorView.setText(mData.get(mData.size() - 1));
//				separatorView.setCompoundDrawablePadding(16);
//				separatorView.setCompoundDrawables(new IconicsDrawable(getActivity(), FontAwesome.Icon.faw_line_chart).color(Color.BLUE).sizeRes(R.dimen.list_item_avatar_size), null, null, null);
//			} else {
            ViewHolder holder;
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_item_tabela, parent, false);
            holder.tvDia = (TextView) convertView.findViewById(R.id.tx_dia_list_tabela);
            holder.tvTotal = (TextView) convertView.findViewById(R.id.tx_total_list_tabela);
            holder.tvTotal2 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela2);
            holder.tvTotal3 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela3);
            holder.tvTotal4 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela4);
            holder.tvTotal5 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela5);
            holder.tvTotal6 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela6);
            holder.tvTotal7 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela7);
            holder.tvTotal8 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela8);
            holder.tvTotal9 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela9);
            holder.tvTotal10 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela10);
            holder.tvTotal11 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela11);
            holder.tvTotal12 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela12);

            switch (listEntries.size()) {

                case 12:
                    holder.tvTotal12.setVisibility(View.VISIBLE);
                    holder.tvTotal12.setBackgroundColor(colors.get(11));
                case 11:
                    holder.tvTotal11.setVisibility(View.VISIBLE);
                    holder.tvTotal11.setBackgroundColor(colors.get(10));
                case 10:
                    holder.tvTotal10.setVisibility(View.VISIBLE);
                    holder.tvTotal10.setBackgroundColor(colors.get(9));
                case 9:

                    holder.tvTotal9.setVisibility(View.VISIBLE);
                    holder.tvTotal9.setBackgroundColor(colors.get(8));
                case 8:
                    holder.tvTotal8.setVisibility(View.VISIBLE);
                    holder.tvTotal8.setBackgroundColor(colors.get(7));
                case 7:
                    holder.tvTotal7.setVisibility(View.VISIBLE);
                    holder.tvTotal7.setBackgroundColor(colors.get(6));

                case 6:
                    holder.tvTotal6.setVisibility(View.VISIBLE);
                    holder.tvTotal6.setBackgroundColor(colors.get(5));
                case 5:
                    holder.tvTotal5.setVisibility(View.VISIBLE);
                    holder.tvTotal5.setBackgroundColor(colors.get(4));

                case 4:
                    holder.tvTotal4.setVisibility(View.VISIBLE);
                    holder.tvTotal4.setBackgroundColor(colors.get(3));
                case 3:
                    holder.tvTotal3.setVisibility(View.VISIBLE);
                    holder.tvTotal3.setBackgroundColor(colors.get(2));
                case 2:
                    holder.tvTotal2.setVisibility(View.VISIBLE);
                    holder.tvTotal2.setBackgroundColor(colors.get(1));
                case 1:

                default:
                    holder.tvTotal.setVisibility(View.VISIBLE);
                    holder.tvTotal.setBackgroundColor(colors.get(0));

            }


//            holder.tvTotal4 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela4);
//				convertView.setTag(holder);


//			else {
//				holder = (ViewHolder) convertView.getTag();
//			}


            holder.tvTotal.setText(getEntrievalue(0, position));
            holder.tvDia.setText(mData.get(position));
//        switch ()

            switch (listEntries.size()) {

                case 12:
                    holder.tvTotal12.setText(getEntrievalue(11, position));
                case 11:
                    holder.tvTotal11.setText(getEntrievalue(10, position));
                case 10:
                    holder.tvTotal10.setText(getEntrievalue(9, position));
                case 9:
                    holder.tvTotal9.setText(getEntrievalue(8, position));
                case 8:
                    holder.tvTotal8.setText(getEntrievalue(7, position));
                case 7:
                    holder.tvTotal7.setText(getEntrievalue(6, position));
                case 6:
                    holder.tvTotal6.setText(getEntrievalue(5, position));
                case 5:
                    holder.tvTotal5.setText(getEntrievalue(4, position));
                case 4:
                    holder.tvTotal4.setText(getEntrievalue(3, position));
                case 3:
                    holder.tvTotal3.setText(getEntrievalue(2, position));
                case 2:
                    holder.tvTotal2.setText(getEntrievalue(1, position));

                default:
                    break;
            }
//			}
            return convertView;
        }

        private String getEntrievalue(int which, int position) {
            NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###.##");

            for (Entry theEntr : listEntries.get(which)) {
                if (theEntr.getXIndex() == position) {
                    return nf.format(theEntr.getData() == null ? (double) theEntr.getVal() : (BigDecimal) theEntr.getData());

                }

            }
            return " - ";
        }

        public class ViewHolder {
            public TextView tvTotal;
            public TextView tvDia;
            public TextView tvTotal2;
            public TextView tvTotal3;
            public TextView tvTotal4;
            public TextView tvTotal5;
            public TextView tvTotal6;
            public TextView tvTotal7;
            public TextView tvTotal10;
            public TextView tvTotal8;
            public TextView tvTotal9;
            public TextView tvTotal12;
            public TextView tvTotal11;
        }

    }

}
