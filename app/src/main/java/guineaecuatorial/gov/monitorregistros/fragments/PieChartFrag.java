package guineaecuatorial.gov.monitorregistros.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.adapter.AdapterPieBar;
import guineaecuatorial.gov.monitorregistros.util.ColorTemplate;


public class PieChartFrag extends SimpleFragment implements OnChartValueSelectedListener {


private static final String ARG_PARAM1 = "xVals";
private static final String ARG_PARAM2 = "entries";
private static final String ARG_PARAM4 = "listv";
int mListv = -1;

private ArrayList<String> mEntries;
private ArrayList<String> mXvals;
private Spinner spinner;
private PieChart mChart;
private String TAG = "PieChart";
private AdapterPieBar adapter;
private ListView listview;
private Typeface tf;
private OnFragmentInteractionListener mListener;


public static Fragment newInstance() {
	return new PieChartFrag();

}

public static Fragment newInstance(ArrayList<String> xVals, ArrayList<String> entries, int listv) {
	PieChartFrag fragment = new PieChartFrag();
	Bundle args = new Bundle();
	args.putStringArrayList(ARG_PARAM1, xVals);
	args.putStringArrayList(ARG_PARAM2, entries);
	args.putInt(ARG_PARAM4, listv);
	fragment.setArguments(args);
	return fragment;

}

public static Fragment newInstance(ArrayList<String> xVals, ArrayList<String> entries) {
	return newInstance(xVals, entries, -1);

}

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@Override
public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	super.onCreateOptionsMenu(menu, inflater);
//		getActivity().getMenuInflater().inflate(R.menu.detail, menu);
	if (isAdded())
		menu.add(0, 3, 0, "Bar").setIcon(new IconicsDrawable(getView().getContext(), FontAwesome.Icon.faw_bar_chart).actionBar().colorRes(R.color.spinnerColorPeriodo)).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
}


@Override
public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
		case 3:
			if (mListener != null) {
				mListener.onFragmentInteraction(mXvals, mEntries, mListv);
			}
	}
	return super.onOptionsItemSelected(item);
}

@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setRetainInstance(true);
	if (getArguments() != null) {
		mXvals = getArguments().getStringArrayList(ARG_PARAM1);
		mEntries = getArguments().getStringArrayList(ARG_PARAM2);
		mListv = getArguments().getInt(ARG_PARAM4, 0);

	}

}

@Override
public void onAttach(Context context) {
	super.onAttach(context);
	tf = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
	try {
		mListener = (OnFragmentInteractionListener) context;
	} catch (ClassCastException e) {
		throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
	}
}

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	return inflater.inflate(R.layout.frag_simple_pie, container, false);
}


@Override
public void onViewCreated(View v, Bundle savedInstanceState) {
	super.onViewCreated(v, savedInstanceState);
	mChart = (PieChart) v.findViewById(R.id.pieChart1);
//		mChart.setDescription(GrafItemContent.ITEMS.get(GraficoDetailActivity.intExtra).getNome()+" * "+ GraficoDetailActivity.dataUltima);
//		mChart.setDescriptionPosition(400, 10);
	mChart.setDescription("");
	mChart.setDragDecelerationFrictionCoef(0.95f);
	mChart.setHoleColorTransparent(true);
	mChart.setExtraTopOffset(28);
//        mChart.setTransparentCircleColor(Color.WHITE);
	mChart.setCenterTextTypeface(tf);
	mChart.setUsePercentValues(true);
//		mChart.setCenterTextWordWrapEnabled(true);
	mChart.setCenterTextSize(20f);
	mChart.setCenterTextRadiusPercent(0.9f);
	mChart.setDrawSliceText(false);
	mChart.setNoDataText(getActivity().getString(R.string.no_data_text_description));
	mChart.setOnChartValueSelectedListener(this);
//        mChart.setBackgroundColor(getResources().getColor(R.color.amarelinho));
	// radius of the center hole in percent of maximum radius
	mChart.setHoleRadius(78f);
	mChart.setTransparentCircleRadius(81f);
	mChart.setData(generatePieData(mXvals, mEntries));
	mChart.highlightValues(null);
	Legend l = mChart.getLegend();
	l.setEnabled(false);
//            l.setTextSize(12f);
//            l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
	if (mChart.getData().getYValueSum() != 0) setTotalCenterText();
	mChart.setCenterTextColor(Color.DKGRAY);
//		mChart.animateXY(700, 1200, Easing.EasingOption.EaseOutBounce, Easing.EasingOption.EaseInOutSine);
	mChart.animateY(1700, Easing.EasingOption.EaseOutBack);
	mChart.animateX(700);
	prepareLisView();
//		initSpinner();
	setHasOptionsMenu(true);
}

@Override
public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
	NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###,###,###,###,###.#");
	mChart.setCenterText(mChart.getXValue(e.getXIndex()) + "\n" + nf.format((e.getData() == null ? e.getVal() : (BigDecimal) e.getData())) + " = " + nf.format((double) mChart.getPercentOfTotal(e.getVal())) + "% \n");
	mChart.setCenterTextColor(ContextCompat.getColor(getContext(), ColorTemplate.PASTEL_COLORS[e.getXIndex()]));
	listview.setSelection(e.getXIndex());
	Log.i("VAL SELECTED", "Value: " + e.getVal() + ", xIndex: " + e.getXIndex() + ", DataSet index: " + dataSetIndex);
}

@Override
public void onNothingSelected() {
	setTotalCenterText();

}

private void setTotalCenterText() {
	NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###,###,###.###");
	double yValueSum = (double) mChart.getData().getYValueSum();
	if (yValueSum > 99999999.0) {
		List<BigDecimal> bigDecimalsValues = new ArrayList<>();
		for (int i = 0; i < mEntries.size(); i++) {
			bigDecimalsValues.add(new BigDecimal(mEntries.get(i)));
		}
		BigDecimal addBigDecimals = new BigDecimal(0);
		for (BigDecimal value : bigDecimalsValues) {
			addBigDecimals = addBigDecimals.add(new BigDecimal(value.doubleValue()));
		}
		if (getView() != null)
			Snackbar.make(getView(), "A soma é muito elevada. Pode haver um erro de calculo!!!!!", Snackbar.LENGTH_LONG).show();
		mChart.setCenterText("Total:\n" + nf.format(addBigDecimals) + " = 100%");
	} else mChart.setCenterText("Total:\n" + nf.format(yValueSum) + " = 100%");
	mChart.setCenterTextColor(Color.DKGRAY);
}

private PieData generatePieData(ArrayList<String> xVals, ArrayList<String> strEntries) {
	ArrayList<Entry> entries = new ArrayList<>();
	for (int i = 0; i < strEntries.size(); i++) {
		if (strEntries.get(i).length() > 6) {
			BigDecimal val = new BigDecimal(strEntries.get(i));
			entries.add(new Entry(val.floatValue(), i, val));
		} else entries.add(new Entry(Float.valueOf(strEntries.get(i)), i));
	}
	PieDataSet ds1 = new PieDataSet(entries, "");
	ds1.setColors(ColorTemplate.PASTEL_COLORS, getActivity());
	ds1.setSliceSpace(1f);
	ds1.setSelectionShift(9f);
	ds1.setValueTextColor(Color.WHITE);
	PieData pieData = new PieData(xVals, ds1);
	pieData.setValueFormatter(new PercentFormatter());
	pieData.setValueTextSize(9f);
	pieData.setValueTypeface(tf);
	return pieData;
}

private void prepareLisView() {
	if (-1 == mListv) {
		return;
	}
	if (!isAdded()) return;
	listview = (ListView) getActivity().findViewById(mListv);
	adapter = new AdapterPieBar(getContext(), mXvals, mEntries);
	listview.setAdapter(adapter);
	listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			mChart.highlightValues(new Highlight[]{new Highlight(position, mChart.getDataSetIndexForIndex(0))});
			NumberFormat nf = new DecimalFormat("###,###,###,###,###,###,###,###,###,###.###");
			float getVal = mChart.getSelectionDetailsAtIndex(position).get(0).val;
			mChart.setCenterText(mChart.getXValue(position) + "\n" + nf.format(new BigDecimal(mEntries.get(position))) + " = " + nf.format((double) mChart.getPercentOfTotal(getVal)) + "% \n");
			mChart.setCenterTextColor(ContextCompat.getColor(getContext(), ColorTemplate.PASTEL_COLORS[position]));

		}

	});

}


@Override
public void onDetach() {
	super.onDetach();
	mListener = null;
}

/**
 * This interface must be implemented by activities that contain this
 * fragment to allow an interaction in this fragment to be communicated
 * to the activity and potentially other fragments contained in that
 * activity.
 * <p/>
 * See the Android Training lesson <a href=
 * "http://developer.android.com/training/basics/fragments/communicating.html"
 * >Communicating with Other Fragments</a> for more information.
 */
public interface OnFragmentInteractionListener {
	// TODO: Update argument type and name
	void onFragmentInteraction(ArrayList<String> xVals, ArrayList<String> entries, int listv);
}
}
