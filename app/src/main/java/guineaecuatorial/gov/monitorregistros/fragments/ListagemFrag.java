package guineaecuatorial.gov.monitorregistros.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import java.util.List;

import guineaecuatorial.gov.monitorregistros.GraficoDetailActivity;
import guineaecuatorial.gov.monitorregistros.R;
import guineaecuatorial.gov.monitorregistros.model.ContentItem4;


public class ListagemFrag extends SimpleFragment {

    private static final String ARG_PARAM1 = "objs";
    private Typeface tfr, rm;
    private ListView list;

    public static Fragment newInstance() {

        return new ListagemFrag();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        tfr = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        rm = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Medium.ttf");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.frag_list_item_listagem, list);
        if (!GraficoDetailActivity.listagem.isEmpty())
            convertView.findViewById(R.id.tv_listagem_empty).setVisibility(View.INVISIBLE);
        list = (ListView) convertView.findViewById(R.id.listagem_id);
        list.addHeaderView(new View(getActivity()));
        list.addFooterView(new View(getActivity()));

//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
//                R.layout.list_item, R.id.tvName, values);


        final MyAdapter adapter = new MyAdapter(getActivity(), GraficoDetailActivity.listagem);


        list.setAdapter(adapter);


        return convertView;
    }


    private class MyAdapter extends ArrayAdapter<ContentItem4> {

        public MyAdapter(Context context, List<ContentItem4> objects) {
            super(context, 0, objects);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ContentItem4 c = getItem(position);

            ViewHolder holder;

            if (convertView == null) {

                holder = new ViewHolder();


                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item, parent, false);
                holder.tvName = (TextView) convertView.findViewById(R.id.listagem_tv_name);

                holder.tvTotal = (TextView) convertView.findViewById(R.id.listagem_tv_bottom_left);
                holder.tvTotal.setTypeface(tfr);
                holder.tvDia = (TextView) convertView.findViewById(R.id.listagem_tv_bottomright);
                holder.tvDia.setTypeface(tfr);
                holder.tvOrg = (TextView) convertView.findViewById(R.id.listagem_tv_organica);

                holder.tvEtap = (TextView) convertView.findViewById(R.id.listagem_tv_quart_lesft);
                holder.tvEtap.setTypeface(rm);
                holder.tvEtap.setTextColor(getResources().getColor(R.color.mono_3));
                holder.tvTemp = (TextView) convertView.findViewById(R.id.listagem_tv_quartpos_right);
                holder.tvTemp.setTypeface(tfr);

                holder.tvTemp.setCompoundDrawables(new IconicsDrawable(getContext(), FontAwesome.Icon.faw_clock_o).colorRes(R.color.darkred).sizeDp(14), null, null, null);
                holder.tvTemp.setCompoundDrawablePadding(8);


//            holder.tvTotal4 = (TextView) convertView.findViewById(R.id.tx_total_list_tabela4);
                convertView.setTag(holder);


            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tvName.setText(c.name);
            holder.tvTotal.setText(c.tota);
            holder.tvDia.setText(c.dia);
            holder.tvOrg.setText(c.orga);
            holder.tvEtap.setText(c.etapa);
            holder.tvTemp.setText(c.tempo);


            return convertView;
        }

        private class ViewHolder {

            TextView tvName, tvTotal, tvDia, tvOrg, tvEtap, tvTemp;
        }
    }


}
