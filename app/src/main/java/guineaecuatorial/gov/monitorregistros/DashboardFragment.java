package guineaecuatorial.gov.monitorregistros;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import guineaecuatorial.gov.monitorregistros.dashboardchartitem.BarChartItem;
import guineaecuatorial.gov.monitorregistros.dashboardchartitem.ChartItem;
import guineaecuatorial.gov.monitorregistros.dashboardchartitem.LineChartItem;
import guineaecuatorial.gov.monitorregistros.dashboardchartitem.PieChartItem;
import guineaecuatorial.gov.monitorregistros.helper.dashboard.REQ5003940_Helper;
import guineaecuatorial.gov.monitorregistros.helper.dashboard.RESP5003940_Helper;
import guineaecuatorial.gov.monitorregistros.model.GrafItemContent;
import guineaecuatorial.gov.monitorregistros.model.dashboard.RESP5003940_Model;
import guineaecuatorial.gov.monitorregistros.model.thegraf.RESP5003531_Model;
import guineaecuatorial.gov.monitorregistros.network.Networkhelper;
import guineaecuatorial.gov.monitorregistros.util.ColorTemplate;
import guineaecuatorial.gov.monitorregistros.util.MyValueFormatter;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link }
 * interface.
 */
public class DashboardFragment extends Fragment implements AbsListView.OnItemClickListener {

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private static final String ARG_PARAM1 = "param1";

public String idSub = "";
ArrayList<ChartItem> list;
private ArrayList<String> mLegsNoRep;
/**
 * The fragment's ListView/GridView.
 */
private AbsListView mListView;
/**
 * The Adapter which will be used to populate the ListView/GridView with
 * Views.
 */

	private JSONObject json;
	private List<RESP5003940_Model.grafsModel> grafsModelArrayList;

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
public DashboardFragment() {
}

// TODO: Rename and change types of parameters
public static DashboardFragment newInstance(String id) {
	DashboardFragment fragment = new DashboardFragment();
	Bundle args = new Bundle();
	args.putString(ARG_PARAM1, id);
	fragment.setArguments(args);
	return fragment;
}

@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	GraficoListActivity.progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_spinner);
	//Make progress bar appear when you need it
	if (getArguments() != null) {
		idSub = getArguments().getString(ARG_PARAM1);
	}

}


	@Override
	public void onResume() {
		super.onResume();
		if (null != grafsModelArrayList) setupStagredView();
	}

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
	// Set the adapter
	mListView = (AbsListView) view.findViewById(android.R.id.list);
	return view;
}

@Override
public void onViewCreated(View view, Bundle savedInstanceState) {
	super.onViewCreated(view, savedInstanceState);
	GraficoListActivity.progressBar.setVisibility(View.VISIBLE);
	new Thread(new Runnable() {
		public void run() {
			new Networkhelper(getActivity()).callWebservice(REQ5003940_Helper.prepareModel(Integer.parseInt(idSub), LoginActivity.sessid, ""), new Callback() {
				@Override
				public void onFailure(Request request, IOException e) {
					if (isAdded()) getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							GraficoListActivity.progressBar.setVisibility(View.GONE);
							Toast.makeText(getActivity(), "Não foi possivel conectar com servidor", Toast.LENGTH_LONG).show();

						}
					});
				}

				@Override
				public void onResponse(Response response) throws IOException {
					try {
						json = new JSONObject(response.body().string());
						if (BuildConfig.DEBUG)
							Log.e("DashboardFragment", "onResponse (line 175): " + json);
						if (null != grafsModelArrayList) grafsModelArrayList.clear();
						grafsModelArrayList = RESP5003940_Helper.parseModel(json);
						if (grafsModelArrayList != null) {
							Log.e("DashboardFragment", "onResponse (line 179): grafsModelArrayList." + grafsModelArrayList.get(0).getDados().getRows().get(0).getDados().get(0).getXvals());
						}
						try {
							if (isAdded()) getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									GraficoListActivity.progressBar.setVisibility(View.GONE);
									if (null != grafsModelArrayList) setupStagredView();

								}
							});
						} catch (Exception e) {
							e.printStackTrace();
							if (isAdded()) getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									Toast.makeText(getContext(), "Erro ", Toast.LENGTH_LONG).show();
									GraficoListActivity.progressBar.setVisibility(View.GONE);
								}
							});
						}

					} catch (JSONException e) {
						e.printStackTrace();
						if (isAdded()) getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									Toast.makeText(getContext(), "Erro ", Toast.LENGTH_LONG).show();
									GraficoListActivity.progressBar.setVisibility(View.GONE);
								}
							});

						}


					}
				});
			}
	}).start();
}

private void setupStagredView() {
	list = new ArrayList<>();
	if (grafsModelArrayList != null) {
		for (RESP5003940_Model.grafsModel grafsModel : grafsModelArrayList) {
			ArrayList<String> legs = null;
			ArrayList<String> xVals = new ArrayList<>();
			ArrayList<String> entr = new ArrayList<>();
			switch (grafsModel.getTp_dashboard()) {
				case "PIE":
					case "BAR":
						break;
					case "STACKBAR":
					case "LINHA":
						legs = new ArrayList<>();
						break;

				}
			ArrayList<RESP5003531_Model.dadosModel> dadosModelArrayList = null;
			try {
//			dataUltima = grafsModel.getDados().getRows().get(0).getDt_ultima_act();
				dadosModelArrayList = grafsModel.getDados().getRows().get(0).getDados();

			} catch (Exception e) {
				e.printStackTrace();
				//PARA NO DATA mostrar
				switch (grafsModel.getTp_dashboard()) {
						case "PIE":
							list.add(new PieChartItem(generatePieData(xVals, entr), getContext(), grafsModel.getTipo() != null, grafsModel.getNome_dashboard()));
							break;
					case "BAR":
						list.add(new BarChartItem(generateBarData(xVals, entr, legs), getContext(), grafsModel.getTipo() != null, grafsModel.getNome_dashboard()));
						break;
					case "STACKBAR":
						break;
					case "LINHA":
						legs = new ArrayList<>();
						list.add(new LineChartItem(generateLineData(xVals, entr, legs), getContext(), grafsModel.getTipo() != null, grafsModel.getNome_dashboard()));
						break;

				}
				}
			if (dadosModelArrayList != null) {
				for (int i = 0; i < dadosModelArrayList.size(); i++) {
					//					String format = String.format("%.2f", 1.20093);
					//					String format = f.format(dadosModelArrayList.get(i).getTotal());
					String format = dadosModelArrayList.get(i).getTotal();
					//							!= null ? dadosModelArrayList.get(i).getTotal() : 0;
					entr.add("" + format);
					switch (grafsModel.getTp_dashboard()) {
						case "STACKBAR":
							if (legs != null) {
								legs.add(null != dadosModelArrayList.get(i).getNomelinha() ? dadosModelArrayList.get(i).getNomelinha() : "");
							}
						case "BAR":
						case "PIE":
							xVals.add(null != dadosModelArrayList.get(i).getXvals() ? dadosModelArrayList.get(i).getXvals() : "-");
							//					objects.add(new ContentItem(xVals.get(i), entr.get(i), dadosModelArrayList.get(i).getNomelinha()));
							break;
						case "LINHA":
							xVals.add(null != dadosModelArrayList.get(i).getData_evol() ? dadosModelArrayList.get(i).getData_evol() : "");
							if (legs != null) {
								legs.add(null != dadosModelArrayList.get(i).getNomelinha() ? dadosModelArrayList.get(i).getNomelinha() : "");
							}
							break;
						//				case "LISTA":
						//					listagem.add(new ContentItem4(dadosModelArrayList.get(i).getTitulo(), dadosModelArrayList.get(i).getDatatext(), dadosModelArrayList.get(i).getSubtit(), "", "", ""));
						//					break;
						default:
							return;
					}
				}
			}
//				Log.e("GraficoDetailActivity", "onResponse (line 172): xVals.size()" + xVals.size());
				if (xVals.size() == entr.size() && null != xVals.get(0)) {
					switch (grafsModel.getTp_dashboard()) {
						case "PIE":
							list.add(new PieChartItem(generatePieData(xVals, entr), getContext(), grafsModel.getTipo() == null, grafsModel.getNome_dashboard()));
							break;
						case "BAR":
						case "STACKBAR":
//							list.add(new BarChartItem(generateDataBar(3), getContext(),grafsModel.getTipo()==null));
							list.add(new BarChartItem(generateBarData(xVals, entr, legs), getContext(), grafsModel.getTipo() == null, grafsModel.getNome_dashboard()));
							break;
						case "LINHA":
							list.add(new LineChartItem(generateLineData(xVals, entr, legs), getContext(), grafsModel.getTipo() == null, grafsModel.getNome_dashboard()));
							break;

					}
				}


			}
	} else setEmptyText("Lista vazia!");


		// 30 items
//		for (int i = 0; i < 30; i++) {
//
//			if(i % 3 == 0) {
//				list.add(new LineChartItem(generateDataLine(i + 1), getContext()));
//			} else if(i % 3 == 1) {
//				list.add(new BarChartItem(generateDataBar(i + 1), getContext()));
//			} else if(i % 3 == 2) {
//				list.add(new PieChartItem(generateDataPie(i + 1), getContext()));
//			}
//		}

		ChartDataAdapter cda = new ChartDataAdapter(getContext(), list);
		mListView.setAdapter(cda);
	mListView.setOnItemClickListener(this);
}


public void setEmptyText(CharSequence emptyText) {
	View emptyView = mListView.getEmptyView();
	if (emptyView instanceof TextView) {
		((TextView) emptyView).setText(emptyText);
	}
}

@Override
public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	if (grafsModelArrayList.get(position).getTipo() != null) {
		Intent intent = new Intent(getContext(), GraficoDetailActivity.class);
		Bundle bundle = new Bundle();
		bundle.putParcelable(GraficoDetailActivity.ARG_POSITION, new GrafItemContent.GrafItem(grafsModelArrayList.get(position).getId(), grafsModelArrayList.get(position).getNome_dashboard(), grafsModelArrayList.get(position).getTipo(), grafsModelArrayList.get(position).getPeriodo()));
		intent.putExtras(bundle);
		startActivity(intent);
		(getActivity()).overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
	}
}

private LineData generateDataLine(int cnt) {
	ArrayList<Entry> e1 = new ArrayList<Entry>();
	for (int i = 0; i < 12; i++) {
		e1.add(new Entry((int) (Math.random() * 65) + 40, i));
	}
	LineDataSet d1 = new LineDataSet(e1, "New DataSet " + cnt + ", (1)");
	d1.setLineWidth(2.5f);
	d1.setCircleSize(4.5f);
	d1.setHighLightColor(Color.rgb(244, 117, 117));
	d1.setDrawValues(false);
	ArrayList<Entry> e2 = new ArrayList<Entry>();
	for (int i = 0; i < 12; i++) {
		e2.add(new Entry(e1.get(i).getVal() - 30, i));
	}
	LineDataSet d2 = new LineDataSet(e2, "New DataSet " + cnt + ", (2)");
	d2.setLineWidth(2.5f);
	d2.setCircleSize(4.5f);
	d2.setHighLightColor(Color.rgb(244, 117, 117));
	d2.setColor(ColorTemplate.VORDIPLOM_COLORS[0]);
	d2.setCircleColor(ColorTemplate.VORDIPLOM_COLORS[0]);
	d2.setDrawValues(false);
	ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();
	sets.add(d1);
	sets.add(d2);
	return new LineData(getMonths(), sets);
}

/**
 * generates a random ChartData object with just one DataSet
 *
 * @return
 */
private BarData generateDataBar(int cnt) {
	ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
	for (int i = 0; i < 12; i++) {
		entries.add(new BarEntry((int) (Math.random() * 70) + 30, i));
	}
	BarDataSet d = new BarDataSet(entries, "New DataSet " + cnt);
	d.setBarSpacePercent(20f);
	d.setColors(ColorTemplate.VORDIPLOM_COLORS);
	d.setHighLightAlpha(255);
	return new BarData(getMonths(), d);
}

/**
 * generates a random ChartData object with just one DataSet
 *
 * @return
 */
private PieData generateDataPie(int cnt) {
	ArrayList<Entry> entries = new ArrayList<Entry>();
	for (int i = 0; i < 4; i++) {
		entries.add(new Entry((int) (Math.random() * 70) + 30, i));
	}
	PieDataSet d = new PieDataSet(entries, "");
	// space between slices
	d.setSliceSpace(2f);
	d.setColors(ColorTemplate.VORDIPLOM_COLORS);
	return new PieData(getQuarters(), d);
}

private BarData generateBarData(ArrayList<String> xVals, ArrayList<String> strEntries, ArrayList<String> mLegs) {
	ArrayList<BarDataSet> sets = new ArrayList<>();
	ArrayList<BarEntry> entries = new ArrayList<>();
	List<String> mXvalsNoRepit = new ArrayList<>();
	mLegsNoRep = new ArrayList<>();
	mLegsNoRep.clear();
	mXvalsNoRepit.clear();
	for (int i = 0; i < xVals.size(); i++) {
		if (!mXvalsNoRepit.contains(xVals.get(i))) {
			mXvalsNoRepit.add(xVals.get(i));
		}

	}
	if (null != mLegs) {

			for (String leg : mLegs) {
				if (!mLegsNoRep.contains(leg) && leg != null) mLegsNoRep.add(leg);
			}
			int size = mLegsNoRep.size();
			//Marcos: para o caso de stack vir com um só valor
			if (size <= 1) for (int x = 0; x < strEntries.size(); x++) {

				if (strEntries.get(x).length() > 6) {
					BigDecimal val = new BigDecimal(strEntries.get(x));
					entries.add(new BarEntry(val.floatValue(), x, val));
				} else entries.add(new BarEntry(Float.valueOf(strEntries.get(x)), x));

			}
			else {

				//Faz um for entre todos os valores

				//Identifica os valores repetidos e constroi para cada barra
				for (int x = 0; x < mXvalsNoRepit.size(); x++) {
					String xCurrentValue = mXvalsNoRepit.get(x);
					//Marcos: para o caso de stack vir com um só valor

					float[] vals = new float[size];
					for (int l = 0; l < mLegsNoRep.size(); l++) {
						String leg = mLegsNoRep.get(l);
						for (int i = 0; i < xVals.size(); i++) {
							if (!xVals.get(i).equals(xCurrentValue)) continue;
							if (mLegs.get(i) != null && mLegs.get(i).equals(leg)) {

								vals[l] = Float.valueOf(strEntries.get(i));


							}
						}

					}
					entries.add(new BarEntry(vals, x));


				}
			}

		} else {
			for (int x = 0; x < strEntries.size(); x++) {

				if (strEntries.get(x).length() > 6) {
					BigDecimal val = new BigDecimal(strEntries.get(x));
					entries.add(new BarEntry(val.floatValue(), x, val));
				} else entries.add(new BarEntry(Float.valueOf(strEntries.get(x)), x));

			}
		}


		String stackBar = "";
		String[] strStackLabels = new String[mLegsNoRep.size()];
	//para Stack bar
	if (null != mLegs) {
		if (mLegsNoRep.size() == 1) stackBar = mLegsNoRep.get(0);
		else stackBar = ".";
		for (int i = 0; i < mLegsNoRep.size(); i++) {
			strStackLabels[i] = mLegsNoRep.get(i);
		}

	}
	BarDataSet ds1 = new BarDataSet(entries, stackBar);
		ds1.setColors(getColors(mLegs), getContext());
	ds1.setColors(getColors(mLegs), getContext());
	ds1.setBarSpacePercent(20f);
	ds1.setDrawValues(null == mLegs);
	ds1.setValueTextColor(Color.BLACK);
	ds1.setValueFormatter(new MyValueFormatter());
	ds1.setValueTextSize(12f);
	if (null != mLegs) ds1.setStackLabels(strStackLabels);
//			ds1.setStackLabels(new String[]{"Marcos","joss"});
//		Log.e("BarChartFrag", "generateBarData (line 182): ds1.getYValForXIndex(0);" + ds1.getYValForXIndex(0));
//		GraficoDetailActivity.objects.clear();
//		for (int i = 0; i < mXvalsNoRepit.size(); i++) {
//			GraficoDetailActivity.objects.add(new ContentItem(mXvalsNoRepit.get(i), entries.get(i).getData() == null ? "" + ds1.getYValForXIndex(i) : entries.get(i).getData().toString(), entries.get(i).toString()));
//		}
		sets.add(ds1);
		return new BarData(mXvalsNoRepit, sets);
}

private PieData generatePieData(ArrayList<String> xVals, ArrayList<String> strEntries) {
	ArrayList<Entry> entries = new ArrayList<>();
	for (int i = 0; i < strEntries.size(); i++) {
		if (strEntries.get(i).length() > 6) {
			BigDecimal val = new BigDecimal(strEntries.get(i));
			entries.add(new Entry(val.floatValue(), i, val));
		} else entries.add(new Entry(Float.valueOf(strEntries.get(i)), i));
	}
	PieDataSet ds1 = new PieDataSet(entries, "");
	ds1.setColors(ColorTemplate.MONO_COLORS, getActivity());
	ds1.setSliceSpace(1f);
	ds1.setSelectionShift(9f);
	ds1.setValueTextColor(Color.WHITE);
	PieData pieData = new PieData(xVals, ds1);
	pieData.setValueFormatter(new PercentFormatter());
	pieData.setValueTextSize(9f);
//		pieData.setValueTypeface(tf);
	return pieData;
}

private LineData generateLineData(ArrayList<String> xVals, ArrayList<String> strEntries, ArrayList<String> mLegs) {
	LineDataSet set;
	ArrayList<Entry> entries = new ArrayList<>();
//        ArrayList<Entry> entries2 = new ArrayList<Entry>();
	ArrayList<ArrayList<Entry>> listEntries = new ArrayList<>();
	listEntries.clear();
	ArrayList<LineDataSet> dataSets = new ArrayList<>();
	ArrayList<String> mXvalsNoRepit = new ArrayList<>();
	mXvalsNoRepit.clear();
	ArrayList<String> mDiasLeg = new ArrayList<>();
	mLegsNoRep = new ArrayList<>();
	ArrayList<LineDataSet> ds = new ArrayList<>();
	for (int i = 0; i < xVals.size(); i++) {
		if (!mXvalsNoRepit.contains(xVals.get(i)) && null != xVals.get(i)) {
			mXvalsNoRepit.add(xVals.get(i));
		}

	}
//        mLegsNoRep
		for (String leg : mLegs) {
			if (!mLegsNoRep.contains(leg) && leg != null) mLegsNoRep.add(leg);
		}
//		GraficoDetailActivity.objects = new ArrayList<>();
		int exclude = 0;
		for (int l = 0; l < mLegsNoRep.size(); l++) {
			String leg = mLegsNoRep.get(l);
//			Log.e(TAG, "vmaosod odojdoasdosa " + leg);
			for (int i = 0; i < mLegs.size(); i++) {
				if (mLegs.get(i) != null && mLegs.get(i).equals(leg)) mDiasLeg.add(xVals.get(i));
				else mDiasLeg.add("0");
			}
			//TODO Marcos ver isto            entries.add(new Entry(Float.valueOf(strEntries.get(i)), i, "dsa"));
			for (int j = 0; j < mXvalsNoRepit.size(); j++) {
//                   if(.equals(leg)){
				int ind = mDiasLeg.indexOf(mXvalsNoRepit.get(j));
//
				if (ind != -1) {
					if (strEntries.get(ind).length() > 6) {
						BigDecimal val = new BigDecimal(strEntries.get(ind));
						entries.add(new Entry(val.floatValue(), j, val));
					} else entries.add(new Entry(Float.valueOf(strEntries.get(ind)), j));
				}
//				GraficoDetailActivity.objects.add(new ContentItem(mXvalsNoRepit.get(j), mXvalsNoRepit.get(j), leg));
//				else entries.add(new Entry(0, j));
			}
			listEntries.add((ArrayList<Entry>) entries.clone());
			set = new LineDataSet(listEntries.get(l - exclude), leg);
			int color = ContextCompat.getColor(getContext(), ColorTemplate.MONO_COLORS[l]);
			set.setColor(color);
			set.setCircleColor(color);
			set.setHighlightLineWidth(3);
			set.setValueFormatter(new MyValueFormatter());
//            set.setColor(ColorTemplate.PASTEL_COLORS[l]);
			ds.add(set);
//			colors.add(ds.get(l - exclude).getColor(0));
			ds.get(l - exclude).setLineWidth(3f);
			ds.get(l - exclude).setCircleSize(5f);
			dataSets.add(ds.get(l - exclude));
			entries.clear();
			mDiasLeg.clear();
			if (l > 10) break;
		}
	return new LineData(mXvalsNoRepit, dataSets);
}

private int[] getColors(ArrayList<String> mLegs) {
	int[] colors;
	if (null != mLegs) {
		int stacksize = mLegsNoRep.size();
		colors = new int[stacksize];
		System.arraycopy(ColorTemplate.VORDIPLOM_COLORS, 0, colors, 0, stacksize);
	} else colors = ColorTemplate.MONO_COLORS;
	return colors;
}

private ArrayList<String> getQuarters() {
	ArrayList<String> q = new ArrayList<String>();
	q.add("1st Quarter");
	q.add("2nd Quarter");
	q.add("3rd Quarter");
	q.add("4th Quarter");
	return q;
}

private ArrayList<String> getMonths() {
	ArrayList<String> m = new ArrayList<String>();
	m.add("Jan");
	m.add("Feb");
	m.add("Mar");
	m.add("Apr");
	m.add("May");
	m.add("Jun");
	m.add("Jul");
	m.add("Aug");
	m.add("Sep");
	m.add("Okt");
	m.add("Nov");
	m.add("Dec");
	return m;
}

private class ChartDataAdapter extends ArrayAdapter<ChartItem> {

	public ChartDataAdapter(Context context, List<ChartItem> objects) {
		super(context, 0, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = getItem(position).getView(position, convertView, getContext());
		view.setEnabled(false);
		return view;
	}

	@Override
	public int getItemViewType(int position) {
		// return the views type
		return getItem(position).getItemType();
	}

	@Override
	public int getViewTypeCount() {
		return 3; // we have 3 different item-types
	}
}

}
